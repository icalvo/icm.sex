﻿namespace Icm.Sex.Domain.Test
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.BodyTemplates.Services;

    public static class Helper
    {
        public static IList<Body> GetBodies(params Tuple<string, string>[] bodies)
        {
            var templates = new BodyTemplateInitializationService().DefaultItems().ToList();
            return bodies.Select(tup => new Body(tup.Item1, templates.ByName(tup.Item2))).ToList();
        }
    }
}