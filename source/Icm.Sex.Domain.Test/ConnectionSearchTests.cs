﻿namespace Icm.Sex.Domain.Test
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Core.Test;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Activities.Services;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Predicates;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Services;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Connections.Predicates;
    using Icm.Sex.Domain.Connections.Predicates.Suitability;
    using Icm.Sex.Domain.ConnectionSearch;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Parts.Services;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.UnitsOfWork;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class ConnectionSearchTests
    {
        private ISexUnitOfWork unitOfWork;

        [TestInitialize]
        public void Initialize()
        {
            var bodyRepository = new MemoryBodyRepository();
            var bodyTemplateRepository = new MemoryBodyTemplateRepository();
            var activityRepository = new MemoryActivityRepository();
            var partRepository = new MemoryPartRepository();
            var sceneRepository = new MemorySceneRepository();
            unitOfWork = new MemorySexUnitOfWork(
                bodyRepository,
                bodyTemplateRepository,
                sceneRepository,
                partRepository,
                activityRepository);

            var initService = new InitializationService(
                unitOfWork,
                new PartInitializationService(),
                new ActivityInitializationService(),
                new BodyTemplateInitializationService());

            initService.Initialize();

            unitOfWork.Save();
        }

        [TestMethod]
        public void GetConnections_Test()
        {
            Body[] bodies =
            {
                new Body("Ignacio", unitOfWork.BodyTemplateRepository.GetById("man")),
                new Body("María", unitOfWork.BodyTemplateRepository.GetById("woman"))
            };
            Activity[] activities =
            {
                unitOfWork.ActivityRepository.GetById("penetrate")
            };
            var maleNotPenetratedFilter = new MaleIsPenetratedPredicate(new MaleByTemplatePredicate()).Not();
            var suitabilityPredicate = new ActivityPartSuitabilityPredicate();

            // Act
            IEnumerable<Connection> connections = ConnectionSearchService.GetConnections(
                bodies,
                activities,
                maleNotPenetratedFilter.And(suitabilityPredicate));

            var expected = @"
ignacio.finger penetrate maría.rectum
ignacio.finger penetrate maría.vagina
ignacio.penis penetrate maría.rectum
ignacio.penis penetrate maría.vagina
maría.finger penetrate maría.rectum
maría.finger penetrate maría.vagina
";
            ListAssertionHelpers.AssertEnumerableContents(connections, expected);
        }

        [TestMethod]
        public void GetAllSituations_Test()
        {
            Body[] bodies =
            {
                new Body("Ignacio", unitOfWork.BodyTemplateRepository.GetById("man")),
                new Body("María", unitOfWork.BodyTemplateRepository.GetById("woman"))
            };
            Activity[] activities =
            {
                new Penetrate(),
            };
            var connectionPredicate = new MaleIsPenetratedPredicate(new MaleByTemplatePredicate()).Not();
            var suitabilityPredicate = new ActivityPartSuitabilityPredicate();
            var partsConnectedOnce = new BodyPartsUsedOnlyOncePredicate().Enum();

            // Act
            IEnumerable<Tuple<BitArray, IEnumerable<Connection>>> situations = ConnectionSearchService.GetAllSituations(
                bodies,
                activities,
                connectionPredicate.And(suitabilityPredicate),
                partsConnectedOnce);

            var expectedVariations = @"
100000 ignacio.finger penetrate maría.rectum
010000 ignacio.finger penetrate maría.vagina
001000 ignacio.penis penetrate maría.rectum
011000 ignacio.finger penetrate maría.vagina, ignacio.penis penetrate maría.rectum
000100 ignacio.penis penetrate maría.vagina
100100 ignacio.finger penetrate maría.rectum, ignacio.penis penetrate maría.vagina
000010 maría.finger penetrate maría.rectum
010010 ignacio.finger penetrate maría.vagina, maría.finger penetrate maría.rectum
000110 ignacio.penis penetrate maría.vagina, maría.finger penetrate maría.rectum
000001 maría.finger penetrate maría.vagina
100001 ignacio.finger penetrate maría.rectum, maría.finger penetrate maría.vagina
001001 ignacio.penis penetrate maría.rectum, maría.finger penetrate maría.vagina
";
            ListAssertionHelpers.AssertVariationContents(
                situations.ToList(),
                expectedVariations);
        }
    }
}