﻿namespace Icm.Sex.Domain.Test
{
    using System.Diagnostics.CodeAnalysis;
    using FluentAssertions;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Parts.Services;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class PartInitializationServiceTest
    {
        [TestMethod]
        public void EnsurePartsInRepositoryTest()
        {
            var service = new PartInitializationService();
            service.DefaultItems().Should().HaveCount(15);
        }
    }
}