﻿namespace Icm.Sex.Domain.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Activities;
    using Bodies;
    using BodyTemplates;
    using Connections;
    using FakeItEasy;
    using FluentAssertions;
    using Icm.Sex.Core;
    using Icm.Sex.Domain.Activities.Services;
    using Icm.Sex.Domain.Connections.Predicates.Suitability;
    using Icm.Sex.Domain.Parts.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Parts;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class ConnectionTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenGivingNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(null, A.Fake<Activity>(), A.Fake<BodyPart>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenActivity_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<BodyPart>(), null, A.Fake<BodyPart>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenReceiving_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<BodyPart>(), A.Fake<Activity>(), null);
        }

        [TestMethod]
        public void Ctor_WhenNoNull_Success()
        {
            var connection = new Connection(A.Fake<BodyPart>(), A.Fake<Activity>(), A.Fake<BodyPart>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenGivingBodyNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(null, A.Fake<Part>(), A.Fake<Activity>(), A.Fake<Body>(), A.Fake<Part>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenGivingPartNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<Body>(), null, A.Fake<Activity>(), A.Fake<Body>(), A.Fake<Part>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor2_WhenActivityNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<Body>(), A.Fake<Part>(), null, A.Fake<Body>(), A.Fake<Part>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenReceivingBodyNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<Body>(), A.Fake<Part>(), A.Fake<Activity>(), null, A.Fake<Part>());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenReceivingPartNull_ThrowsArgumentNullException()
        {
            var connection = new Connection(A.Fake<Body>(), A.Fake<Part>(), A.Fake<Activity>(), A.Fake<Body>(), null);
        }

        [TestMethod]
        public void Ctor2_WhenBothBodyPartsDisconnected_ThrowsArgumentException()
        {
            var bodies = Helper.GetBodies(
                Tuple.Create("Ignacio", "man"),
                Tuple.Create("Mery", "woman"));

            Action action = () => new Connection(
                bodies.ByName("ignacio"), new Tits(), new Stroke(), bodies.ByName("mery"), new Testicles());

            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void Ctor2_WhenGivingBodyPartDisconnected_ThrowsArgumentException()
        {
            var bodies = Helper.GetBodies(
                Tuple.Create("Ignacio", "man"),
                Tuple.Create("Mery", "woman"));

            Action action = () => new Connection(
                bodies.ByName("ignacio"), new Vagina(), new Penetrate(), bodies.ByName("mery"), new Anus());

            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void Ctor2_WhenReceivingBodyPartDisconnected_ThrowsArgumentException()
        {
            var bodies = Helper.GetBodies(
                Tuple.Create("Ignacio", "man"),
                Tuple.Create("Mery", "woman"));

            Action action = () => new Connection(
                bodies.ByName("ignacio"), new Hand(), new Stroke(), bodies.ByName("mery"), new Testicles());

            action.ShouldThrow<ArgumentException>();
        }

        [TestMethod]
        public void Ctor2_WhenAllBodyPartConnected_Success()
        {
            var bodies = Helper.GetBodies(
                Tuple.Create("Ignacio", "man"),
                Tuple.Create("Mery", "woman"));

            var connection = new Connection(
                bodies.ByName("ignacio"), new Penis(), new Penetrate(), bodies.ByName("mery"), new Anus());
        }

        [TestMethod]
        public void Identifier()
        {
            var givingBody = new Body("pepe", new BodyTemplate("man", new Finger()));
            Part givingPart = new Finger();
            Activity activity = new Stroke();
            var receivingBody = new Body("pepa", new BodyTemplate("woman", new Hand()));
            Part receivingPart = new Hand();
            var connection = new Connection(givingBody, givingPart, activity, receivingBody, receivingPart);

            connection.Identifier().Should().Be("pepe|finger|stroke|pepa|hand");
        }

        [TestMethod]
        public void ToStringTest()
        {
            var givingBody = new Body("pepe", new BodyTemplate("man", new Finger()));
            Part givingPart = new Finger();
            Activity activity = new Stroke();
            var receivingBody = new Body("pepa", new BodyTemplate("woman", new Hand()));
            Part receivingPart = new Hand();
            var connection = new Connection(givingBody, givingPart, activity, receivingBody, receivingPart);

            connection.ToString().Should().Be("pepe.finger stroke pepa.hand");
        }

        [TestMethod]
        public void AbbreviatedTest()
        {
            var bodies = Helper.GetBodies(
                Tuple.Create("Ignacio", "man"),
                Tuple.Create("Mery", "woman"));

            var connection = new Connection(
                bodies.ByName("ignacio"), new Penis(), new Penetrate(), bodies.ByName("mery"), new Vagina());

            Abbreviated(connection, bodies).Should().Be(".p p .va");

            connection = new Connection(
                bodies.ByName("ignacio"), new Tongue(), new Lick(), bodies.ByName("mery"), new Vulva());

            Abbreviated(connection, bodies).Should().Be("i l .vu");

            connection = new Connection(
                bodies.ByName("ignacio"), new Finger(), new Stroke(), bodies.ByName("mery"), new Ass());

            Abbreviated(connection, bodies).Should().Be("i.fi st m.as");
        }

        public static string Abbreviated(
            Connection connection,
            IEnumerable<Body> bodies)
        {
            return Abbreviated(
                connection,
                bodies,
                new PartInitializationService().DefaultItems(),
                new ActivityInitializationService().DefaultItems());
        }

        public static string Abbreviated(
            Connection connection,
            IEnumerable<Body> bodies,
            IEnumerable<Part> parts,
            IEnumerable<Activity> activities)
        {
            var bodyDic =
                ShortcutDictionary.Empty.AddRange(
                    bodies
                    .Select(x => x.UniqueName)
                    .ToArray());

            var partDic =
                ShortcutDictionary.Empty.AddRange(
                    parts
                    .Select(x => x.Name)
                    .ToArray());

            var activityDic =
                ShortcutDictionary.Empty.AddRange(
                    activities
                    .Select(x => x.UniqueName)
                    .ToArray());

            var suitDic = ActivityPartSuitabilityPredicate.GetStandardSuitabilityDictionary();

            string givingBody =
                bodies.Count(x => x.Parts.Contains(connection.GivingPart)) == 1
                ? string.Empty
                : connection.GivingBody.UniqueName.ShortcutAt(bodyDic);
            string givingPart =
                suitDic.Get(connection.Activity).GivingParts.Count() == 1
                ? string.Empty
                : "." + connection.GivingPart.Name.ShortcutAt(partDic);
            string activity = connection.Activity.UniqueName.ShortcutAt(activityDic);
            string receivingBody =
                bodies.Count(x => x.Parts.Contains(connection.ReceivingPart)) == 1
                ? string.Empty
                : connection.ReceivingBody.UniqueName.ShortcutAt(bodyDic);
            string receivingPart =
                suitDic.Get(connection.Activity).ReceivingParts.Count() == 1
                ? string.Empty
                : "." + connection.ReceivingPart.Name.ShortcutAt(partDic);
            return $"{givingBody}{givingPart} {activity} {receivingBody}{receivingPart}";
        }
    }
}