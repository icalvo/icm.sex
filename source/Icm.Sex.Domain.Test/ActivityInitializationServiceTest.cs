﻿namespace Icm.Sex.Domain.Test
{
    using System.Diagnostics.CodeAnalysis;
    using FluentAssertions;
    using Icm.Sex.Domain.Activities.Services;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class ActivityInitializationServiceTest
    {
        [TestMethod]
        [ExcludeFromCodeCoverage]
        public void EnsureActivitiesInRepositoryTest()
        {
            var service = new ActivityInitializationService();
            service.DefaultItems().Should().HaveCount(6);
        }
    }
}