﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Icm.Sex.Language.Test
{
    using System.Linq;
    using FluentAssertions;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.BodyTemplates.Services;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;

    [TestClass]
    public class TranslationServiceTests
    {
        [TestMethod]
        public void OrgasmDescription_Test()
        {
            var templates = new BodyTemplateInitializationService().DefaultItems().ToList();

            var bodies = new[]
            {
                new Body("Ignacio", templates.ByName("man")),
                new Body("Mery", templates.ByName("woman"))
            };

            var svc = new TranslationService(new LocalizationRepo(), 3082);

            var actual = svc.OrgasmDescription(
                new Connection(bodies.ByName("ignacio"), new Finger(), new Stroke(), bodies.ByName("mery"), new Vulva()));

            actual.Should().Be("acariciando la vulva de Mery con el dedo");
        }
    }
}
