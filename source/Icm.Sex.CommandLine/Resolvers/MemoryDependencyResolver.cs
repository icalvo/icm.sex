﻿namespace Icm.Sex.CommandLine.Resolvers
{
    using System;
    using Domain.Activities.Repositories;
    using Domain.Activities.Services;
    using Domain.Bodies.Repositories;
    using Domain.BodyTemplates.Repositories;
    using Domain.BodyTemplates.Services;
    using Domain.Parts.Repositories;
    using Domain.Parts.Services;
    using Domain.Scenes.Repositories;
    using Domain.UnitsOfWork;
    using Icm.ContextConsole;
    using Icm.Localization;
    using Icm.Sex.Application.Parser;
    using Icm.Sex.Application.Reporting;
    using Icm.Sex.Domain;
    using Icm.Sex.Infrastructure.Reporting;

    /// <summary>
    /// Dependency resolver for the command line application.
    /// </summary>
    public class MemoryDependencyResolver : IDependencyResolver
    {
        private readonly MemoryBodyRepository bodyRepository;
        private readonly MemoryBodyTemplateRepository bodyTemplateRepository;
        private readonly MemoryActivityRepository activityRepository;
        private readonly ILocalizationRepository localizationRepo;
        private readonly MemoryPartRepository partRepository;
        private readonly MemorySceneRepository sceneRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryDependencyResolver" /> class.
        /// </summary>
        public MemoryDependencyResolver(ILocalizationRepository locRepo)
        {
            bodyRepository = new MemoryBodyRepository();
            bodyTemplateRepository = new MemoryBodyTemplateRepository();
            activityRepository = new MemoryActivityRepository();
            partRepository = new MemoryPartRepository();
            sceneRepository = new MemorySceneRepository();
            localizationRepo = locRepo;

            SetInitialRepositoryState();
        }

        /// <summary>
        /// Clears the request scope.
        /// </summary>
        void IDependencyResolver.ClearRequestScope()
        {
        }

        /// <summary>
        /// Resolves a type by giving an instance of that type.
        /// </summary>
        /// <param name="service">Service asked.</param>
        /// <returns></returns>
        object IDependencyResolver.GetService(Type service)
        {
            if (service == typeof(MainContext))
            {
                var lcid = System.Globalization.CultureInfo.CurrentCulture.LCID;
                var unitOfWork = new MemorySexUnitOfWork(bodyRepository, bodyTemplateRepository, sceneRepository, partRepository, activityRepository);
                var parser = new ParsingService(
                    unitOfWork,
                    new StandardTokenParser(),
                    null);
                var reporter = new DiaryReportService(sceneRepository, bodyRepository, localizationRepo, lcid);
                var printer = new MarkdownReportPrinter();
                var context = new MainContext(parser, reporter, printer, unitOfWork);

                return context;
            }

            return Activator.CreateInstance(service);
        }

        private void SetInitialRepositoryState()
        {
            var unitOfWork = new MemorySexUnitOfWork(
                bodyRepository,
                bodyTemplateRepository,
                sceneRepository,
                partRepository,
                activityRepository);

            var initService = new InitializationService(
                unitOfWork,
                new PartInitializationService(),
                new ActivityInitializationService(),
                new BodyTemplateInitializationService());

            initService.Initialize();

            unitOfWork.Save();
        }
    }
}