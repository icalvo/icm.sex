﻿namespace Icm.Sex.CommandLine
{
    using Icm.ContextConsole;
    using Icm.Sex.CommandLine.Resolvers;
    using Icm.Sex.Language;

    /// <summary>
    ///     Main program
    /// </summary>
    public class Program
    {
        /// <summary>
        ///     Defines the entry point of the application.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            var locRepo = new LocalizationRepo();
            IApplication app = StandardApplication.Create<MainContext>(extLocRepo: locRepo);
            app.ApplicationPrompt = string.Empty;
            app.DependencyResolver = new MemoryDependencyResolver(locRepo);
            app.Run();
        }
    }
}