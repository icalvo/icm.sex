﻿namespace Icm.Sex.CommandLine
{
    using System;
    using Icm.ContextConsole;
    using Icm.Sex.Application.Reporting;
    using Icm.Sex.Infrastructure.Reporting;

    /// <summary>
    ///     Context for parsing tasks.
    /// </summary>
    public class ReportContext : BaseContext
    {
        private readonly IReportService reportingService;
        private readonly IReportPrinter printerService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainContext" /> class.
        /// </summary>
        /// <param name="parsingService">The parsing service.</param>
        /// <param name="reportingService">The reporting service.</param>
        /// <param name="printerService">The printer service.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public ReportContext(IReportService reportingService, IReportPrinter printerService)
        {
            this.printerService = printerService;
            this.reportingService = reportingService;
        }

        public void Diary()
        {
            var report = reportingService.Generate();
            printerService.Print(report, Console.Out);
        }
    }
}