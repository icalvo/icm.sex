﻿namespace Icm.Sex.CommandLine
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Icm.ColorConsole;
    using Icm.ContextConsole;
    using Icm.Sex.Application.Parser;
    using Icm.Sex.Application.Reporting;
    using Icm.Sex.Core;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Predicates;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Connections.Predicates;
    using Icm.Sex.Domain.Connections.Predicates.Suitability;
    using Icm.Sex.Domain.ConnectionSearch;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Predicates;
    using Icm.Sex.Domain.UnitsOfWork;
    using Icm.Sex.Infrastructure.Reporting;

    /// <summary>
    ///     Context for parsing tasks.
    /// </summary>
    public class MainContext : RootContext
    {
        private static readonly IPredicate<Connection> ConnectionPredicate;

        private readonly ParsingService parsingService;
        private readonly IReportService reportingService;
        private readonly IReportPrinter printerService;
        private readonly ISexUnitOfWork unitOfWork;

        static MainContext()
        {
            IPredicate<Connection> suitabilityPredicate = new ActivityPartSuitabilityPredicate();
            IPredicate<Connection> erogenousGivingPartPredicate =
                new IsErogenousPredicate().Transform<Part, Connection>(cnx => cnx.GivingPart);
            IPredicate<Connection> erogenousReceivingPartsPredicate =
                new IsErogenousPredicate().Transform<Part, Connection>(cnx => cnx.ReceivingPart);
            IPredicate<Connection> maleNotPenetratedFilter = new MaleIsPenetratedPredicate(new MaleByTemplatePredicate()).Not();

            ConnectionPredicate = suitabilityPredicate
                .And(
                    maleNotPenetratedFilter,
                    erogenousGivingPartPredicate.Or(erogenousReceivingPartsPredicate));
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainContext" /> class.
        /// </summary>
        /// <param name="parsingService">The parsing service.</param>
        /// <param name="reportingService">The reporting service.</param>
        /// <param name="printerService">The printer service.</param>
        /// <param name="unitOfWork">The unit of work.</param>
        public MainContext(ParsingService parsingService, IReportService reportingService, IReportPrinter printerService, ISexUnitOfWork unitOfWork)
        {
            this.parsingService = parsingService;
            this.printerService = printerService;
            this.unitOfWork = unitOfWork;
            this.reportingService = reportingService;
        }

        /// <summary>
        ///     Returns the credits of the context.
        /// </summary>
        public override void Credits()
        {
            Interactor.ShowMessage("Icm.Sex Console");
            Interactor.ShowMessage("---------------");
        }

        public void DiaryParse()
        {
            parsingService.Options = ParsingOptions.Diary();
            Parse();
        }

        public void FilmParse()
        {
            parsingService.Options = ParsingOptions.Film();
            Parse();
        }

        public void DiaryReport()
        {
            var report = reportingService.Generate();
            printerService.Print(report, Console.Out);
        }

        public void NewScene()
        {
            ISceneBuilder builder = parsingService;
            string title = Interactor.AskString("Title");
            DateTime date = Interactor.AskDate("Date", DateTime.Now).Value;

            builder.AddScene(date, title);

            Body[] bodies =
            {
                new Body("Ignacio", unitOfWork.BodyTemplateRepository.GetById("man")),
                new Body("María", unitOfWork.BodyTemplateRepository.GetById("woman"))
            };

            builder.AddBody(bodies[0]);
            builder.AddBody(bodies[1]);
            var participants = Interactor.AskMany("Participants", "Choose one", bodies);

            bool exitScene;
            do
            {
                builder.AddSituation();
                bool exitSituation;
                do
                {
                    var connectionLine = Interactor.AskString("Connection");
                    builder.AddConnection(connectionLine);
                    exitSituation = Interactor.AskBoolean("More connections?", true).Value;
                }
                while (!exitSituation);

                exitScene = Interactor.AskBoolean("More situations?", true).Value;
            }
            while (!exitScene);
        }

        /// <summary>
        ///     Connection search demo.
        /// </summary>
        public void Connections()
        {
            Activity[] activities =
            {
                new Penetrate(),
                new Suck(),
                new Stroke(),
                new Kiss(),
                new Lick(),
            };
            IPredicate<Connection> suitabilityPredicate = new ActivityPartSuitabilityPredicate();
            IPredicate<Connection> erogenousGivingPartPredicate =
                new IsErogenousPredicate().Transform<Part, Connection>(cnx => cnx.GivingPart);
            IPredicate<Connection> erogenousReceivingPartsPredicate =
                new IsErogenousPredicate().Transform<Part, Connection>(cnx => cnx.ReceivingPart);

            var cnxPred = suitabilityPredicate
                .And(
                    erogenousGivingPartPredicate.Or(erogenousReceivingPartsPredicate),
                    new FunctionPredicate<Connection>(cnx => !Equals(cnx.Activity, new Kiss()) || cnx.ReceivingPart.Equals(new Mouth())));

            Connections(activities, cnxPred);

            var cnxPred2 = suitabilityPredicate
                .And(
                    erogenousGivingPartPredicate.Not(),
                    erogenousReceivingPartsPredicate.Not(),
                    new FunctionPredicate<Connection>(cnx => !Equals(cnx.Activity, new Kiss()) || cnx.ReceivingPart.Equals(new Mouth())));

            Connections(activities, cnxPred2);
        }

        private void Connections(Activity[] activities, IPredicate<Connection> cnxPred)
        {
            Interactor.ShowList("Activities", activities);
            Interactor.ShowMessage(string.Empty);
            Interactor.ShowMessage(cnxPred.Name());
            Console.ReadLine();

            var man = new Body("man", unitOfWork.BodyTemplateRepository.GetById("man"));
            var woman = new Body("woman", unitOfWork.BodyTemplateRepository.GetById("woman"));
            var man1 = new Body("man1", unitOfWork.BodyTemplateRepository.GetById("man"));
            var woman1 = new Body("woman1", unitOfWork.BodyTemplateRepository.GetById("woman"));
            var man2 = new Body("man2", unitOfWork.BodyTemplateRepository.GetById("man"));
            var woman2 = new Body("woman2", unitOfWork.BodyTemplateRepository.GetById("woman"));

            var connections =
                Connections2(man, woman, activities, cnxPred)
                    .Concat(
                        Connections2(
                            woman,
                            man,
                            activities,
                            cnxPred.And(
                                new FunctionPredicate<Connection>(
                                    cnx =>
                                        !cnxPred.Evaluate(
                                            new Connection(cnx.ReceivingBodyPart, cnx.Activity, cnx.GivingBodyPart))))))
                    .Concat(Connections2(man1, man2, activities, cnxPred))
                    .Concat(Connections2(woman1, woman2, activities, cnxPred)).ToList();

            Interactor.ShowList("Connections", connections, cnx => cnx.FileIdentifier());
            Interactor.ShowMessage("Total: " + connections.Count);
        }

        private IEnumerable<Connection> Connections2(Body givingBody, Body receivingBody, Activity[] activities, IPredicate<Connection> cnxPred)
        {
            return ConnectionSearchService.GetConnections(
                givingBody,
                receivingBody,
                activities,
                cnxPred).ToList();
        }

        /// <summary>
        ///     Situation search demo.
        /// </summary>
        public void SituationSearch()
        {
            Body[] bodies =
            {
                new Body("Ignacio", unitOfWork.BodyTemplateRepository.GetById("man")),
                new Body("María", unitOfWork.BodyTemplateRepository.GetById("woman"))
            };

            Activity[] activities =
            {
                new Penetrate(),
                new Suck(),
                new Stroke(),
            };

            IPredicate<IEnumerable<Connection>> partsConnectedOnce = new BodyPartsUsedOnlyOncePredicate().Enum();
            var holesPenetratedOnce = new HoleBodyPartsPenetratedOnlyOncePredicate().Enum();

            IPredicate<IEnumerable<Connection>> situationPredicate = partsConnectedOnce.And(holesPenetratedOnce);

            Interactor.ShowList("Bodies", bodies);
            Interactor.ShowList("Activities", activities);

            Interactor.ShowMessage(string.Empty);
            Interactor.ShowMessage(ConnectionPredicate.Name());

            Interactor.ShowMessage(string.Empty);
            Interactor.ShowMessage(situationPredicate.Name());

            Console.ReadLine();

            IList<Connection> connections = ConnectionSearchService.GetConnections(
                bodies,
                activities,
                ConnectionPredicate).ToList();

            foreach (Connection connection in connections)
            {
                Interactor.ShowMessage(connection.ToString());
            }

            Interactor.ShowMessage(connections.Count() + " possible connections found");

            Console.ReadLine();

            Interactor.ShowMessage($"Evaluating {Math.Pow(2, connections.Count())} possible situations...");

            IList<Connection> testingConnection = connections.WhereByBitmap("00000001010101000001");
            situationPredicate.DoTest(testingConnection);

            Interactor.ShowMessage(string.Join("\r\n", testingConnection));
            Interactor.ShowMessage(string.Join("\r\n", situationPredicate.Reasons()));

            IEnumerable<Tuple<BitArray, IEnumerable<Connection>>> situations = ConnectionSearchService
                .GetWorkingSituations(
                    connections.ToList(),
                    situationPredicate);

            int count = 0;
            foreach (var situation in situations)
            {
                string bitArrayToString = BitArrayToString(situation.Item1);

                Interactor.ShowMessage("Situation " + bitArrayToString);
                foreach (Connection connection in situation.Item2)
                {
                    Interactor.ShowMessage("  " + connection);
                }

                count++;
            }

            Interactor.ShowMessage(count + " possible situations found");
        }

        /// <summary>
        ///     Parses a file.
        /// </summary>
        private void Parse()
        {
            string file = Interactor.AskString("File to parse");

            parsingService.ParseFileName(file);

            foreach (ParseMessage message in parsingService.Messages)
            {
                if (message is ParseError)
                {
                    ColorConsole.WriteLineError(message.Message);
                }
                else if (message is ParseAction)
                {
                    ColorConsole.WriteLineInfo(message.Message);
                }
                else if (message is ParseInfo)
                {
                    ColorConsole.WriteLineInfo(message.Message);
                }
                else
                {
                    ColorConsole.WriteLineWarning(message.Message);
                }
            }

            int lcid = CultureInfo.CurrentCulture.LCID;

            ////this.printerService.Generate(Console.Out);

            var reportFile = new StreamWriter("result.markdown");

            ////this.printerService.Generate(reportFile);

            reportFile.Close();
        }

        private string BitArrayToString(BitArray bitArray)
        {
            var builder = new StringBuilder(bitArray.Length);
            for (int i = 0; i < bitArray.Length; i++)
            {
                builder.Append(bitArray[i] ? "1" : "0");
            }

            return builder.ToString();
        }
    }
}