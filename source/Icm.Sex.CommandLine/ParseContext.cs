﻿namespace Icm.Sex.CommandLine
{
    using System.Collections;
    using System.Globalization;
    using System.IO;
    using System.Text;
    using Icm.ColorConsole;
    using Icm.ContextConsole;
    using Icm.Sex.Application.Parser;

    /// <summary>
    ///     Context for parsing tasks.
    /// </summary>
    public class ParseContext : BaseContext
    {
        private readonly ParsingService parsingService;

        /// <summary>
        ///     Initializes a new instance of the <see cref="MainContext" /> class.
        /// </summary>
        /// <param name="parsingService">The parsing service.</param>
        public ParseContext(ParsingService parsingService)
        {
            this.parsingService = parsingService;
        }

        public void Diary()
        {
            parsingService.Options = ParsingOptions.Diary();
            Parse();
        }

        public void Film()
        {
            parsingService.Options = ParsingOptions.Film();
            Parse();
        }

        /// <summary>
        ///     Parses a file.
        /// </summary>
        private void Parse()
        {
            string file = Interactor.AskString("File to parse");

            parsingService.ParseFileName(file);

            foreach (ParseMessage message in parsingService.Messages)
            {
                if (message is ParseError)
                {
                    ColorConsole.WriteLineError(message.Message);
                }
                else if (message is ParseAction)
                {
                    ColorConsole.WriteLineInfo(message.Message);
                }
                else if (message is ParseInfo)
                {
                    ColorConsole.WriteLineInfo(message.Message);
                }
                else
                {
                    ColorConsole.WriteLineWarning(message.Message);
                }
            }

            ////int lcid = CultureInfo.CurrentCulture.LCID;

            ////this.printerService.Generate(Console.Out);

            var reportFile = new StreamWriter("result.markdown");

            ////this.printerService.Generate(reportFile);

            reportFile.Close();
        }

        private string BitArrayToString(BitArray bitArray)
        {
            var builder = new StringBuilder(bitArray.Length);
            for (int i = 0; i < bitArray.Length; i++)
            {
                builder.Append(bitArray[i] ? "1" : "0");
            }

            return builder.ToString();
        }
    }
}