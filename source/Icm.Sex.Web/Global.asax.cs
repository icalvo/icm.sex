﻿namespace Icm.Sex.Web
{
    using System.Web.Mvc;
    using System.Web.Routing;
    using Icm.Sex.Domain;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Activities.Services;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Services;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Parts.Services;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.UnitsOfWork;
    using Icm.Sex.Web.DependencyResolvers;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            MemoryInitialize();
        }

        private static void MemoryInitialize()
        {
            var bodyRepository = new MemoryBodyRepository();
            var bodyTemplateRepository = new MemoryBodyTemplateRepository();
            var activityRepository = new MemoryActivityRepository();
            var partRepository = new MemoryPartRepository();
            var sceneRepository = new MemorySceneRepository();

            using (var unitOfWork = new MemorySexUnitOfWork(bodyRepository, bodyTemplateRepository, sceneRepository, partRepository, activityRepository))
            {
                Initialize(unitOfWork);
            }

            ControllerBuilder.Current.SetControllerFactory(new MemoryControllerFactory(
                new Language.LocalizationRepo(),
                bodyRepository,
                bodyTemplateRepository,
                sceneRepository,
                partRepository,
                activityRepository));
        }

        private static void Initialize(ISexUnitOfWork unitOfWork)
        {
            var initService = new InitializationService(
                                unitOfWork,
                                new PartInitializationService(),
                                new ActivityInitializationService(),
                                new BodyTemplateInitializationService());

            initService.Initialize();
        }
    }
}