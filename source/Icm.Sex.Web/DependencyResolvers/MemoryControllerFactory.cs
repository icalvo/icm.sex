﻿namespace Icm.Sex.Web.DependencyResolvers
{
    using System;
    using System.Web.Mvc;
    using Icm.Localization;
    using Icm.Sex.Application.Parser;
    using Icm.Sex.Application.Reporting;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Repositories;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.UnitsOfWork;
    using Icm.Sex.Infrastructure.Reporting;
    using Icm.Sex.Web.Controllers;

    public class MemoryControllerFactory : DefaultControllerFactory
    {
        private readonly ILocalizationRepository localizationRepo;

        private readonly MemoryBodyRepository bodyRepository;
        private readonly MemoryBodyTemplateRepository bodyTemplateRepository;
        private readonly MemoryActivityRepository activityRepository;
        private readonly MemoryPartRepository partRepository;
        private readonly MemorySceneRepository sceneRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyResolver"/> class.
        /// </summary>
        public MemoryControllerFactory(
            ILocalizationRepository localizationRepo,
            MemoryBodyRepository bodyRepository,
            MemoryBodyTemplateRepository bodyTemplateRepository,
            MemorySceneRepository sceneRepository,
            MemoryPartRepository partRepository,
            MemoryActivityRepository activityRepository)
        {
            this.activityRepository = activityRepository;
            this.partRepository = partRepository;
            this.sceneRepository = sceneRepository;
            this.bodyTemplateRepository = bodyTemplateRepository;
            this.bodyRepository = bodyRepository;
            this.localizationRepo = localizationRepo;
        }

        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            var unitOfWork = new MemorySexUnitOfWork(bodyRepository, bodyTemplateRepository, sceneRepository, partRepository, activityRepository);

            if (controllerType == typeof(HomeController))
            {
                return new HomeController(unitOfWork.BodyRepository, unitOfWork.SceneRepository);
            }

            if (controllerType == typeof(ReportController))
            {
                var lcid = System.Globalization.CultureInfo.CurrentCulture.LCID;

                var diaryReporter = new DiaryReportService(unitOfWork.SceneRepository, unitOfWork.BodyRepository, localizationRepo, lcid);
                var filmReporter = new FilmReportService(unitOfWork.SceneRepository, unitOfWork.BodyRepository, localizationRepo, lcid);
                var printer = new MarkdownReportPrinter();
                var controller = new ReportController(diaryReporter, filmReporter, printer);

                return controller;
            }

            if (controllerType == typeof(ParseController))
            {
                var diaryParser = new ParsingService(unitOfWork, new StandardTokenParser(), ParsingOptions.Diary());
                var filmParser = new ParsingService(unitOfWork, new StandardTokenParser(), ParsingOptions.Film());
                var controller = new ParseController(diaryParser, filmParser);

                return controller;
            }

            if (controllerType == null)
            {
                return null;
            }

            return (IController)Activator.CreateInstance(controllerType);
        }
    }
}