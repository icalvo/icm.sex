﻿namespace Icm.Sex.Web.ViewModels
{
    using System.Collections.Generic;
    using Icm.Sex.Domain.Scenes;

    public class SceneListViewModel : ApplicationViewModel
    {
        public SceneListViewModel(string pageTitle)
            : base(pageTitle)
        {
        }

        public IList<Scene> Scenes { get; set; }
    }
}