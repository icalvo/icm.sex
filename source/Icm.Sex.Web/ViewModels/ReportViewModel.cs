﻿namespace Icm.Sex.Web.ViewModels
{
    using Icm.Sex.Infrastructure.Reporting;

    public class ReportViewModel : ApplicationViewModel
    {
        public ReportViewModel(string pageTitle)
            : base(pageTitle)
        {
        }

        public string HtmlMarkdown { get; set; }

        public Report Data { get; set; }
    }
}