﻿namespace Icm.Sex.Web.ViewModels
{
    using System.Collections.Generic;

    public class HomeViewModel : ApplicationViewModel
    {
        public HomeViewModel(string pageTitle)
            : base(pageTitle)
        {
            MessagesForUser = new List<string>();
        }

        public int ScenesCount { get; set; }

        public int BodiesCount { get; set; }
    }
}