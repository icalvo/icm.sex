﻿namespace Icm.Sex.Web.ViewModels
{
    using System.Collections.Generic;

    public abstract class ApplicationViewModel
    {
        protected ApplicationViewModel(string pageTitle)
        {
            MessagesForUser = new List<string>();
            PageTitle = pageTitle;
        }

        public string PageTitle { get; set; }

        public IList<string> MessagesForUser { get; set; }
    }
}