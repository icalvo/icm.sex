﻿namespace Icm.Sex.Web.Controllers
{
    using System.Linq;
    using System.Web.Mvc;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Web.ViewModels;

    public class HomeController : Controller
    {
        private readonly IBodyRepository bodyRepository;
        private readonly ISceneRepository sceneRepository;

        public HomeController(IBodyRepository bodyRepository, ISceneRepository sceneRepository)
        {
            this.bodyRepository = bodyRepository;
            this.sceneRepository = sceneRepository;
        }

        /// <summary>
        /// Default home page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(new HomeViewModel("Home")
            {
                BodiesCount = bodyRepository.Count(),
                ScenesCount = sceneRepository.Count(),
            });
        }
    }
}