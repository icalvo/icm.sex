﻿namespace Icm.Sex.Web.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using Icm.Sex.Application.Parser;

    public class ParseController : Controller
    {
        private readonly ParsingService diaryParsingService;
        private readonly ParsingService filmParsingService;

        public ParseController(ParsingService diaryParsingService, ParsingService filmParsingService)
        {
            this.diaryParsingService = diaryParsingService;
            this.filmParsingService = filmParsingService;
        }

        /// <summary>
        /// Action for parsing a file with diary syntax.
        /// </summary>
        /// <param name="virtualFilename"></param>
        /// <returns></returns>
        public ActionResult Diary(string virtualFilename)
        {
            if (virtualFilename.Contains('/') || virtualFilename.Contains('\\'))
            {
                throw new ArgumentException("virtualFilename");
            }

            string fileName = Server.MapPath("~/bin/App_Data/" + virtualFilename);
            diaryParsingService.ParseFileName(fileName);

            return View("ParseResult", diaryParsingService.Messages);
        }

        /// <summary>
        /// Action for parsing a file with film syntax.
        /// </summary>
        /// <param name="virtualFilename"></param>
        /// <returns></returns>
        public ActionResult Film(string virtualFilename)
        {
            if (virtualFilename.Contains('/') || virtualFilename.Contains('\\'))
            {
                throw new ArgumentException("virtualFilename");
            }

            string fileName = Server.MapPath("~/bin/App_Data/" + virtualFilename);
            filmParsingService.ParseFileName(fileName);

            return View("ParseResult", filmParsingService.Messages);
        }
    }
}