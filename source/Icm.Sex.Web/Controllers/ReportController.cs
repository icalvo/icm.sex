﻿namespace Icm.Sex.Web.Controllers
{
    using System.IO;
    using System.Web.Mvc;
    using Icm.Sex.Application.Reporting;
    using Icm.Sex.Infrastructure.Reporting;
    using Icm.Sex.Web.ViewModels;

    public class ReportController : Controller
    {
        private readonly DiaryReportService diaryReportService;
        private readonly FilmReportService filmReportService;
        private readonly IReportPrinter printer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportController" /> class.
        /// </summary>
        /// <param name="reportingService">The reporting service.</param>
        public ReportController(DiaryReportService reportingService, FilmReportService filmReportService, IReportPrinter printer)
        {
            diaryReportService = reportingService;
            this.filmReportService = filmReportService;
            this.printer = printer;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ActionResult Diary()
        {
            return Report(diaryReportService);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public ActionResult Film()
        {
            return Report(filmReportService);
        }

        private ActionResult Report(IReportService reportService)
        {
            var viewModel = new ReportViewModel(null);

            var reportData = reportService.Generate();

            viewModel.Data = reportData;

            var writer = new StringWriter();

            printer.Print(reportData, writer);

            //// var markdownSharp = new MarkdownSharp.Markdown();
            //// viewModel.HtmlMarkdown = markdownSharp.Transform(writer.ToString());
            viewModel.HtmlMarkdown = writer.ToString();
            return View("Report", viewModel);
        }
    }
}