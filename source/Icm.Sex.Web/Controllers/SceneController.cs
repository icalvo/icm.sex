﻿using System.Web.Mvc;

namespace Icm.Sex.Web.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Web.ViewModels;

    public class SceneController : Controller
    {
        private readonly ISceneRepository sceneRepository;

        public SceneController(ISceneRepository sceneRepository)
        {
            this.sceneRepository = sceneRepository;
        }

        [Route("scenes")]
        public ActionResult Index()
        {
            IList<Scene> scenes = sceneRepository.ToList();
            return View(new SceneListViewModel("Scenes") { Scenes = scenes });
        }
    }
}