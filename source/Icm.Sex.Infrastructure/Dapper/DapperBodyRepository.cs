namespace Icm.Sex.Infrastructure.Dapper
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::Dapper;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Repositories;

    public class DapperBodyRepository : IBodyRepository
    {
        private readonly IDbConnection cnx;
        private readonly IDbTransaction tran;

        public DapperBodyRepository(IDbConnection cnx, IDbTransaction tran)
        {
            this.cnx = cnx;
            this.tran = tran;
        }

        public IEnumerator<Body> GetEnumerator()
        {
            return cnx.Query("select * from Bodies", transaction: tran)
                .Select(x => new Body(x.name, null))
                .GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Body entity)
        {
            cnx.Execute(
                @"insert into Bodies (id, name) values (@id, @UniqueName)",
                new { id = entity.UniqueName, entity.Name },
                tran);
        }

        public Body GetById(string key)
        {
            throw new System.NotImplementedException();
        }

        public bool Contains(Body entity)
        {
            throw new System.NotImplementedException();
        }

        public bool ContainsId(string key)
        {
            throw new System.NotImplementedException();
        }

        public void Delete(Body entity)
        {
            throw new System.NotImplementedException();
        }
    }
}