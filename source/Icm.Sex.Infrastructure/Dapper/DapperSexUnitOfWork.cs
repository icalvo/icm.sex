﻿namespace Icm.Sex.Infrastructure.Dapper
{
    using System.Data;
    using System.Data.SqlClient;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.BodyTemplates.Repositories;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.UnitsOfWork;

    public class DapperSexUnitOfWork : ISexUnitOfWork
    {
        private IDbTransaction tran;
        private readonly SqlConnection cnx;

        public DapperSexUnitOfWork()
        {
            cnx = new SqlConnection();
            cnx.Open();
            tran = cnx.BeginTransaction();
            BodyRepository = new DapperBodyRepository(cnx, tran);
        }

        public void Save()
        {
            tran.Commit();
            tran = cnx.BeginTransaction();
        }

        public void Dispose()
        {
            tran.Rollback();
            cnx.Close();
            tran.Dispose();
            cnx.Dispose();
        }

        public IBodyRepository BodyRepository { get; }

        public IBodyTemplateRepository BodyTemplateRepository { get; }

        public ISceneRepository SceneRepository { get; }

        public IPartRepository PartRepository { get; }

        public IActivityRepository ActivityRepository { get; }

        public ISceneLabelRepository SceneLabelRepository { get; }
    }
}
