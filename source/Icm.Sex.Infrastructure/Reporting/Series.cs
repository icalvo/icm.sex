﻿namespace Icm.Sex.Infrastructure.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;

    /// <summary>
    /// Graph series.
    /// </summary>
    public class Series
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
        /// <param name="label">The label.</param>
        public Series(string label)
        {
            Label = label;
            Points = new List<SeriesPoint>();
            Properties = new Dictionary<string, string>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Series"/> class.
        /// </summary>
        /// <param name="label">The label.</param>
        public Series(string label, IEnumerable<SeriesPoint> points)
        {
            Label = label;
            Points = new List<SeriesPoint>(points);
            Properties = new Dictionary<string, string>();
        }

        public string SeriesColor
        {
            get
            {
                Random generator = new Random(Label.GetHashCode());

                Color color = Color.FromArgb(generator.Next(256), generator.Next(256), generator.Next(256));

                return ColorTranslator.ToHtml(color);
            }
        }

        public string Label { get; }

        /// <summary>
        /// Gets the series points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public IList<SeriesPoint> Points { get; }

        public IDictionary<string, string> Properties { get; private set; }

        public static Series SinglePoint(string name, double value)
        {
            var result = new Series(name);

            result.Points.Add(SeriesPoint.At(value, value));

            return result;
        }

        public static Series GroupCountPoint<T>(IGrouping<string, T> @group)
        {
            return SinglePoint(@group.Key, @group.Count());
        }
    }
}