﻿namespace Icm.Sex.Infrastructure.Reporting
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Report composed of a list of subreports.
    /// </summary>
    public class CompositeReport : Report, IEnumerable<Report>
    {
        private readonly IList<Report> children;

        public CompositeReport(string name)
            : base(name)
        {
            children = new List<Report>();
        }

        public CompositeReport(string name, IEnumerable<Report> children)
            : base(name)
        {
            this.children = new List<Report>(children);
        }

        public void AddChild(Report child)
        {
            children.Add(child);
        }

        public Report GetChild(string name)
        {
            return children.SingleOrDefault(child => child.Name == name);
        }

        IEnumerator<Report> IEnumerable<Report>.GetEnumerator()
        {
            return children.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return children.GetEnumerator();
        }
    }
}