﻿namespace Icm.Sex.Infrastructure.Reporting
{
    public class SeriesPoint
    {
        private SeriesPoint(double x, double y)
        {
            X = x;
            Y = y;
        }

        public double X { get; }

        public double Y { get; }

        public static SeriesPoint At(double x, double y)
        {
            return new SeriesPoint(x, y);
        }

        public static SeriesPoint At(int x, int y)
        {
            return new SeriesPoint(x, y);
        }

        public override string ToString()
        {
            return $"{X}, {Y}";
        }
    }
}
