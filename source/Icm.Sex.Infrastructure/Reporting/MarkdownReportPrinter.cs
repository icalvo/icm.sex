﻿namespace Icm.Sex.Infrastructure.Reporting
{
    using System;
    using System.IO;
    using System.Linq;

    public class MarkdownReportPrinter : IReportPrinter
    {
        private TextWriter output;
        private int level;

        /// <summary>
        /// Initializes a new instance of the <see cref="MarkdownReportPrinter"/> class.
        /// </summary>
        public MarkdownReportPrinter()
        {
        }

        public void Print(Report report, TextWriter output)
        {
            this.output = output;
            level = 1;

            Print(report);
        }

        private void Print(Report report)
        {
            Header(report.Name);
            CompositeReport compositeReport = report as CompositeReport;
            if (compositeReport != null)
            {
                level += 1;
                foreach (var subreport in compositeReport)
                {
                    Print(subreport);
                }

                level -= 1;
                return;
            }

            Chart chart = report as Chart;
            if (chart != null)
            {
                if (chart.Series.All(series => series.Points.Count == 1))
                {
                    foreach (var series in chart.Series)
                    {
                        output.WriteLine("- {0}: {1}", series.Label, series.Points.First().X);
                    }

                    output.WriteLine();
                }
                else
                {
                    foreach (var series in chart.Series)
                    {
                        output.WriteLine("- " + series.Label);

                        foreach (var point in series.Points)
                        {
                            output.WriteLine("  - {0}, {1}", point.X, point.Y);
                        }
                    }

                    output.WriteLine();
                }

                return;
            }

            throw new Exception("Unrecognized report type: " + report.GetType().Name);
        }

        private void Header(string text)
        {
            string hashes = new string('#', level);
            output.WriteLine("{0} {1}", hashes, text);
            output.WriteLine();
        }
    }
}