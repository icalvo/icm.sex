﻿namespace Icm.Sex.Infrastructure.Reporting
{
    public abstract class Report
    {
        protected Report(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}