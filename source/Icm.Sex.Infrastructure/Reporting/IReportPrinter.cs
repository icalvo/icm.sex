﻿namespace Icm.Sex.Infrastructure.Reporting
{
    using System.IO;

    public interface IReportPrinter
    {
        void Print(Report report, TextWriter output);
    }
}