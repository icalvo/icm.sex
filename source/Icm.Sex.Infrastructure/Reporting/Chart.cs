﻿namespace Icm.Sex.Infrastructure.Reporting
{
    using System.Collections.Generic;
    using System.Linq;

    public class Chart : Report
    {
        public Chart(string name)
            : base(name)
        {
            Series = new List<Series>();
            Properties = new Dictionary<string, string>();
        }

        public Chart(string name, IEnumerable<Series> singlePoints)
            : base(name)
        {
            Series = new List<Series>(singlePoints);
            Properties = new Dictionary<string, string>();
        }

        public static Chart FromGroupingList<T>(string name, IEnumerable<IGrouping<string, T>> groupEnumerable)
        {
            return new Chart(name, groupEnumerable.Select(Reporting.Series.GroupCountPoint));
        }

        public IList<Series> Series { get; private set; }

        public IDictionary<string, string> Properties { get; private set; }
    }
}