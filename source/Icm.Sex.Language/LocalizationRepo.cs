﻿namespace Icm.Sex.Language
{
    using System.Collections.Generic;
    using Icm.Localization;

    /// <summary>
    /// Repository for translation strings in this assembly.
    /// </summary>
    public class LocalizationRepo : DictionaryLocalizationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LocalizationRepo"/> class.
        /// </summary>
        public LocalizationRepo()
            : base(new Dictionary<LocalizationKey, string>
                {
                    { new LocalizationKey("Body_man_Genre", 1033), "Masculine" },
                    { new LocalizationKey("Body_woman_Genre", 1033), "Feminine" },
                    ////
                    { new LocalizationKey("Body_man_Genre", 3082), "Masculine" },
                    { new LocalizationKey("Body_woman_Genre", 3082), "Feminine" },
                    ////
                    { new LocalizationKey("Activity_Kiss", 1033), "kiss" },
                    { new LocalizationKey("Activity_Lick", 1033), "lick" },
                    { new LocalizationKey("Activity_Penetrate", 1033), "penetrat" },
                    { new LocalizationKey("Activity_Slap", 1033), "slap" },
                    { new LocalizationKey("Activity_Stroke", 1033), "strok" },
                    { new LocalizationKey("Activity_Suck", 1033), "suck" },
                    ////
                    { new LocalizationKey("Activity_Kiss_Gerund", 3082), "besando" },
                    { new LocalizationKey("Activity_Lick_Gerund", 3082), "lamiendo" },
                    { new LocalizationKey("Activity_Penetrate_Gerund", 3082), "penetrando" },
                    { new LocalizationKey("Activity_Slap_Gerund", 3082), "palmeando" },
                    { new LocalizationKey("Activity_Stroke_Gerund", 3082), "acariciando" },
                    { new LocalizationKey("Activity_Suck_Gerund", 3082), "chupando" },
                    ////
                    { new LocalizationKey("Activity_Kiss_PastParticiple_Masculine", 3082), "besado" },
                    { new LocalizationKey("Activity_Lick_PastParticiple_Masculine", 3082), "lamido" },
                    { new LocalizationKey("Activity_Penetrate_PastParticiple_Masculine", 3082), "penetrado" },
                    { new LocalizationKey("Activity_Slap_PastParticiple_Masculine", 3082), "palmeado" },
                    { new LocalizationKey("Activity_Stroke_PastParticiple_Masculine", 3082), "acariciado" },
                    { new LocalizationKey("Activity_Suck_PastParticiple_Masculine", 3082), "chupado" },
                    ////
                    { new LocalizationKey("Activity_Kiss_PastParticiple_Feminine", 3082), "besada" },
                    { new LocalizationKey("Activity_Lick_PastParticiple_Feminine", 3082), "lamida" },
                    { new LocalizationKey("Activity_Penetrate_PastParticiple_Feminine", 3082), "penetrada" },
                    { new LocalizationKey("Activity_Slap_PastParticiple_Feminine", 3082), "palmeada" },
                    { new LocalizationKey("Activity_Stroke_PastParticiple_Feminine", 3082), "acariciada" },
                    { new LocalizationKey("Activity_Suck_PastParticiple_Feminine", 3082), "chupada" },
                    ////
                    { new LocalizationKey("Part_Abdomen", 1033), "abdomen" },
                    { new LocalizationKey("Part_Anus", 1033), "anus" },
                    { new LocalizationKey("Part_Face", 1033), "face" },
                    { new LocalizationKey("Part_Finger", 1033), "finger" },
                    { new LocalizationKey("Part_Hand", 1033), "hand" },
                    { new LocalizationKey("Part_Mouth", 1033), "mouth" },
                    { new LocalizationKey("Part_Nipples", 1033), "nipples" },
                    { new LocalizationKey("Part_Penis", 1033), "penis" },
                    { new LocalizationKey("Part_Testicles", 1033), "testicles" },
                    { new LocalizationKey("Part_Tits", 1033), "tits" },
                    { new LocalizationKey("Part_Tongue", 1033), "tongue" },
                    { new LocalizationKey("Part_Vagina", 1033), "vagina" },
                    { new LocalizationKey("Part_Vulva", 1033), "vulva" },
                    ////
                    { new LocalizationKey("Part_Abdomen", 3082), "abdomen" },
                    { new LocalizationKey("Part_Anus", 3082), "ano" },
                    { new LocalizationKey("Part_Face", 3082), "cara" },
                    { new LocalizationKey("Part_Finger", 3082), "dedo" },
                    { new LocalizationKey("Part_Hand", 3082), "mano" },
                    { new LocalizationKey("Part_Mouth", 3082), "boca" },
                    { new LocalizationKey("Part_Nipples", 3082), "pezones" },
                    { new LocalizationKey("Part_Penis", 3082), "pene" },
                    { new LocalizationKey("Part_Testicles", 3082), "testículos" },
                    { new LocalizationKey("Part_Tits", 3082), "tetas" },
                    { new LocalizationKey("Part_Tongue", 3082), "lengua" },
                    { new LocalizationKey("Part_Vagina", 3082), "vagina" },
                    { new LocalizationKey("Part_Vulva", 3082), "vulva" },
                    ////
                    { new LocalizationKey("Part_Abdomen_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Anus_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Face_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Finger_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Hand_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Mouth_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Nipples_Possesive", 3082), "sus" },
                    { new LocalizationKey("Part_Penis_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Testicles_Possesive", 3082), "sus" },
                    { new LocalizationKey("Part_Tits_Possesive", 3082), "sus" },
                    { new LocalizationKey("Part_Tongue_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Vagina_Possesive", 3082), "su" },
                    { new LocalizationKey("Part_Vulva_Possesive", 3082), "su" },
                    ////
                    { new LocalizationKey("Part_Abdomen_Determinate", 3082), "el" },
                    { new LocalizationKey("Part_Anus_Determinate", 3082), "el" },
                    { new LocalizationKey("Part_Face_Determinate", 3082), "la" },
                    { new LocalizationKey("Part_Finger_Determinate", 3082), "el" },
                    { new LocalizationKey("Part_Hand_Determinate", 3082), "la" },
                    { new LocalizationKey("Part_Mouth_Determinate", 3082), "la" },
                    { new LocalizationKey("Part_Nipples_Determinate", 3082), "los" },
                    { new LocalizationKey("Part_Penis_Determinate", 3082), "el" },
                    { new LocalizationKey("Part_Testicles_Determinate", 3082), "los" },
                    { new LocalizationKey("Part_Tits_Determinate", 3082), "las" },
                    { new LocalizationKey("Part_Tongue_Determinate", 3082), "la" },
                    { new LocalizationKey("Part_Vagina_Determinate", 3082), "la" },
                    { new LocalizationKey("Part_Vulva_Determinate", 3082), "la" },
                    ////
                    { new LocalizationKey("Part_Abdomen_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Anus_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Face_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Finger_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Hand_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Mouth_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Nipples_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Penis_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Testicles_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Tits_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Tongue_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Vagina_Possesive", 1033), "{owner}" },
                    { new LocalizationKey("Part_Vulva_Possesive", 1033), "{owner}" },
                    ////
                    { new LocalizationKey("Article_Determinate", 1033), "the" },
                    ////
                    { new LocalizationKey("Report_Orgasms_Breakdown", 1033), "Orgasms breakdown" },
                    { new LocalizationKey("Report_Non_Solo_Orgasms_Breakdown", 1033), "Non-solo orgasms breakdown" },
                    { new LocalizationKey("Report_Non_Solo_Ejaculations_Breakdown", 1033), "Non-solo ejaculations breakdown" },
                    { new LocalizationKey("Report_Non_Solo_Connections_Frequency", 1033), "Connection types" },
                    { new LocalizationKey("Report_Non_Solo_Orgasm_Simultaneity_Breakdown", 1033), "Simultaneity of non-solo orgasms" },
                    { new LocalizationKey("Report_Non_Solo_Scenes_Breakdown", 1033), "Non-solo scene categories breakdown" },
                    ////
                    { new LocalizationKey("Report", 3082), "Informe" },
                    { new LocalizationKey("Report_Scenes_By_Participants_Hourly_Breakdown", 3082), "Escenas por participantes y hora del día" },
                    { new LocalizationKey("Report_Scenes_By_Participants_Monthly_Breakdown", 3082), "Escenas por participantes y mes" },
                    { new LocalizationKey("Report_Scenes_By_Participants_Weekly_Breakdown", 3082), "Escenas por participantes y día de la semana" },
                    { new LocalizationKey("Report_Scenes_By_Participants_Breakdown", 3082), "Escenas por participantes" },
                    ////
                    { new LocalizationKey("Report_Orgasms_Breakdown", 3082), "Orgasmos" },
                    { new LocalizationKey("Report_Non_Solo_Orgasms_Breakdown", 3082), "Orgasmos en compañía" },
                    { new LocalizationKey("Report_Non_Solo_Ejaculations_Breakdown", 3082), "Eyaculaciones en compañía" },
                    { new LocalizationKey("Report_Non_Solo_Connections_Frequency", 3082), "Tipos de conexión" },
                    { new LocalizationKey("Report_Non_Solo_Orgasm_Simultaneity_Breakdown", 3082), "Simultaneidad de orgasmos en compañía" },
                    { new LocalizationKey("Report_Non_Solo_Scenes_Breakdown", 3082), "Escenas por categoría" },
                    ////
                    { new LocalizationKey("Ejaculation_Over_1", 1033), "over {0}" },
                    { new LocalizationKey("Ejaculation_In_1", 1033), "in {0}" },
                    { new LocalizationKey("BodyPart_2", 1033), "{1} of {0}" },
                    { new LocalizationKey("Report_Body_Orgasms_2", 1033), "{0}: {1} orgasms" },
                    { new LocalizationKey("Report_Orgasm_Count_By_Type_2", 1033), "{0} by {1}" },
                    { new LocalizationKey("Report_Simultaneous_Orgasm_Situations_1", 1033), "Simultaneous: {0} situations" },
                    { new LocalizationKey("Report_Not_Simultaneous_Orgasm_Situations_1", 1033), "Non-simultaneous: {0} situations" },
                    ////
                    { new LocalizationKey("Ejaculation_Over_1", 3082), "sobre {0}" },
                    { new LocalizationKey("Ejaculation_In_1", 3082), "en {0}" },
                    { new LocalizationKey("BodyPart_2", 3082), "{1} de {0}" },
                    { new LocalizationKey("Report_Body_Orgasms_2", 3082), "{0}: {1} orgasmos" },
                    { new LocalizationKey("Report_Orgasm_Count_By_Type_2", 3082), "{0} {1}" },
                    { new LocalizationKey("Report_Simultaneous_Orgasm_Situations_1", 3082), "{0} situaciones con orgasmo simultáneo" },
                    { new LocalizationKey("Report_Not_Simultaneous_Orgasm_Situations_1", 3082), "{0} situaciones con orgasmo no simultáneo" },
                    ////
                    { new LocalizationKey("Connection_Reflective_Default_2", 1033), "{0}ing {1}" },
                    { new LocalizationKey("Connection_Reflective_NonDefault_3", 1033), "{0}ing {1} with {2}" },
                    { new LocalizationKey("Connection_Giving_GivingDefault_ReceivingDefault_2", 1033), "{0}ing {1}" },
                    { new LocalizationKey("Connection_Giving_GivingDefault_3", 1033), "{0}ing {1}'s {2}" },
                    { new LocalizationKey("Connection_Giving_ReceivingDefault_3", 1033), "{0}ing {1} with {2}" },
                    { new LocalizationKey("Connection_Giving_NonDefault_4", 1033), "{0}ing {1}'s {2} with {3}" },
                    { new LocalizationKey("Connection_Receiving_GivingDefault_ReceivingDefault_2", 1033), "being {0}ed by {1}" },
                    { new LocalizationKey("Connection_Receiving_GivingDefault_3", 1033), "having {2} {0}ed by {1}" },
                    { new LocalizationKey("Connection_Receiving_ReceivingDefault_3", 1033), "being {0}ed by {1}'s {2}" },
                    { new LocalizationKey("Connection_Receiving_NonDefault_4", 1033), "having {3} {0}ed by {1}'s {2}" },
                    ////
                    { new LocalizationKey("Connection_Reflective_Default_2", 3082), "{0} {1}" },
                    { new LocalizationKey("Connection_Reflective_NonDefault_3", 3082), "{0} {1} con {2}" },
                    { new LocalizationKey("Connection_Giving_GivingDefault_ReceivingDefault_2", 3082), "{0} a {1}" },
                    { new LocalizationKey("Connection_Giving_GivingDefault_3", 3082), "{0} {2} de {1}" },
                    { new LocalizationKey("Connection_Giving_ReceivingDefault_3", 3082), "{0} a {1} con {2}" },
                    { new LocalizationKey("Connection_Giving_NonDefault_4", 3082), "{0} {2} de {1} con {3}" },
                    { new LocalizationKey("Connection_Receiving_GivingDefault_ReceivingDefault_2", 3082), "{0} por {1}" },
                    { new LocalizationKey("Connection_Receiving_GivingDefault_3", 3082), "{0} en {2} por {1}" },
                    { new LocalizationKey("Connection_Receiving_ReceivingDefault_3", 3082), "{0} por {2} de {1}" },
                    { new LocalizationKey("Connection_Receiving_NonDefault_4", 3082), "{0} en {3} por {2} de {1}" },
                    ////
                    { new LocalizationKey("Report_Category_Anal", 3082), "Anal" },
                    { new LocalizationKey("Report_Category_Oral", 3082), "Oral" },
                    { new LocalizationKey("Report_Category_Straight", 3082), "Estándar" },
                    ////
                    { new LocalizationKey("Report_Category_Anal", 1033), "Anal" },
                    { new LocalizationKey("Report_Category_Oral", 1033), "Oral" },
                    { new LocalizationKey("Report_Category_Straight", 1033), "Straight" },
                })
        {
        }
    }
}
