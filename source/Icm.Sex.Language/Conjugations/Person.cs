namespace Icm.Sex.Language.Conjugations
{
    /// <summary>
    /// Person of a verbal conjugation.
    /// </summary>
    public enum Person
    {
        /// <summary>
        /// Undefined person
        /// </summary>
        None = 0,

        /// <summary>
        /// The first person
        /// </summary>
        First,

        /// <summary>
        /// The second person
        /// </summary>
        Second,

        /// <summary>
        /// The third person
        /// </summary>
        Third
    }
}