namespace Icm.Sex.Language.Conjugations
{
    /// <summary>
    /// Number of a word.
    /// </summary>
    public enum Number
    {
        /// <summary>
        /// Undefined number
        /// </summary>
        None = 0,

        /// <summary>
        /// The singular
        /// </summary>
        Singular,

        /// <summary>
        /// The plural
        /// </summary>
        Plural
    }
}