namespace Icm.Sex.Language.Conjugations
{
    /// <summary>
    /// Verbal form, including non-finite and finite tenses.
    /// </summary>
    public enum Form
    {
        /// <summary>
        /// Undefined form
        /// </summary>
        None = 0,

        /// <summary>
        /// The present
        /// </summary>
        Present,

        /// <summary>
        /// The infinitive
        /// </summary>
        Infinitive,

        /// <summary>
        /// The gerund
        /// </summary>
        Gerund,

        /// <summary>
        /// The past participle
        /// </summary>
        PastParticiple
    }
}