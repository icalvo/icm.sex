﻿namespace Icm.Sex.Language.Conjugations
{
    using System;
    using System.Collections.Generic;
    using Icm.Collections;

    /// <summary>
    /// A verbal conjugation.
    /// </summary>
    public class Conjugation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Conjugation"/> class. Builds an infinitive or gerund conjugation.
        /// </summary>
        /// <param name="form">The form.</param>
        public Conjugation(Form form)
        {
            Form = form;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Conjugation"/> class. Build a past participle conjugation with genre.
        /// </summary>
        /// <param name="genre">The genre.</param>
        public Conjugation(Genre genre)
        {
            Form = Form.PastParticiple;
            Genre = genre;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Conjugation"/> class. Build a normal conjugation with person and number.
        /// </summary>
        /// <param name="person">The person.</param>
        /// <param name="number">The number.</param>
        public Conjugation(Person person, Number number)
        {
            Form = Form.Present;
            Person = person;
            Number = number;
        }

        /// <summary>
        /// Gets or sets the form.
        /// </summary>
        /// <value>
        /// The form.
        /// </value>
        public Form Form { get; set; }

        /// <summary>
        /// Gets or sets the person.
        /// </summary>
        /// <value>
        /// The person.
        /// </value>
        public Person Person { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public Number Number { get; set; }

        /// <summary>
        /// Gets or sets the genre.
        /// </summary>
        /// <value>
        /// The genre.
        /// </value>
        public Genre Genre { get; set; }

        /// <summary>
        /// Suffixes this instance.
        /// </summary>
        /// <returns></returns>
        public string Suffix()
        {
            var parts = new List<string>();
            if (Form != Form.None)
            {
                parts.Add(Enum.GetName(typeof(Form), Form));
            }

            if (Person != Person.None)
            {
                parts.Add(Enum.GetName(typeof(Person), Person));
            }

            if (Number != Number.None)
            {
                parts.Add(Enum.GetName(typeof(Number), Number));
            }

            if (Genre != Genre.None)
            {
                parts.Add(Enum.GetName(typeof(Genre), Genre));
            }

            return parts.JoinStr("_");
        }
    }
}
