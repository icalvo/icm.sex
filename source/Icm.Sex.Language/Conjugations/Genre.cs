namespace Icm.Sex.Language.Conjugations
{
    /// <summary>
    /// Genre of a word.
    /// </summary>
    public enum Genre
    {
        /// <summary>
        /// Undefined genre
        /// </summary>
        None = 0,

        /// <summary>
        /// The masculine genre
        /// </summary>
        Masculine,

        /// <summary>
        /// The feminine genre
        /// </summary>
        Feminine,

        /// <summary>
        /// The neutral genre
        /// </summary>
        Neutral
    }
}