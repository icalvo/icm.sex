﻿namespace Icm.Sex.Language
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Localization;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Exceptions;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Predicates;
    using Icm.Sex.Domain.Situations;
    using Icm.Sex.Language.Conjugations;

    /// <summary>
    /// Translation service for the sex domain.
    /// </summary>
    public class TranslationService : ILocalizationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TranslationService"/> class.
        /// </summary>
        /// <param name="localizationRepo">The localization repo.</param>
        /// <param name="lcid">The lcid.</param>
        public TranslationService(ILocalizationRepository localizationRepo, int lcid)
        {
            Repository = localizationRepo;
            Lcid = lcid;
        }

        /// <summary>
        /// Gets the lcid.
        /// </summary>
        /// <value>
        /// The lcid.
        /// </value>
        public int Lcid { get; }

        /// <summary>
        /// Gets the localization repository.
        /// </summary>
        /// <value>
        /// The localization repository.
        /// </value>
        public ILocalizationRepository Repository { get; }

        /// <summary>
        /// Conjugates the specified activity with the specified conjugation.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <param name="conjugation">The conjugation.</param>
        /// <returns></returns>
        public string Conjugate(Activity activity, Conjugation conjugation)
        {
            string activityTypeName = activity.GetType().Name;
            string translation = this.Trans("Activity_" + activityTypeName + "_" + conjugation.Suffix());
            if (translation == null)
            {
                return "Activity_" + activityTypeName + "_" + conjugation.Suffix();
            }

            return translation;
        }

        /// <summary>
        /// Translates the specified part.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns></returns>
        public string Trans(Part part)
        {
            string partTypeName = part.GetType().Name;
            string translation = this.Trans("Part_" + partTypeName);
            if (translation == null)
            {
                return partTypeName.ToLower();
            }

            return translation;
        }

        /// <summary>
        /// Gets the genre of a body.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public Genre GetGenre(Body body)
        {
            string genreName = this.Trans("Body_" + body.UniqueName + "_Genre");
            if (genreName != null)
            {
                return (Genre)Enum.Parse(typeof(Genre), genreName);
            }

            genreName = this.Trans("Body_" + body.Template.UniqueName + "_Genre");
            if (genreName != null)
            {
                return (Genre)Enum.Parse(typeof(Genre), genreName);
            }

            return Genre.Neutral;
        }

        /// <summary>
        /// Translates a part with a matching determinate article.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns></returns>
        public string TransDet(Part part)
        {
            string partTypeName = part.GetType().Name;
            string pronoun = GetDeterminate($"Part_{partTypeName}");
            string translation = $"{pronoun} {this.Trans("Part_" + partTypeName)}";

            return translation;
        }

        /// <summary>
        /// Translates a part with a matching possessive article.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public string TransPossess(Part part, Body body)
        {
            string partTypeName = part.GetType().Name;
            string pronoun = this.Trans($"Part_{partTypeName}_Possesive");
            if (pronoun == "{owner}")
            {
                switch (GetGenre(body))
                {
                    case Genre.Masculine:
                        pronoun = "his";
                        break;

                    case Genre.Feminine:
                        pronoun = "her";
                        break;

                    case Genre.Neutral:
                        pronoun = "its";
                        break;
                }
            }

            string translation = $"{pronoun} {this.Trans("Part_" + partTypeName)}";

            return translation;
        }

        /// <summary>
        /// Description of an ejaculation.
        /// </summary>
        /// <param name="bodyParts">The ejaculated body parts.</param>
        /// <returns></returns>
        public string EjaculationDescription(IEnumerable<BodyPart> bodyParts)
        {
            var blocks = new List<object>();
            var isHolePredicate = new IsHolePredicate();
            IPredicate<BodyPart> bodyPartIsHole = isHolePredicate.Transform<Part, BodyPart>(bp => bp.Part);
            List<BodyPart> bodyPartsList = bodyParts.ToList();
            IPredicate<BodyPart> bodyPartIsNotHole = bodyPartIsHole.Not();

            if (bodyPartsList.Any(bodyPartIsNotHole.Evaluate))
            {
                IEnumerable<PhraseFormat> phraseFormats = bodyPartsList
                    .Where(bodyPartIsNotHole.Evaluate)
                    .Select(bodyPart =>
                        new PhraseFormat(
                            "BodyPart",
                            bodyPart.Body.Name,
                            TransDet(bodyPart.Part)));
                var phraseFormat = new PhraseFormat("Ejaculation_Over", new PhraseAnd(phraseFormats.ToArray()));
                blocks.Add(phraseFormat);
            }

            if (bodyPartsList.Any(bodyPartIsHole.Evaluate))
            {
                IEnumerable<PhraseFormat> phraseFormats = bodyPartsList
                    .Where(bodyPartIsHole.Evaluate)
                    .Select(bodyPart =>
                        new PhraseFormat(
                            "BodyPart",
                            bodyPart.Body.Name,
                            TransDet(bodyPart.Part)));
                var phraseFormat = new PhraseFormat(
                    "Ejaculation_In",
                    new PhraseAnd(phraseFormats.ToArray()));
                blocks.Add(phraseFormat);
            }

            Phrase resultPhrase = new PhraseAnd(blocks.ToArray());
            return resultPhrase.Translate(Lcid, Repository);
        }

        /// <summary>
        /// Lexical root of the specified activity.
        /// </summary>
        /// <param name="activity">The activity.</param>
        /// <returns></returns>
        public string Root(Activity activity)
        {
            return activity.GetType().Name.ToLower();
        }

        public string OrgasmDescription(Connection connection)
        {
            return OrgasmDescription(connection, connection.GivingBody);
        }

        /// <summary>
        /// Description of an connection that produces an orgasm, given the connection and the orgasming body.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        /// <exception cref="BodyNotFoundException">Cannot describe a connection as an orgasm cause for a body that doesn't belong to the connection</exception>
        /// <exception cref="ConnectionCannotCauseOrgasmException"></exception>
        private string OrgasmDescription(Connection connection, Body body)
        {
            if (!connection.GivingBody.Equals(body) && !connection.ReceivingBody.Equals(body))
            {
                throw new BodyNotFoundException("Cannot describe a connection as an orgasm cause for a body that doesn't belong to the connection");
            }

            // Debug.Assert(cnx.CanCauseOrgasmTo(body), "A connection should not be described as an orgasm cause if it cannot cause orgasm to the body");
            string activityGerund = Conjugate(connection.Activity, new Conjugation(Form.Gerund));
            string activityParticiple = Conjugate(connection.Activity, new Conjugation(GetGenre(connection.ReceivingBody)));
            if (connection.GivingBody.Equals(body) && connection.ReceivingBody.Equals(body))
            {
                return connection.IsGivingWithDefaultPart()
                    ? this.TransF("Connection_Reflective_Default", activityGerund, TransPossess(connection.ReceivingPart, body))
                    : this.TransF("Connection_Reflective_NonDefault", activityGerund, TransPossess(connection.ReceivingPart, body), TransDet(connection.GivingPart));
            }

            if (connection.GivingBody.Equals(body))
            {
                if (connection.IsGivingWithDefaultPart())
                {
                    if (connection.IsReceivingWithDefaultPart())
                    {
                        return this.TransF("Connection_Giving_GivingDefault_ReceivingDefault", activityGerund, connection.ReceivingBody.Name);
                    }
                    else
                    {
                        return this.TransF("Connection_Giving_GivingDefault", activityGerund, connection.ReceivingBody.Name, TransDet(connection.ReceivingPart));
                    }
                }
                else
                {
                    if (connection.IsReceivingWithDefaultPart())
                    {
                        return this.TransF("Connection_Giving_ReceivingDefault", activityGerund, connection.ReceivingBody.Name, TransDet(connection.GivingPart));
                    }
                    else
                    {
                        return this.TransF("Connection_Giving_NonDefault", activityGerund, connection.ReceivingBody.Name, TransDet(connection.ReceivingPart), TransDet(connection.GivingPart));
                    }
                }
            }

            if (connection.ReceivingBody.Equals(body))
            {
                if (connection.IsGivingWithDefaultPart())
                {
                    if (connection.IsReceivingWithDefaultPart())
                    {
                        return this.TransF("Connection_Receiving_GivingDefault_ReceivingDefault", activityParticiple, connection.GivingBody.Name);
                    }
                    else
                    {
                        return this.TransF("Connection_Receiving_GivingDefault", activityParticiple, connection.GivingBody.Name, TransDet(connection.ReceivingPart));
                    }
                }
                else
                {
                    if (connection.IsReceivingWithDefaultPart())
                    {
                        return this.TransF("Connection_Receiving_ReceivingDefault", activityParticiple, connection.GivingBody.Name, TransDet(connection.GivingPart));
                    }
                    else
                    {
                        return this.TransF("Connection_Receiving_NonDefault", activityParticiple, connection.GivingBody.Name, TransDet(connection.GivingPart), TransDet(connection.ReceivingPart));
                    }
                }
            }

            throw new ConnectionCannotCauseOrgasmException();
        }

        /// <summary>
        ///     Can this activity cause an orgasm for the supplied body?
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        private IPredicate<Connection> CanCauseOrgasmTo(Body body)
        {
            IPredicate<Body> bodyEquals = new EqualsPredicate<Body>(body);

            IPredicate<Connection> givingBodyEquals = bodyEquals.Transform<Body, Connection>(cnx => cnx.GivingBody);
            IPredicate<Connection> receivingBodyEquals = bodyEquals.Transform<Body, Connection>(cnx => cnx.ReceivingBody);

            IPredicate<Part> isErogenous = new IsErogenousPredicate();

            IPredicate<Connection> givingIsErogenous = isErogenous.Transform<Part, Connection>(cnx => cnx.GivingPart);
            IPredicate<Connection> receivingIsErogenous = isErogenous.Transform<Part, Connection>(cnx => cnx.ReceivingPart);

            IPredicate<Connection> predicate = givingBodyEquals.And(givingIsErogenous).Or(
                receivingBodyEquals.And(receivingIsErogenous));

            return predicate;
        }

        /// <summary>
        /// Description of situation with that produces an orgasm, given the situation and the orgasming body.
        /// </summary>
        /// <param name="situation">The situation.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public string OrgasmDescription(Situation situation, Body body)
        {
            IPredicate<Connection> canCauseOrgasmTo = CanCauseOrgasmTo(body);

            var orgasmicConnections = situation
                .ConnectionGroups
                .SelectMany(connectionGroup => connectionGroup.Connections)
                .Where(canCauseOrgasmTo.Evaluate);

            var strings = orgasmicConnections.Select(cnx => OrgasmDescription(cnx, body)).ToArray();

            return this.TransAnd(strings);
        }

        /// <summary>
        /// Gets the determinate article for a given key.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns></returns>
        private string GetDeterminate(string prefix)
        {
            string pronoun = this.Trans($"{prefix}_Determinate");
            if (pronoun == null)
            {
                return this.Trans("Article_Determinate");
            }

            return pronoun;
        }
    }
}