﻿namespace Icm.Sex.Core.Test
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.VisualStudio.TestTools.UnitTesting;


    [TestClass]
    [ExcludeFromCodeCoverage]
    public class TypeExtensionsTests
    {

        [TestMethod]
        public void Implements_WhenImplementorNull_ReturnsFalse()
        {
            Assert.IsFalse(TypeExtensions.Implements(null, typeof(ExampleClass5<,>)));
        }

        [TestMethod]
        public void Implements_WhenTypeNull_ReturnsFalse()
        {
            Assert.IsFalse(typeof(ExampleClass1).Implements(null));
        }

        [TestMethod]
        public void Implements_WhenGeneric0_ReturnsTrue()
        {
            Assert.IsTrue(typeof(ExampleClass6<>).Implements(typeof(ExampleClass1)));
        }

        [TestMethod]
        public void Implements_WhenIsAssignable_ReturnsTrue()
        {
            Assert.IsTrue(typeof(ExampleClass1).Implements(typeof(IExample<string>)));
        }

        [TestMethod]
        public void Implements_WhenIsAssignable_ReturnsFalse()
        {
            Assert.IsFalse(typeof(ExampleClass2).Implements(typeof(IExample<string>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric_ReturnsTrue()
        {
            Assert.IsTrue(typeof(ExampleClass3<>).Implements(typeof(IExample<string>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric2_ReturnsTrue()
        {
            Assert.IsTrue(typeof(ExampleClass4<>).Implements(typeof(ExampleClass3<string>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric3_ReturnsTrue()
        {
            Assert.IsTrue(typeof(ExampleClass5<,>).Implements(typeof(IExample<string>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric_ReturnsFalse()
        {
            Assert.IsFalse(typeof(IExample<string>).Implements(typeof(ExampleClass3<>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric2_ReturnsFalse()
        {
            Assert.IsFalse(typeof(ExampleClass3<string>).Implements(typeof(ExampleClass4<>)));
        }

        [TestMethod]
        public void Implements_WhenGeneric3_ReturnsFalse()
        {
            Assert.IsFalse(typeof(IExample<string>).Implements(typeof(ExampleClass5<,>)));
        }

        private interface IExample<T>
        {
        }

        private class ExampleClass1 : IExample<string>
        {
        }

        private class ExampleClass2 : IExample<int>
        {
        }

        private class ExampleClass3<T> : IExample<T>
        {
        }

        private class ExampleClass4<T> : ExampleClass3<T>
        {
        }

        private class ExampleClass5<T, S> : IExample<T>
        {
        }

        private class ExampleClass6<T> : ExampleClass1
        {
        }

    }
}