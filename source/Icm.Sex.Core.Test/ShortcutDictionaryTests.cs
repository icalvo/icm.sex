namespace Icm.Sex.Core.Test
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class ShortcutDictionaryTests
    {
        [TestMethod]
        public void DifferentInitials_InitialsAreKeys()
        {
            var actual = ShortcutDictionary.Empty.AddRange("war", "deal", "peace");
            var expected = new Dictionary<string, string>
            {
                { "p", "peace" },
                { "d", "deal" },
                { "w", "war" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        [TestMethod]
        public void MatchingInitials_IncorporateSecondCharacter()
        {
            var actual = ShortcutDictionary.Empty.AddRange("world", "war", "peace");
            var expected = new Dictionary<string, string>
            {
                { "p", "peace" },
                { "wo", "world" },
                { "wa", "war" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        [TestMethod]
        public void MoreMatchingInitials_IncorporateEnoughCharacters()
        {
            var actual = ShortcutDictionary.Empty.AddRange("world", "war", "peace", "worm");
            var expected = new Dictionary<string, string>
            {
                { "p", "peace" },
                { "worl", "world" },
                { "worm", "worm" },
                { "wa", "war" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        [TestMethod]
        public void VariedCaps_IncorporateEnoughCharacters()
        {
            var actual = ShortcutDictionary.Empty
                .AddRange("world", "War", "peaCE")
                .Add("wOrm");
            var expected = new Dictionary<string, string>
            {
                { "p", "peaCE" },
                { "worl", "world" },
                { "worm", "wOrm" },
                { "wa", "War" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        private static void AssertEqualDictionaries(
            IReadOnlyDictionary<string, string> expected,
            IReadOnlyDictionary<string, string> actual)
        {
            foreach (var expectedKvp in expected)
            {
                Assert.IsTrue(actual.ContainsKey(expectedKvp.Key), $"Actual should contain expected key '{expectedKvp.Key}'");
                Assert.AreEqual(expectedKvp.Value, actual[expectedKvp.Key], $"Expected actual value for key '{expectedKvp.Key}' to be '{expectedKvp.Value}', but was '{actual[expectedKvp.Key]}' instead");
            }

            foreach (var actualKey in actual.Keys)
            {
                Assert.IsTrue(expected.ContainsKey(actualKey), $"Actual should NOT contain key '{actualKey}'");
            }
        }

        [TestMethod]
        public void ShortcutLength()
        {
            Assert.AreEqual(1, ShortcutDictionary.NewShortcutLength("war", "peace"));
            Assert.AreEqual(2, ShortcutDictionary.NewShortcutLength("war", "world"));
            Assert.AreEqual(4, ShortcutDictionary.NewShortcutLength("world", "worm"));
            Assert.AreEqual(6, ShortcutDictionary.NewShortcutLength("world", "world"));
            Assert.AreEqual(1, ShortcutDictionary.NewShortcutLength("world", null));
        }

        [TestMethod]
        public void AddShortcut()
        {
            var actual = ImmutableDictionary<string, string>.Empty
                .AddShortcut("peaCE", 1)
                .AddShortcut("wOrm", 4)
                .AddShortcut("world", 4)
                .AddShortcut("War", 2);
            var expected = new Dictionary<string, string>
            {
                { "p", "peaCE" },
                { "worl", "world" },
                { "worm", "wOrm" },
                { "wa", "War" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        [TestMethod]
        public void ReplaceShortcut()
        {
            var actual = ImmutableDictionary<string, string>.Empty
                .AddShortcut("world", 2)
                .ReplaceShortcut("wo", "world", 3);
            var expected = new Dictionary<string, string>
            {
                { "wor", "world" },
            };
            AssertEqualDictionaries(expected, actual);
        }

        [TestMethod]
        public void ReplaceShortcut_Nulls()
        {
            var actual = ImmutableDictionary<string, string>.Empty
                .AddShortcut("world", 2)
                .ReplaceShortcut(null, "ignored", 3);
            var expected = new Dictionary<string, string>
            {
                { "wo", "world" },
            };
            AssertEqualDictionaries(expected, actual);
        }
    }
}