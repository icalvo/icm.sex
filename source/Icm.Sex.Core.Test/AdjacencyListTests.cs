﻿namespace Icm.Sex.Core.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Icm.Sex.Core.Graph;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AdjacencyListTests
    {
        [TestMethod]
        public void Ctor1()
        {
            var list = new AdjacencyList<string, FakeEdge>();
            Assert.AreEqual(0, list.Nodes().Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor_WhenNull()
        {
            var list = new AdjacencyList<string, FakeEdge>(null);
        }

        [TestMethod]
        public void Ctor_WhenEmptyArray()
        {
            var list = new AdjacencyList<string, FakeEdge>(new string[0]);
            Assert.AreEqual(0, list.Nodes().Count());
        }

        [TestMethod]
        public void Ctor_WhenNotNull()
        {
            var list = new AdjacencyList<string, FakeEdge>("node");
            Assert.AreEqual(1, list.Nodes().Count());
        }

        [TestMethod]
        public void ContainsNode()
        {
            var list = new AdjacencyList<string, FakeEdge>("node");

            Assert.AreEqual(1, list.Nodes().Count());
            Assert.IsTrue(list.ContainsNode("node"));
        }

        [TestMethod]
        public void AddNode()
        {
            var list = new AdjacencyList<string, FakeEdge>();

            list.AddNode("node");

            Assert.AreEqual(1, list.Nodes().Count());
            Assert.IsTrue(list.ContainsNode("node"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AddNode_SameNodeAgain()
        {
            var list = new AdjacencyList<string, FakeEdge>();

            list.AddNode("node");
            list.AddNode("node");
        }

        [TestMethod]
        public void Clear()
        {
            var list = new AdjacencyList<string, FakeEdge>("node");

            Assert.AreEqual(1, list.Nodes().Count());

            list.Clear();

            Assert.AreEqual(0, list.Nodes().Count());
        }

        [TestMethod]
        public void ForceNode()
        {
            var list = new AdjacencyList<string, FakeEdge>();

            list.ForceNode("node");

            Assert.AreEqual(1, list.Nodes().Count());
            Assert.IsTrue(list.ContainsNode("node"));
        }

        [TestMethod]
        public void ForceNode_SameNodeAgain()
        {
            var list = new AdjacencyList<string, FakeEdge>();

            list.ForceNode("node");
            list.ForceNode("node");

            Assert.AreEqual(1, list.Nodes().Count());
            Assert.IsTrue(list.ContainsNode("node"));
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void AddEdge_NoNodes()
        {
            var list = new AdjacencyList<string, FakeEdge>();

            list.AddEdge(new FakeEdge("node1", "node2"));
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void AddEdge_NoSource()
        {
            var list = new AdjacencyList<string, FakeEdge>("node1");

            list.AddEdge(new FakeEdge("node1", "node2"));
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void AddEdge_NoDestination()
        {
            var list = new AdjacencyList<string, FakeEdge>("node2");

            list.AddEdge(new FakeEdge("node1", "node2"));
        }

        [TestMethod]
        public void AddEdge_AllOk()
        {
            var list = new AdjacencyList<string, FakeEdge>("node1", "node2");

            list.AddEdge(new FakeEdge("node1", "node2"));

            Assert.AreEqual(2, list.Nodes().Count());
            Assert.AreEqual(1, list.Edges().Count());
        }

        [TestMethod]
        public void RemoveNode()
        {
            var list = new AdjacencyList<string, FakeEdge>("node");

            list.RemoveNode("node");

            Assert.AreEqual(0, list.Nodes().Count());
        }

        [TestMethod]
        public void Edges2()
        {
            var list = new AdjacencyList<string, FakeEdge>("node1", "node2", "node3");

            list.AddEdge(new FakeEdge("node1", "node2"));
            list.AddEdge(new FakeEdge("node3", "node2"));
            list.AddEdge(new FakeEdge("node3", "node1"));

            Assert.AreEqual(3, list.Edges().Count());
            Assert.AreEqual(2, list.Edges("node2").Count());
        }

        private class FakeEdge : IEdge<string>
        {
            public FakeEdge(string source, string destination)
            {
                Source = source;
                Destination = destination;
            }

            public string Source { get; }

            public string Destination { get; }
        }
    }
}