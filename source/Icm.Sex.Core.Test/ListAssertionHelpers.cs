﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Icm.Sex.Core.Test
{
    using System.Collections;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    public static class ListAssertionHelpers
    {
        public static void AssertListContents<T>(
            IList<T> collection,
            string collectionText)
        {
            AssertListContentsAux(collection, collectionText, null);
        }

        public static void AssertListContents<T>(
            IList<T> collection,
            Func<T, string> toString,
            string collectionText)
        {
            AssertListContentsAux(collection, collectionText, toString);
        }

        private static void AssertListContentsAux<T>(
            IList<T> collection,
            string collectionText,
            Func<T, string> toString)
        {
            string[] itemStrings = collectionText.Split(
                new[]
                {
                    '\r',
                    '\n'
                },
                StringSplitOptions.RemoveEmptyEntries);

            Assert.AreEqual(collection.Count(), itemStrings.Count(), "The string has not the same number of lines as the collection");

            if (toString == null)
            {
                for (int i = 0; i < collection.Count - 1; i++)
                {
                    Assert.AreEqual(itemStrings[i], collection[i].ToString());
                }
            }
            else
            {
                for (int i = 0; i < collection.Count - 1; i++)
                {
                    Assert.AreEqual(itemStrings[i], toString(collection[i]));
                }
            }
        }

        public static void AssertEnumerableContents<T>(
            IEnumerable<T> collection,
            string collectionText)
        {
            AssertEnumerableContentsAux(collection, collectionText, null);
        }

        public static void AssertEnumerableContents<T>(
            IEnumerable<T> collection,
            Func<T, string> toString,
            string collectionText)
        {
            AssertEnumerableContentsAux(collection, collectionText, toString);
        }

        private static void AssertEnumerableContentsAux<T>(
            IEnumerable<T> collection,
            string collectionText,
            Func<T, string> toString)
        {
            string[] itemStrings = collectionText.Split(
                new[]
                {
                    '\r',
                    '\n'
                },
                StringSplitOptions.RemoveEmptyEntries);

            if (toString == null)
            {
                foreach (var item in collection)
                {
                    Assert.IsTrue(itemStrings.Contains(item.ToString()), $"{item} not found");
                }
            }
            else
            {
                foreach (var item in collection)
                {
                    Assert.IsTrue(itemStrings.Contains(toString(item)));
                }
            }
        }

        public static void AssertVariationContents<T>(
            IEnumerable<Tuple<BitArray, IEnumerable<T>>> collection,
            string collectionText)
        {
            AssertListContents(
                collection.ToList(),
                situation => situation.Item1.To01String() + " " + string.Join(", ", situation.Item2),
                collectionText);
        }

        private static string To01String(this BitArray bitArray)
        {
            var builder = new StringBuilder(bitArray.Length);
            for (int i = 0; i < bitArray.Length; i++)
            {
                builder.Append(bitArray[i] ? "1" : "0");
            }

            return builder.ToString();
        }
    }
}
