﻿namespace Icm.Sex.Core.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Icm.Sex.Core.Predicates;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AndPredicateTests
    {
        [TestMethod]
        public void Ctor_Success()
        {
            var test = new AndPredicate<string>();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor2_WhenNull_Fails()
        {
            var test = new AndPredicate<string>(null);
        }

        [TestMethod]
        public void Ctor2_WhenOK_Success()
        {
            var test = new AndPredicate<string>(new TruePredicate<string>(), new TruePredicate<string>());
        }

        [TestMethod]
        public void Ctor3_WhenOK_Success()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new TruePredicate<string>(), new TruePredicate<string>() };
            var test = new AndPredicate<string>(list);
        }

        [TestMethod]
        public void TrueFor_TrueTrue_WorksAsExpected()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new TruePredicate<string>(), new TruePredicate<string>() };
            var test = new AndPredicate<string>(list);

            Assert.IsTrue(test.TrueFor("value"));
        }

        [TestMethod]
        public void TrueFor_FalseTrue_WorksAsExpected()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new TruePredicate<string>(), new NotPredicate<string>(new TruePredicate<string>()) };
            var test = new AndPredicate<string>(list);

            Assert.IsFalse(test.TrueFor("value"));
        }

        [TestMethod]
        public void TrueFor_TrueFalse_WorksAsExpected()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new NotPredicate<string>(new TruePredicate<string>()), new TruePredicate<string>() };
            var test = new AndPredicate<string>(list);

            Assert.IsFalse(test.TrueFor("value"));
        }

        [TestMethod]
        public void TrueFor_FalseFalse_WorksAsExpected()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new NotPredicate<string>(new TruePredicate<string>()), new NotPredicate<string>(new TruePredicate<string>()) };
            var test = new AndPredicate<string>(list);

            Assert.IsFalse(test.TrueFor("value"));
        }
    }
}