﻿namespace Icm.Sex.Core.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Predicates;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class NotPredicateTests
    {
        [TestMethod]
        public void Ctor_Success()
        {
            var test = new NotPredicate<string>();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Ctor2_WhenNull_Fails()
        {
            var test = new NotPredicate<string>(null);
        }

        [TestMethod]
        public void Ctor2_WhenOK_Success()
        {
            var test = new NotPredicate<string>(new TruePredicate<string>());
        }

        [TestMethod]
        public void TrueFor_True_WorksAsExpected()
        {
            List<IPredicate<string>> list = new List<IPredicate<string>> { new NotPredicate<string>(new TruePredicate<string>()), new TruePredicate<string>() };
            var test = new NotPredicate<string>(new TruePredicate<string>());

            Assert.IsFalse(test.TrueFor("value"));
        }

        [TestMethod]
        public void TrueFor_False_WorksAsExpected()
        {
            var test = new NotPredicate<string>(new NotPredicate<string>(new TruePredicate<string>()));

            Assert.IsTrue(test.TrueFor("value"));
        }
    }
}