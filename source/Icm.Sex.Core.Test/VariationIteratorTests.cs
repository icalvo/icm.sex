﻿namespace Icm.Sex.Core.Test
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class VariationIteratorTests
    {
        [TestMethod]
        public void ReturnsDifferentInstancesWhenIterating()
        {
            var iterator = new VariationIterator<string>(new[] { "A", "B", "C" });

            Tuple<BitArray, IEnumerable<string>> firstElement = iterator.AsEnumerable().ElementAt(0);
            Tuple<BitArray, IEnumerable<string>> secondElement = iterator.AsEnumerable().ElementAt(1);
            Assert.AreNotSame(firstElement.Item1, secondElement.Item1);
            Assert.AreNotSame(firstElement.Item2, secondElement.Item2);
        }

        [TestMethod]
        public void IterationReturnsCorrectVariations()
        {
            string[] strings = { "A", "B", "C" };
            var iterator = new VariationIterator<string>(strings);
            var list = iterator.ToList();

            Assert.AreEqual(Math.Pow(2, strings.Length) - 1, list.Count);
            var expectedVariations = @"
100 A
010 B
110 A, B
001 C
101 A, C
011 B, C
111 A, B, C
";
            ListAssertionHelpers.AssertVariationContents(iterator, expectedVariations);
        }

        [TestMethod]
        public void IterationReturnsCorrectVariations2()
        {
            string[] strings = { "A", "B", "C", "D", "E", "F", "G" };
            var iterator = new VariationIterator<string>(strings);
            var list = iterator.ToList();

            Assert.AreEqual(Math.Pow(2, strings.Length) - 1, list.Count);
        }
    }
}