﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Connection parsing exception.
    /// </summary>
    public class ConnectionParseException : Exception
    {
        public ConnectionParseException(string message)
            : base(message)
        {
        }
    }
}
