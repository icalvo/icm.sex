﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when an activity name is not recognized.
    /// </summary>
    public class UnrecognizedActivityException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnrecognizedActivityException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public UnrecognizedActivityException(string message)
            : base(message)
        {
        }
    }
}
