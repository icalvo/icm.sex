﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when a part is not found (and it should).
    /// </summary>
    public class PartNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PartNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public PartNotFoundException(string message)
            : base(message)
        {
        }
    }
}
