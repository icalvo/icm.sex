﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when a template is not found (and it should).
    /// </summary>
    public class TemplateNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TemplateNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public TemplateNotFoundException(string message)
            : base(message)
        {
        }
    }
}
