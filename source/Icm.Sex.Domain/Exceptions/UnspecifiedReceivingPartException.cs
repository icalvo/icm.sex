﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when no receiving part is specified for a connection.
    /// </summary>
    public class UnspecifiedReceivingPartException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnspecifiedReceivingPartException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public UnspecifiedReceivingPartException(string message)
            : base(message)
        {
        }
    }
}
