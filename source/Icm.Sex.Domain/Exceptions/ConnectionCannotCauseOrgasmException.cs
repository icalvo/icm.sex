﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when a connection cannot cause an orgasm (and it should).
    /// </summary>
    public class ConnectionCannotCauseOrgasmException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionCannotCauseOrgasmException"/> class.
        /// </summary>
        public ConnectionCannotCauseOrgasmException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionCannotCauseOrgasmException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public ConnectionCannotCauseOrgasmException(string message)
            : base(message)
        {
        }
    }
}
