﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception raised when a body already exists (and should not).
    /// </summary>
    public class BodyAlreadyExistsException : Exception
    {
    }
}
