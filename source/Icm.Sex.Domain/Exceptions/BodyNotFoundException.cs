﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception raised when a body is not found (and it should be).
    /// </summary>
    public class BodyNotFoundException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BodyNotFoundException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public BodyNotFoundException(string message)
            : base(message)
        {
        }
    }
}
