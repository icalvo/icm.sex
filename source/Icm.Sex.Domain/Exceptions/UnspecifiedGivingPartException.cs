﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when no giving part is specified for a connection.
    /// </summary>
    public class UnspecifiedGivingPartException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UnspecifiedGivingPartException"/> class.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public UnspecifiedGivingPartException(string message)
            : base(message)
        {
        }
    }
}
