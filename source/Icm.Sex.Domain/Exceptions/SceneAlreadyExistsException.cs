﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception thrown when a scene already exists (and it shouldn't).
    /// </summary>
    public class SceneAlreadyExistsException : Exception
    {
    }
}
