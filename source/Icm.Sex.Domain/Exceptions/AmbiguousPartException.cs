﻿namespace Icm.Sex.Domain.Exceptions
{
    using System;

    /// <summary>
    /// Exception raised when a body is not found (and it should be).
    /// </summary>
    public class AmbiguousPartException : Exception
    {
        public string PartName { get; }

        public string[] BodyNames { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="BodyNotFoundException"/> class.
        /// </summary>
        /// <param name="partName">The name of the part.</param>
        /// <param name="bodyNames">Then bodies that have that part.</param>
        public AmbiguousPartException(string partName, string[] bodyNames)
        {
            PartName = partName;
            BodyNames = bodyNames;
        }

        public override string Message => $"There is not exactly one body with the part {PartName}; instead the following bodies have them: [{string.Join(", ", BodyNames)}]";
    }
}