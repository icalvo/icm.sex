﻿namespace Icm.Sex.Domain.ConnectionSearch
{
    using Icm.Sex.Core.Graph;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Predicate on connections inside a graph.
    /// </summary>
    public class ConnectionPredicate : GraphPredicate<BodyPart, Connection>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectionPredicate"/> class.
        /// </summary>
        /// <param name="test">The test.</param>
        public ConnectionPredicate(IPredicate<Connection, AdjacencyList<BodyPart, Connection>> test)
            : base(test)
        {
        }
    }
}