﻿namespace Icm.Sex.Domain.ConnectionSearch
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Searcher of working situations.
    /// </summary>
    public static class ConnectionSearchService
    {
        /// <summary>
        /// Gets all possible situations for a set of bodies with a set of activities. The definition of 'possible' is given
        /// by a connection predicate and a situation predicate.
        /// </summary>
        /// <param name="bodies">The bodies.</param>
        /// <param name="activities">The activities.</param>
        /// <param name="connectionTest">The connection test.</param>
        /// <param name="situationTest">The situation test.</param>
        /// <returns></returns>
        public static IEnumerable<Tuple<BitArray, IEnumerable<Connection>>> GetAllSituations(
            IEnumerable<Body> bodies,
            IEnumerable<Activity> activities,
            IPredicate<Connection> connectionTest,
            IPredicate<IEnumerable<Connection>> situationTest)
        {
            return GetWorkingSituations(GetConnections(bodies, activities, connectionTest).ToList(), situationTest);
        }

        /// <summary>
        /// Gets the working situations.
        /// </summary>
        /// <param name="connectionSet">The connection set.</param>
        /// <param name="connectionSetTest">The situation test.</param>
        /// <returns></returns>
        public static IEnumerable<Tuple<BitArray, IEnumerable<Connection>>> GetWorkingSituations(
            IList<Connection> connectionSet,
            IPredicate<IEnumerable<Connection>> connectionSetTest)
        {
            return new VariationIterator<Connection>(connectionSet)
                .Where(current => connectionSetTest.Evaluate(current.Item2));
        }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <param name="bodies">The bodies.</param>
        /// <param name="activities">The activities.</param>
        /// <param name="connectionTest">The connection test.</param>
        /// <returns></returns>
        public static IEnumerable<Connection> GetConnections(
            IEnumerable<Body> bodies,
            IEnumerable<Activity> activities,
            IPredicate<Connection> connectionTest)
        {
            IEnumerable<BodyPart> parts = bodies
                .SelectMany(body => body.Parts.Select(part => new BodyPart(body, part)));

            return GetConnections(parts, activities, connectionTest);
        }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <param name="bodies">The bodies.</param>
        /// <param name="activities">The activities.</param>
        /// <param name="connectionTest">The connection test.</param>
        /// <returns></returns>
        public static IEnumerable<Connection> GetConnections(
            Body givingBody,
            Body receivingBody,
            IEnumerable<Activity> activities,
            IPredicate<Connection> connectionTest)
        {
            IEnumerable<BodyPart> givingBodyParts = givingBody.Parts.Select(part => new BodyPart(givingBody, part));
            IEnumerable<BodyPart> receivingBodyParts = receivingBody.Parts.Select(part => new BodyPart(receivingBody, part));

            return GetConnections(givingBodyParts, receivingBodyParts, activities, connectionTest);
        }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <param name="parts">The parts.</param>
        /// <param name="activities">The activities.</param>
        /// <param name="connectionTest">The connection test.</param>
        /// <returns></returns>
        public static IEnumerable<Connection> GetConnections(
            IEnumerable<BodyPart> parts,
            IEnumerable<Activity> activities,
            IPredicate<Connection> connectionTest)
        {
            return GetConnections(parts, parts, activities, connectionTest);
        }

        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <param name="parts">The parts.</param>
        /// <param name="activities">The activities.</param>
        /// <param name="connectionTest">The connection test.</param>
        /// <returns></returns>
        public static IEnumerable<Connection> GetConnections(
            IEnumerable<BodyPart> givingBodyParts,
            IEnumerable<BodyPart> receivingBodyParts,
            IEnumerable<Activity> activities,
            IPredicate<Connection> connectionTest)
        {
            List<BodyPart> givingBodyPartsList = givingBodyParts.ToList();
            List<BodyPart> receivingBodyPartsList = receivingBodyParts.ToList();

            var result =
                from givingBodyPart in givingBodyPartsList
                from receivingBodyPart in receivingBodyPartsList
                from activity in activities
                let cnx = new Connection(givingBodyPart, activity, receivingBodyPart)
                where Eval(connectionTest, cnx)
                select cnx;

            return result;
        }

        private static bool Eval(IPredicate<Connection> connectionTest, Connection cnx)
        {
            return !cnx.GivingBodyPart.Equals(cnx.ReceivingBodyPart) && connectionTest.Evaluate(cnx);
        }
    }
}