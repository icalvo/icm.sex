﻿namespace Icm.Sex.Domain.Situations
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Extensions for <see cref="Situation"/>
    /// </summary>
    public static class SituationExtensions
    {
        /// <summary>
        /// Determines whether the specified situation has a simultaneous orgasm.
        /// </summary>
        /// <param name="situation">The situation.</param>
        /// <returns></returns>
        public static bool HasSimultaneousOrgasm(this Situation situation)
        {
            int bodyCount = situation.Bodies().Count();

            int orgasmingBodyCount = situation.Orgasms.Select(orgasm => orgasm.OrgasmingBody).Distinct().Count();

            return bodyCount == orgasmingBodyCount;
        }

        /// <summary>
        /// Bodies of the specified situation.
        /// </summary>
        /// <param name="situation">The situation.</param>
        /// <returns></returns>
        public static IEnumerable<Body> Bodies(this Situation situation)
        {
            return situation.ConnectionGroups
                            .SelectMany(connectionGroup => connectionGroup.Connections)
                            .SelectMany(connection => connection.Bodies())
                            .Distinct();
        }
    }
}