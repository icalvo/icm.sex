namespace Icm.Sex.Domain.Situations
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Collections;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Exceptions;
    using Icm.Sex.Domain.Orgasms;
    using Icm.Sex.Domain.Scenes;

    /// <summary>
    ///     Set of connections that do not change for a certain span of time.
    /// </summary>
    /// <remarks>
    ///     <para>
    ///         A situation defines a span of time during which only one of the defined connection groups happen at a given
    ///         time.
    ///         All the defined connection groups must happen in a more or less fast or indistinguishable succession.
    ///     </para>
    ///     <para>
    ///         For example: an erotic tale says "The three of us kissed each other for a while". Or, during a porn
    ///         video, three characters kiss each other in such a way that no kiss lasts more than a few seconds.
    ///         In both cases, the first one due to indefinition and the second because of the excessive work of defining a
    ///         different situation for each kiss, it is better to define a single situation with many connection groups,
    ///         each one accounting for a different possible "frame". For example:
    ///     </para>
    ///     <code>
    ///     situation 05:00 -- five minutes
    ///       group
    ///         A kisses B
    ///       group
    ///         A kisses C
    ///       group
    ///         A kisses C
    ///         B kisses A.neck
    ///       group
    ///         B kisses C
    /// </code>
    /// </remarks>
    public class Situation
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Situation" /> class.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <param name="order">The order.</param>
        /// <param name="duration">The duration.</param>
        /// <exception cref="System.ArgumentNullException">The scene.</exception>
        /// <exception cref="System.ArgumentOutOfRangeException">
        ///     order; The order must be greater or equal than 1
        ///     or
        ///     duration; The duration cannot be negative.
        /// </exception>
        internal Situation(Scene scene, int order, TimeSpan duration)
        {
            if (scene == null)
            {
                throw new ArgumentNullException(nameof(scene));
            }

            if (order <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(order), "The order must be greater or equal than 1");
            }

            if (duration.CompareTo(TimeSpan.Zero) < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(duration), "The duration cannot be negative.");
            }

            Scene = scene;
            Order = order;
            ConnectionGroups = new HashSet<ConnectionGroup>();
            Orgasms = new HashSet<Orgasm>();
            Duration = duration;
        }

        protected Situation()
        {
        }

        public virtual Scene Scene { get; protected set; }

        public virtual int SceneId { get; protected set; }

        /// <summary>
        ///     Gets or sets the order of the situation inside the scene.
        /// </summary>
        /// <value>
        ///     The order.
        /// </value>
        public int Order { get; protected set; }

        /// <summary>
        ///     Gets or sets the duration.
        /// </summary>
        /// <value>
        ///     The duration.
        /// </value>
        public TimeSpan Duration { get; protected set; }

        public virtual ICollection<ConnectionGroup> ConnectionGroups { get; protected set; }

        /// <summary>
        ///     Gets or sets the orgasms.
        /// </summary>
        /// <value>
        ///     The orgasms.
        /// </value>
        public virtual ICollection<Orgasm> Orgasms { get; protected set; }

        /// <summary>
        ///     Adds an orgasm.
        /// </summary>
        /// <param name="orgasm">The orgasm.</param>
        /// <exception cref="System.ArgumentNullException">The orgasm.</exception>
        /// <exception cref="BodyNotFoundException">
        ///     Could not add the orgasm because the orgasming body is not in the situation.
        ///     Please add a connection in which the body participates.
        /// </exception>
        public void AddOrgasm(Orgasm orgasm)
        {
            if (orgasm == null)
            {
                throw new ArgumentNullException(nameof(orgasm));
            }

            if (
                !ConnectionGroups.SelectMany(connectionGroup => connectionGroup.Connections)
                    .Any(
                        connection =>
                            connection.GivingBody.Equals(orgasm.OrgasmingBody) ||
                            connection.ReceivingBody.Equals(orgasm.OrgasmingBody)))
            {
                throw new BodyNotFoundException(
                    "Could not add the orgasm because the orgasming body is not in the situation. Please add a connection in which '" +
                    orgasm.OrgasmingBody.UniqueName + "' participates.");
            }

            Orgasms.Add(orgasm);
        }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{ConnectionGroups.JoinStr(",")}/{Orgasms.JoinStr(",")}";
        }
    }
}