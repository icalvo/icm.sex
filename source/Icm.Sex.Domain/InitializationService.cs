﻿namespace Icm.Sex.Domain
{
    using BodyTemplates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Activities.Services;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.UnitsOfWork;

    /// <summary>
    ///     Service for ensuring that needed initial data is in the storages.
    /// </summary>
    public class InitializationService
    {
        private readonly IInitializationService<Activity> activityInitService;
        private readonly IInitializationService<BodyTemplate> bodyTemplateInitService;
        private readonly IInitializationService<Part> partInitService;
        private readonly ISexUnitOfWork unitOfWork;

        /// <summary>
        ///     Initializes a new instance of the <see cref="InitializationService" /> class.
        /// </summary>
        /// <param name="unitOfWork">The unit of work.</param>
        /// <param name="partInitService">The part initialize service.</param>
        /// <param name="activityInitService">The activity initialize service.</param>
        /// <param name="bodyTemplateInitService">The body template initialize service.</param>
        public InitializationService(
            ISexUnitOfWork unitOfWork,
            IInitializationService<Part> partInitService,
            IInitializationService<Activity> activityInitService,
            IInitializationService<BodyTemplate> bodyTemplateInitService)
        {
            this.unitOfWork = unitOfWork;
            this.partInitService = partInitService;
            this.activityInitService = activityInitService;
            this.bodyTemplateInitService = bodyTemplateInitService;
        }

        /// <summary>
        ///     Initializes the repositories.
        /// </summary>
        public void Initialize()
        {
            unitOfWork.PartRepository.EnsureDefaults(partInitService);
            unitOfWork.ActivityRepository.EnsureDefaults(activityInitService);
            unitOfWork.BodyTemplateRepository.EnsureDefaults(bodyTemplateInitService);
        }
    }
}