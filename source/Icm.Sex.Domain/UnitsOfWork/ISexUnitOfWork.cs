﻿namespace Icm.Sex.Domain.UnitsOfWork
{
    using System;
    using BodyTemplates.Repositories;
    using Icm.Sex.Core.UnitOfWork;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Scenes.Repositories;

    /// <summary>
    /// Unit of work for the sex domain.
    /// </summary>
    public interface ISexUnitOfWork : IUnitOfWork, IDisposable
    {
        /// <summary>
        /// Gets the body repository.
        /// </summary>
        /// <value>
        /// The body repository.
        /// </value>
        IBodyRepository BodyRepository { get; }

        /// <summary>
        /// Gets the body template repository.
        /// </summary>
        /// <value>
        /// The body template repository.
        /// </value>
        IBodyTemplateRepository BodyTemplateRepository { get; }

        /// <summary>
        /// Gets the scene repository.
        /// </summary>
        /// <value>
        /// The scene repository.
        /// </value>
        ISceneRepository SceneRepository { get; }

        /// <summary>
        /// Gets the part repository.
        /// </summary>
        /// <value>
        /// The part repository.
        /// </value>
        IPartRepository PartRepository { get; }

        /// <summary>
        /// Gets the activity repository.
        /// </summary>
        /// <value>
        /// The activity repository.
        /// </value>
        IActivityRepository ActivityRepository { get; }

        ISceneLabelRepository SceneLabelRepository { get; }
    }
}