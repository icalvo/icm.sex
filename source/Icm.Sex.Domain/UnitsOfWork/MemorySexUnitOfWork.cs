﻿namespace Icm.Sex.Domain.UnitsOfWork
{
    using BodyTemplates.Repositories;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.Scenes.Repositories;

    /// <summary>
    /// In-memory unit of work for the sex domain.
    /// </summary>
    public class MemorySexUnitOfWork : ISexUnitOfWork
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemorySexUnitOfWork"/> class.
        /// </summary>
        public MemorySexUnitOfWork()
        {
            BodyRepository = new MemoryBodyRepository();
            BodyTemplateRepository = new MemoryBodyTemplateRepository();
            SceneRepository = new MemorySceneRepository();
            PartRepository = new MemoryPartRepository();
            ActivityRepository = new MemoryActivityRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MemorySexUnitOfWork" /> class.
        /// </summary>
        /// <param name="bodyRepository">The body repository.</param>
        /// <param name="bodyTemplateRepository">The body template repository.</param>
        /// <param name="sceneRepository">The scene repository.</param>
        /// <param name="partRepository">The part repository.</param>
        /// <param name="activityRepository">The activity repository.</param>
        public MemorySexUnitOfWork(
            MemoryBodyRepository bodyRepository,
            MemoryBodyTemplateRepository bodyTemplateRepository,
            MemorySceneRepository sceneRepository,
            MemoryPartRepository partRepository,
            MemoryActivityRepository activityRepository)
        {
            BodyRepository = bodyRepository;
            BodyTemplateRepository = bodyTemplateRepository;
            SceneRepository = sceneRepository;
            PartRepository = partRepository;
            ActivityRepository = activityRepository;
        }

        /// <summary>
        /// Gets the body repository.
        /// </summary>
        /// <value>
        /// The body repository.
        /// </value>
        public IBodyRepository BodyRepository { get; }

        /// <summary>
        /// Gets the body template repository.
        /// </summary>
        /// <value>
        /// The body template repository.
        /// </value>
        public IBodyTemplateRepository BodyTemplateRepository { get; }

        /// <summary>
        /// Gets the scene repository.
        /// </summary>
        /// <value>
        /// The scene repository.
        /// </value>
        public ISceneRepository SceneRepository { get; }

        /// <summary>
        /// Gets the part repository.
        /// </summary>
        /// <value>
        /// The part repository.
        /// </value>
        public IPartRepository PartRepository { get; }

        /// <summary>
        /// Gets the activity repository.
        /// </summary>
        /// <value>
        /// The activity repository.
        /// </value>
        public IActivityRepository ActivityRepository { get; }

        public ISceneLabelRepository SceneLabelRepository { get; set; }

        /// <summary>
        /// Saves this instance.
        /// </summary>
        public void Save()
        {
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }
    }
}