﻿namespace Icm.Sex.Domain.Activities.Services
{
    using System.Collections.Generic;
    using System.Reflection;
    using Collections;
    using Reflection;

    /// <summary>
    ///     Service for setting initial activity data.
    /// </summary>
    public abstract class ReflectionInitializationService<T> : IInitializationService<T>
    {
        private readonly IList<Assembly> assemblies;

        protected ReflectionInitializationService(params Assembly[] assemblies)
        {
            this.assemblies = new List<Assembly>
            {
                typeof(T).Assembly
            };

            if (assemblies != null)
            {
                this.assemblies.AddRange(assemblies);
            }
        }

        /// <summary>
        ///     Gets an instance of all the declared activities.
        /// </summary>
        /// <returns>An instance of all the declared activities</returns>
        public IEnumerable<T> DefaultItems()
        {
            return ActivatorTools.GetInstanceListOfAllImplementors<T>(assemblies);
        }
    }
}