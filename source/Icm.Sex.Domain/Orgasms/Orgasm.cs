namespace Icm.Sex.Domain.Orgasms
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// An orgasm, defined by the body that is orgasming and the body parts that will be covered with the ejaculation.
    /// </summary>
    public class Orgasm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Orgasm"/> class.
        /// </summary>
        /// <param name="orgasmingBody">The orgasming body.</param>
        /// <param name="partsEjaculated">The parts ejaculated.</param>
        public Orgasm(Body orgasmingBody, IEnumerable<BodyPart> partsEjaculated)
        {
            OrgasmingBody = orgasmingBody;
            PartsEjaculated = partsEjaculated.Aggregate(
                ImmutableHashSet<BodyPart>.Empty,
                (set, part) => set.Add(part));
        }

        /// <summary>
        /// Gets the orgasming body.
        /// </summary>
        /// <value>
        /// The orgasming body.
        /// </value>
        public Body OrgasmingBody { get; }

        /// <summary>
        /// Gets or sets the parts ejaculated.
        /// </summary>
        /// <value>
        /// The parts ejaculated.
        /// </value>
        public IReadOnlyCollection<BodyPart> PartsEjaculated { get; }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            var result = OrgasmingBody.UniqueName;
            if (PartsEjaculated.Any())
            {
                result += " over " + string.Join(",", PartsEjaculated.Select(bodyPart => bodyPart.ToString()));
            }

            return result;
        }
    }
}