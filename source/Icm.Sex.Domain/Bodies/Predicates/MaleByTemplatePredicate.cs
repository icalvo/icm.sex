namespace Icm.Sex.Domain.Bodies.Predicates
{
    using System.Diagnostics;
    using Icm.Sex.Core.Predicates;

    /// <summary>
    /// Predicate for bodies whose template is 'man'
    /// </summary>
    public class MaleByTemplatePredicate : BasePredicate<Body>
    {
        /// <summary>
        /// Is the predicate true for the supplied body?
        /// </summary>
        /// <param name="body">The body to be tested.</param>
        /// <returns></returns>
        public override bool TrueFor(Body body)
        {
            var maleLoosePredicate = new MaleLoosePredicate();
            var isMaleLoose = maleLoosePredicate.TrueFor(body);
            Debug.Assert(body.Template.UniqueName != "man" || isMaleLoose, "The 'man' body template should comply with MaleLoosePredicate");

            if (isMaleLoose && body.Template.UniqueName == "man")
            {
                EditableTrueReasons.Add(body + "'s body template is 'man'");
                return true;
            }

            EditableFalseReasons.Add(body + "'s body template is not 'man'");
            return false;
        }
    }
}