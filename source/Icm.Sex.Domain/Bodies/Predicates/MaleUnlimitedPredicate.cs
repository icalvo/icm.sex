namespace Icm.Sex.Domain.Bodies.Predicates
{
    using Icm.Sex.Core;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate for detecting a male body, with the only condition of having a penis.
    /// </summary>
    public class MaleUnlimitedPredicate : BasePredicate<Body>
    {
        public override bool TrueFor(Body body)
        {
            if (body.Parts.HasOfType<Penis>())
            {
                EditableTrueReasons.Add(body + " has a penis");
                return true;
            }
            else
            {
                EditableFalseReasons.Add(body + " does not have a penis");
                return false;
            }
        }
    }
}
