namespace Icm.Sex.Domain.Bodies.Predicates
{
    using Icm.Collections;
    using Icm.Sex.Core;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Male loose predicate
    /// </summary>
    public class MaleLoosePredicate : BasePredicate<Body>
    {
        /// <summary>
        /// Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="body">The body to be tested.</param>
        /// <returns></returns>
        public override bool TrueFor(Body body)
        {
            var maleUnlimitedPredicate = new MaleUnlimitedPredicate();
            var isMaleUnlimited = maleUnlimitedPredicate.TrueFor(body);
            if (isMaleUnlimited && !body.Parts.HasOfType<Vagina>())
            {
                EditableTrueReasons.AddRange(maleUnlimitedPredicate.TrueReasons);
                EditableTrueReasons.Add(body + " hasn't a vagina");
                return true;
            }
            else
            {
                EditableFalseReasons.Add(body + " doesn't comply MaleUnlimited or has a vagina");
                return false;
            }
        }
    }
}