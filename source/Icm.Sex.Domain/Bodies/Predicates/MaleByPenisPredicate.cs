namespace Icm.Sex.Domain.Bodies.Predicates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate for detecting a male body, with the only condition of having a penis.
    /// </summary>
    public class MaleByPenisPredicate : BasePredicate<Body>
    {
        public override IEnumerable<string> FalseReasons(Body body)
        {
            return new[] {body.Name + " does not have a penis"};
        }

        public override IEnumerable<string> TrueReasons(Body body)
        {
            return new[] { body.Name + " has a penis" };
        }

        public override bool TrueFor(Body body)
        {
            return body.Parts.HasOfType<Penis>();
        }
    }
}
