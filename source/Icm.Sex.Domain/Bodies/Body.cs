﻿namespace Icm.Sex.Domain.Bodies
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using BodyTemplates;
    using Parts;

    /// <summary>
    /// A concrete body. Any concrete body can also work as a template for other bodies.
    /// </summary>
    public class Body : IUniqueName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Body"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="template">The template.</param>
        public Body(string name, BodyTemplate template)
        {
            Name = name;
            UniqueName = name.ToLowerInvariant();
            Template = template;
            BodyParts = template.Parts.Select(part => new BodyPart(this, part))
                .Aggregate(
                    ImmutableHashSet<BodyPart>.Empty,
                    (set, part) => set.Add(part));
        }

        /// <summary>
        /// Gets or sets the body parts.
        /// </summary>
        /// <value>
        /// The parts.
        /// </value>
        public IReadOnlyCollection<BodyPart> BodyParts { get; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets or sets the body unique name.
        /// </summary>
        /// <value>
        /// The body unique name.
        /// </value>
        public string UniqueName { get; }

        /// <summary>
        /// Gets the parts.
        /// </summary>
        /// <value>
        /// The parts.
        /// </value>
        public IEnumerable<Part> Parts
        {
            get
            {
                return BodyParts.Select(bodyPart => bodyPart.Part);
            }
        }

        /// <summary>
        /// Gets or sets the template.
        /// </summary>
        /// <value>
        /// The template.
        /// </value>
        public BodyTemplate Template { get; }

        /// <summary>
        /// Gets the body part corresponding to the specified part.
        /// </summary>
        /// <param name="part">The part.</param>
        /// <returns></returns>
        public BodyPart GetBodyPart(Part part)
        {
            return BodyParts.SingleOrDefault(bodyPart => bodyPart.Part.Equals(part));
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return UniqueName.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(UniqueName, ((Body)obj).UniqueName);
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{UniqueName} ({Template})";
        }
    }
}