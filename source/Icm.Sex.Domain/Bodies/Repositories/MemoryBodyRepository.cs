﻿namespace Icm.Sex.Domain.Bodies.Repositories
{
    using Icm.Data;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// In-memory body repository.
    /// </summary>
    public class MemoryBodyRepository : MemoryRepository<Body, string>, IBodyRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryBodyRepository"/> class.
        /// </summary>
        public MemoryBodyRepository()
            : base(body => body.UniqueName)
        {
        }
    }
}