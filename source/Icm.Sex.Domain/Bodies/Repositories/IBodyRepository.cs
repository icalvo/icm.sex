﻿namespace Icm.Sex.Domain.Bodies.Repositories
{
    using Icm.Data;

    /// <summary>
    /// Body repository interface.
    /// </summary>
    public interface IBodyRepository : IRepository<Body, string>
    {
    }
}