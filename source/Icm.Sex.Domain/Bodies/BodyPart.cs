namespace Icm.Sex.Domain.Bodies
{
    using System;
    using Icm.Sex.Domain.Exceptions;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// A part associated with its body
    /// </summary>
    public class BodyPart
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BodyPart"/> class.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <param name="part">The part.</param>
        /// <exception cref="System.ArgumentNullException">
        /// body
        /// or
        /// part
        /// </exception>
        /// <exception cref="PartNotFoundException"></exception>
        public BodyPart(Body body, Part part)
        {
            if (body == null)
            {
                throw new ArgumentNullException(nameof(body));
            }

            if (part == null)
            {
                throw new ArgumentNullException(nameof(part));
            }

            Body = body;
            Part = part;
        }

        /// <summary>
        /// Gets or sets the body.
        /// </summary>
        /// <value>
        /// The body.
        /// </value>
        public virtual Body Body { get; }

        /// <summary>
        /// Gets or sets the part.
        /// </summary>
        /// <value>
        /// The part.
        /// </value>
        public virtual Part Part { get; }

        /// <summary>
        /// Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return ToString().Equals(obj.ToString());
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{Body?.UniqueName}.{Part?.Name}";
        }
    }
}