namespace Icm.Sex.Domain.Activities
{
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Extensions;

    /// <summary>
    /// Activity of rubbing one part with another.
    /// The giving part cannot be the tongue (see <see cref="Lick"></see>).
    /// The receiving part cannot be a hole (see <see cref="Penetrate"></see>).
    /// </summary>
    public class Stroke : Activity
    {
        /// <summary>
        /// The default part for the giving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultGivingPart(Body template)
        {
            return template.Parts.FirstOrDefault<Hand>();
        }

        /// <summary>
        /// The default part for the receiving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultReceivingPart(Body template)
        {
            return null;
        }
    }
}