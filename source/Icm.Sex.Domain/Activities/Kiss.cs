﻿namespace Icm.Sex.Domain.Activities
{
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Kissing activity
    /// </summary>
    public class Kiss : Activity
    {
        /// <summary>
        /// The default part for the giving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultGivingPart(Body template)
        {
            return new Mouth();
        }

        /// <summary>
        /// The default part for the receiving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultReceivingPart(Body template)
        {
            return new Mouth();
        }
    }
}
