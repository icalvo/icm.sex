namespace Icm.Sex.Domain.Activities
{
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Extensions;

    /// <summary>
    /// Activity of penetrating a hole with a cylindrical part or object.
    /// </summary>
    public class Penetrate : Activity
    {
        /// <summary>
        /// The default part for the giving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template in which the default part will be searched.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultGivingPart(Body template)
        {
            return template.Parts.SingleOrDefault<Penis>();
        }

        /// <summary>
        /// The default part for the receiving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template in which the default part will be searched.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public override Part DefaultReceivingPart(Body template)
        {
            return template.Parts.FirstOrDefault<Part, Vagina, Rectum>();
        }
    }
}