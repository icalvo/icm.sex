﻿namespace Icm.Sex.Domain.Activities.Services
{
    using Icm.Data;

    public static class RepositoryExtensions
    {
        /// <summary>
        ///     Ensures that all the activities (inheritors of <see cref="Activity" /> are present in the repository.
        /// </summary>
        public static void EnsureDefaults<T>(
            this IRepository<T, string> repo,
            IInitializationService<T> initService)
        {
            foreach (T activity in initService.DefaultItems())
            {
                if (!repo.ContainsId(activity.GetType().Name.ToLower()))
                {
                    repo.Add(activity);
                }
            }
        }
    }
}