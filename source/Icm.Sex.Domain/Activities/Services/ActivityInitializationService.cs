﻿namespace Icm.Sex.Domain.Activities.Services
{
    using System.Reflection;

    public class ActivityInitializationService : ReflectionInitializationService<Activity>
    {
        public ActivityInitializationService(params Assembly[] assemblies)
            : base(assemblies)
        {
        }
    }
}