﻿namespace Icm.Sex.Domain.Activities.Repositories
{
    using Icm.Data;

    /// <summary>
    /// Repository of activities.
    /// </summary>
    public interface IActivityRepository : IRepository<Activity, string>
    {
    }
}