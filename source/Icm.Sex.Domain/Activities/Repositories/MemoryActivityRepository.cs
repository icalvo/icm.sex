﻿namespace Icm.Sex.Domain.Activities.Repositories
{
    using Icm.Data;

    /// <summary>
    /// In-memory activity repository.
    /// </summary>
    public class MemoryActivityRepository : MemoryRepository<Activity, string>, IActivityRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryActivityRepository"/> class.
        /// </summary>
        public MemoryActivityRepository()
            : base(activity => activity.UniqueName)
        {
        }
    }
}