﻿namespace Icm.Sex.Domain.Activities.Predicates
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;

    public class ActivityPredicate : BasePredicate<Activity>
    {
        private readonly IEnumerable<Activity> activities;

        public ActivityPredicate(IEnumerable<Activity> activities)
        {
            this.activities = activities;
        }

        public ActivityPredicate(params Activity[] activities)
        {
            this.activities = activities;
        }

        public override bool TrueFor(Activity obj)
        {
            return activities.Contains(obj);
        }
    }
}
