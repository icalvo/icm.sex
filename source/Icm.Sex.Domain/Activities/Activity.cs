﻿namespace Icm.Sex.Domain.Activities
{
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Sexual activity
    /// </summary>
    public abstract class Activity : IUniqueName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Activity"/> class.
        /// </summary>
        protected Activity()
        {
            UniqueName = GetType().Name.ToLower();
        }

        /// <summary>
        /// Gets the name of the activity. Matches the type name in lower case.
        /// </summary>
        public string UniqueName { get; }

        /// <summary>
        /// The default part for the giving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public abstract Part DefaultGivingPart(Body template);

        /// <summary>
        /// The default part for the receiving side of this activity within the supplied body template.
        /// </summary>
        /// <param name="template">The template.</param>
        /// <returns>
        /// Can be null if no default part can be discerned.
        /// </returns>
        public abstract Part DefaultReceivingPart(Body template);

        /// <summary>
        /// Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return UniqueName.Equals(((Activity)obj).UniqueName);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return UniqueName.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return UniqueName;
        }
    }
}