﻿namespace Icm.Sex.Domain.Scenes.Extensions
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Orgasms;
    using Icm.Sex.Domain.Situations;

    /// <summary>
    /// Extensions for <see cref="IEnumerable{Scene}">Scene lists</see>.
    /// </summary>
    public static class SceneEnumerableExtensions
    {
        /// <summary>
        /// Gets the connections.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <returns></returns>
        public static IEnumerable<Connection> GetConnections(this IEnumerable<Scene> scenes)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .SelectMany(situation => situation.ConnectionGroups)
                .SelectMany(connectionGroup => connectionGroup.Connections);
        }

        /// <summary>
        /// Situations with orgasm.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <returns></returns>
        public static IEnumerable<Situation> SituationsWithOrgasm(this IEnumerable<Scene> scenes)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .Where(situation => situation.Orgasms.Any());
        }

        /// <summary>
        /// Situations with orgasm.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public static IEnumerable<Situation> SituationsWithOrgasm(this IEnumerable<Scene> scenes, Body body)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .Where(situation => situation.Orgasms.Any(orgasm => orgasm.OrgasmingBody.Equals(body)));
        }

        /// <summary>
        /// Orgasms in the specified scenes.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <param name="body">The body.</param>
        /// <returns></returns>
        public static IEnumerable<Orgasm> Orgasms(this IEnumerable<Scene> scenes, Body body)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .SelectMany(situation => situation.Orgasms)
                .Where(orgasm => orgasm.OrgasmingBody.Equals(body));
        }

        /// <summary>
        /// Orgasms in the specified scenes where any connection has the specified activity.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <param name="activity">The activity.</param>
        /// <returns></returns>
        public static int Orgasms(this IEnumerable<Scene> scenes, Activity activity)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .Where(situation => situation.Orgasms.Any() && situation.ConnectionGroups.SelectMany(connectionGroup => connectionGroup.Connections).Any(cnx => cnx.Activity.Equals(activity)))
                .Sum(situation => situation.Orgasms.Count());
        }

        /// <summary>
        /// Orgasms in the specified scenes where the orgasming body is the specified one and any connection has the specified activity.
        /// </summary>
        /// <param name="scenes">The scenes.</param>
        /// <param name="body">The body.</param>
        /// <param name="activity">The activity.</param>
        /// <returns></returns>
        public static int Orgasms(this IEnumerable<Scene> scenes, Body body, Activity activity)
        {
            return scenes
                .SelectMany(scene => scene.Situations)
                .Where(situation => situation.Orgasms.Any() && situation.ConnectionGroups.SelectMany(connectionGroup => connectionGroup.Connections).Any(cnx => cnx.Activity.Equals(activity)))
                .Sum(situation => situation.Orgasms.Count(orgasm => orgasm.OrgasmingBody.Equals(body)));
        }
    }
}