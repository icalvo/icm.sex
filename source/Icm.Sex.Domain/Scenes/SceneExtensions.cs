namespace Icm.Sex.Domain.Scenes
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Situations;

    /// <summary>
    /// Extensions for <see cref="Activity">Activities</see>.
    /// </summary>
    public static class SceneExtensions
    {
        /// <summary>
        /// Determines whether a simultaneous orgasm happens in the specified scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public static bool HasSimultaneousOrgasm(this Scene scene)
        {
            var bodiesCount = scene.Bodies().Count();

            return scene.Situations.Any(situation => situation.Orgasms.Count() == bodiesCount);
        }

        /// <summary>
        /// Bodies of the specified scene.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public static IEnumerable<Body> Bodies(this Scene scene)
        {
            return scene.Situations
                            .SelectMany(situation => situation.Bodies())
                            .Distinct();
        }
    }
}