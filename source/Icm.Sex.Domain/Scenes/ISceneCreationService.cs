﻿namespace Icm.Sex.Domain.Scenes
{
    using System;

    /// <summary>
    /// Service for creating scenes.
    /// </summary>
    public interface ISceneCreationService
    {
        /// <summary>
        /// Creates the scene.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="description">The description.</param>
        /// <returns>The new scene.</returns>
        Scene CreateScene(DateTime date, string description);
    }
}
