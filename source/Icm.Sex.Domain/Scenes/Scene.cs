namespace Icm.Sex.Domain.Scenes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.SceneLabels;
    using Icm.Sex.Domain.Situations;

    /// <summary>
    /// A scene is a correlative sequence of sexual activities.
    /// </summary>
    /// <remarks>
    /// It is formed by situations, which are sets of connections that do not change during a certain period of time. When the connections change, it is another
    /// situation.
    /// </remarks>
    public class Scene
    {
        public Scene(DateTime date, string description = "")
        {
            Date = date;
            Description = description;
            Situations = new HashSet<Situation>();
            Labels = new HashSet<SceneLabel>();
        }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
        public DateTime Date { get; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>
        /// The description.
        /// </value>
        public string Description { get; }

        /// <summary>
        /// Gets or sets the situations.
        /// </summary>
        /// <value>
        /// The situations.
        /// </value>
        public ICollection<Situation> Situations { get; }

        /// <summary>
        /// Gets or sets the labels.
        /// </summary>
        /// <value>
        /// The labels.
        /// </value>
        public ICollection<SceneLabel> Labels { get; }

        public bool HasLabel(string label)
        {
            return Labels.Any(x => x.UniqueName == label);
        }

        /// <summary>
        /// Adds a new situation.
        /// </summary>
        /// <param name="duration">The duration.</param>
        /// <returns></returns>
        public Situation AddSituation(TimeSpan? duration = null)
        {
            if (!duration.HasValue)
            {
                duration = TimeSpan.Zero;
            }

            int newOrder;
            if (Situations.Any())
            {
                newOrder = Situations.Max(situation => situation.Order) + 1;
            }
            else
            {
                newOrder = 1;
            }

            var newSituation = new Situation(this, newOrder, duration.Value);
            Situations.Add(newSituation);
            return newSituation;
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Date == ((Scene)obj).Date;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return Date.GetHashCode();
        }
    }
}