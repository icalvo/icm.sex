﻿namespace Icm.Sex.Domain.Scenes.Predicates
{
    using Icm.Collections;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Connections.Predicates;

    /// <summary>
    /// Predicate for detecting oral sex scenes.
    /// </summary>
    /// <remarks>
    /// This predicate is true if there are some oral sex connection and no intercourse connection.
    /// </remarks>
    public class OralSexScenePredicate : BasePredicate<Scene>
    {
        private readonly IPredicate<Scene> predicate;

        /// <summary>
        /// Initializes a new instance of the <see cref="OralSexScenePredicate"/> class.
        /// </summary>
        public OralSexScenePredicate()
        {
            var predicate1 = new ScenePredicateOnAnyConnection(new OralSexConnectionPredicate());
            var predicate2 = new ScenePredicateOnAllConnections(new ConnectionActivityPredicate<Penetrate>().Not());

            predicate = new AndPredicate<Scene>(predicate1, predicate2);
        }

        /// <summary>
        /// Trues for.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public override bool TrueFor(Scene scene)
        {
            EditableFalseReasons.Clear();

            bool result = predicate.Evaluate(scene);

            EditableFalseReasons.AddRange(predicate.FalseReasons);

            return result;
        }
    }
}