﻿namespace Icm.Sex.Domain.Scenes.Predicates
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Predicate for detecting scenes that satisfy a given connection predicate on all its connections.
    /// </summary>
    public class ScenePredicateOnAllConnections : BaseIterativeEnumerablePredicate<Connection>, IPredicate<Scene>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScenePredicateOnAllConnections"/> class.
        /// </summary>
        /// <param name="connectionPredicate">The connection predicate.</param>
        public ScenePredicateOnAllConnections(IPredicate<Connection> connectionPredicate)
            : base(connectionPredicate)
        {
        }

        /// <summary>
        /// Gets the last result.
        /// </summary>
        /// <value>
        /// The last result.
        /// </value>
        bool? IPredicate<Scene>.LastResult => LastResult;

        /// <summary>
        /// Gets the false reasons.
        /// </summary>
        /// <value>
        /// The false reasons.
        /// </value>
        IEnumerable<string> IPredicate<Scene>.FalseReasons => FalseReasons;

        /// <summary>
        /// Gets the true reasons.
        /// </summary>
        /// <value>
        /// The true reasons.
        /// </value>
        IEnumerable<string> IPredicate<Scene>.TrueReasons => TrueReasons;

        /// <summary>
        /// Does the test.
        /// </summary>
        /// <param name="scene">The scene.</param>
        void IPredicate<Scene>.DoTest(Scene scene)
        {
            var cast = (IPredicate<IEnumerable<Connection>>)this;
            cast.Test(scene.Situations.SelectMany(situation => situation.ConnectionGroups).SelectMany(connectionGroup => connectionGroup.Connections));
        }
    }
}