namespace Icm.Sex.Domain.Scenes.Predicates
{
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Scenes;

    /// <summary>
    /// Predicate for solo scenes
    /// </summary>
    public class SoloScenePredicate : BasePredicate<Scene>
    {
        /// <summary>
        /// Is the predicate true for the supplied scene?
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public override bool TrueFor(Scene scene)
        {
            return scene.Bodies().Count() == 1;
        }
    }
}
