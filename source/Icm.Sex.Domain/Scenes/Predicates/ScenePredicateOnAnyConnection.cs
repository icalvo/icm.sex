﻿namespace Icm.Sex.Domain.Scenes.Predicates
{
    using System.Linq;
    using Icm.Collections;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Predicate for detecting scenes that satisfy a given connection predicate on at least one of its connections.
    /// </summary>
    public class ScenePredicateOnAnyConnection : BasePredicate<Scene>
    {
        private readonly IPredicate<Connection> connectionPredicate;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScenePredicateOnAnyConnection"/> class.
        /// </summary>
        /// <param name="connectionPredicate">The connection predicate.</param>
        public ScenePredicateOnAnyConnection(IPredicate<Connection> connectionPredicate)
        {
            this.connectionPredicate = connectionPredicate;
        }

        /// <summary>
        /// Trues for.
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public override bool TrueFor(Scene scene)
        {
            EditableFalseReasons.Clear();

            bool result = false;

            foreach (var connection in scene.Situations.SelectMany(situation => situation.ConnectionGroups).SelectMany(connectionGroup => connectionGroup.Connections))
            {
                if (!connectionPredicate.Evaluate(connection))
                {
                    EditableFalseReasons.AddRange(connectionPredicate.FalseReasons);
                }
                else
                {
                    result = true;
                    EditableFalseReasons.Clear();
                    break;
                }
            }

            return result;
        }
    }
}