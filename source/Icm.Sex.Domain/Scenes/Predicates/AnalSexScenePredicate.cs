﻿namespace Icm.Sex.Domain.Scenes.Predicates
{
    using Icm.Collections;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Connections.Predicates;

    /// <summary>
    /// A predicate for detecting anal sex scenes.
    /// </summary>
    public class AnalSexScenePredicate : BasePredicate<Scene>
    {
        private readonly IPredicate<Scene> predicate;

        /// <summary>
        /// Initializes a new instance of the <see cref="AnalSexScenePredicate"/> class.
        /// </summary>
        public AnalSexScenePredicate()
        {
            predicate = new ScenePredicateOnAnyConnection(new AnalSexConnectionPredicate());
        }

        /// <summary>
        /// Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="scene">The scene.</param>
        /// <returns></returns>
        public override bool TrueFor(Scene scene)
        {
            bool result = predicate.Evaluate(scene);

            EditableFalseReasons.AddRange(predicate.FalseReasons);

            return result;
        }
    }
}