﻿namespace Icm.Sex.Domain.Scenes.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Connections;

    /// <summary>
    /// Predicate for testing if a connection has a given activity type.
    /// </summary>
    /// <typeparam name="TActivity">The type of the activity.</typeparam>
    public class ConnectionActivityPredicate<TActivity> : BasePredicate<Connection>
    {
        /// <summary>
        /// Is the predicate true for the supplied connection?
        /// </summary>
        /// <param name="connection">The connection to be tested.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            return connection.Activity is TActivity;
        }
    }
}