﻿namespace Icm.Sex.Domain.Scenes
{
    using System;
    using Icm.Sex.Domain.Scenes.Repositories;

    /// <summary>
    /// Implementation of a scene creation service.
    /// </summary>
    public class SceneCreationService : ISceneCreationService
    {
        private readonly ISceneRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="SceneCreationService"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        public SceneCreationService(ISceneRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Creates the scene.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="description">The description.</param>
        /// <returns>The new scene.</returns>
        public Scene CreateScene(DateTime date, string description)
        {
            var newScene = new Scene(date, description);
            this.repository.Add(newScene);

            return newScene;
        }
    }
}