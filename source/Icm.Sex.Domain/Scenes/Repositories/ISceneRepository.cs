﻿namespace Icm.Sex.Domain.Scenes.Repositories
{
    using System;
    using Icm.Data;

    /// <summary>
    /// A repository of scenes.
    /// </summary>
    public interface ISceneRepository : IRepository<Scene, DateTime>
    {
    }
}