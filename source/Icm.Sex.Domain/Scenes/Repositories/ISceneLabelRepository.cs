﻿namespace Icm.Sex.Domain.Scenes.Repositories
{
    using Icm.Data;
    using Icm.Sex.Domain.SceneLabels;

    /// <summary>
    /// A repository of scenes.
    /// </summary>
    public interface ISceneLabelRepository : IRepository<SceneLabel, string>
    {
    }
}