﻿namespace Icm.Sex.Domain.Scenes.Repositories
{
    using System;
    using Icm.Data;

    /// <summary>
    /// In-memory scene repository.
    /// </summary>
    public class MemorySceneRepository : MemoryRepository<Scene, DateTime>, ISceneRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemorySceneRepository"/> class.
        /// </summary>
        public MemorySceneRepository()
            : base(reg => reg.Date)
        {
        }
    }
}