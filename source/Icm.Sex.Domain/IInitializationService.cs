﻿namespace Icm.Sex.Domain.Activities.Services
{
    using System.Collections.Generic;

    /// <summary>
    /// Service for setting initial activity data.
    /// </summary>
    public interface IInitializationService<out T>
    {
        /// <summary>
        /// Gets the initial default elements.
        /// </summary>
        IEnumerable<T> DefaultItems();
    }
}