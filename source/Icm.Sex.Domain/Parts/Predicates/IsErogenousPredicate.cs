﻿namespace Icm.Sex.Domain.Parts.Predicates
{
    using Icm.Sex.Core.Predicates;

    public class IsErogenousPredicate : BaseConstructorPredicate<Part>
    {
        public IsErogenousPredicate()
        {
            PredicateToTest = new PartPredicate(
                new Penis(),
                new Vagina(),
                new Rectum(),
                new Anus(),
                new Nipples(),
                new Vulva(),
                new Testicles());
        }
    }
}