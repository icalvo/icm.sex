﻿namespace Icm.Sex.Domain.Parts.Predicates
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;

    public class PartPredicate : BasePredicate<Part>
    {
        private readonly IEnumerable<Part> parts;

        public PartPredicate(IEnumerable<Part> parts)
        {
            this.parts = parts;
        }

        public PartPredicate(params Part[] parts)
        {
            this.parts = parts;
        }

        public override bool TrueFor(Part obj)
        {
            return parts.Contains(obj);
        }
    }
}
