﻿namespace Icm.Sex.Domain.Parts.Predicates
{
    using Icm.Sex.Core.Predicates;

    public class IsHolePredicate : BaseConstructorPredicate<Part>
    {
        public IsHolePredicate()
        {
            PredicateToTest = new PartPredicate(
                new Vagina(),
                new Rectum());
        }
    }
}