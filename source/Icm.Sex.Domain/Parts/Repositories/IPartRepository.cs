﻿namespace Icm.Sex.Domain.Parts.Repositories
{
    using Icm.Data;

    /// <summary>
    /// Repository of parts.
    /// </summary>
    public interface IPartRepository : IRepository<Part, string>
    {
        /// <summary>
        /// Gets a part of a given type.
        /// </summary>
        /// <typeparam name="T">Type of the requested part.</typeparam>
        /// <returns></returns>
        T Get<T>()
            where T : Part;
    }
}