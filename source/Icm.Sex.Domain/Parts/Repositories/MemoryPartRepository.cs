﻿namespace Icm.Sex.Domain.Parts.Repositories
{
    using System.Linq;
    using Icm.Data;

    /// <summary>
    /// In-memory part repository.
    /// </summary>
    public class MemoryPartRepository : MemoryRepository<Part, string>, IPartRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryPartRepository"/> class.
        /// </summary>
        public MemoryPartRepository()
            : base(part => part.Name)
        {
        }

        /// <summary>
        /// Gets a part of a given type.
        /// </summary>
        /// <typeparam name="T">Type of the requested part.</typeparam>
        /// <returns></returns>
        T IPartRepository.Get<T>()
        {
            return this.OfType<T>().Single();
        }
    }
}