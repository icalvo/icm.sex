﻿namespace Icm.Sex.Domain.Parts
{
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// Represents a part of a body, without reference to a concrete body (for this matter consider <see cref="BodyPart"/> ).
    /// </summary>
    /// <remarks><see cref="Part"/> is a value object, meaning that parts with the same name are identical, so you can always build a part object and use it as a quality
    /// of other objects the same way you use an integer, for example.</remarks>
    public abstract class Part
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Part"/> class.
        /// </summary>
        protected Part()
        {
            Name = GetType().Name.ToLower();
        }

        /// <summary>
        /// Gets the name of the part.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Name.Equals(((Part)obj).Name);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name;
        }
    }
}
