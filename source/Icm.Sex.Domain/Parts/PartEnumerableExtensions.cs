﻿namespace Icm.Sex.Domain.Parts.Extensions
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core;

    /// <summary>
    /// Extensions for <see cref="IEnumerable"/> where each element is of a distinct type.
    /// </summary>
    public static class PartEnumerableExtensions
    {
        /// <summary>
        /// Gets the only element of the specified type, or default(T) otherwise..
        /// </summary>
        /// <typeparam name="T">The type of the part.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static T SingleOrDefault<T>(this IEnumerable collection)
        {
            if (collection.HasOfType<T>())
            {
                return collection.OfType<T>().Single();
            }

            return default(T);
        }

        /// <summary>
        /// Gets the first element of the specified type, or default(T) otherwise..
        /// </summary>
        /// <typeparam name="T">The type of the part.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <returns></returns>
        public static T FirstOrDefault<T>(this IEnumerable collection)
        {
            return collection.OfType<T>().FirstOrDefault();
        }

        public static T SingleOrDefault<T, TPart1, TPart2>(this IEnumerable<T> collection)
            where TPart1 : T
            where TPart2 : T
        {
            if (collection.HasOfType<TPart1>())
            {
                if (collection.HasOfType<TPart2>())
                {
                    return default(T);
                }

                return collection.OfType<TPart1>().Single();
            }

            if (collection.HasOfType<TPart2>())
            {
                return collection.OfType<TPart2>().Single();
            }

            return default(T);
        }

        public static T FirstOrDefault<T, TPart1, TPart2>(this IEnumerable<T> collection)
            where TPart1 : T
            where TPart2 : T
        {
            if (collection.HasOfType<TPart1>())
            {
                return collection.OfType<TPart1>().Single();
            }

            if (collection.HasOfType<TPart2>())
            {
                return collection.OfType<TPart2>().Single();
            }

            return default(T);
        }
    }
}
