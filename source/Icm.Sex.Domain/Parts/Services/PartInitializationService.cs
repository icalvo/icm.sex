﻿namespace Icm.Sex.Domain.Parts.Services
{
    using System.Reflection;
    using Icm.Sex.Domain.Activities.Services;

    public class PartInitializationService : ReflectionInitializationService<Part>
    {
        public PartInitializationService(params Assembly[] assemblies)
            : base(assemblies)
        {
        }
    }
}