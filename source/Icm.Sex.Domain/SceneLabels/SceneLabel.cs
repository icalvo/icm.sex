﻿namespace Icm.Sex.Domain.SceneLabels
{
    using System.Collections.Generic;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.Scenes;

    /// <summary>
    /// Label of a scene.
    /// </summary>
    public class SceneLabel : IUniqueName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SceneLabel"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        public SceneLabel(string name)
        {
            Name = name;
            UniqueName = name.ToLowerInvariant();
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets or sets the unique name of the label.
        /// </summary>
        /// <value>
        /// The unique name of the label.
        /// </value>
        public string UniqueName { get; }

        /// <summary>
        /// Gets or sets the scenes.
        /// </summary>
        /// <value>
        /// The scenes.
        /// </value>
        public IReadOnlyCollection<Scene> Scenes { get; }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return UniqueName.GetHashCode();
        }

        /// <summary>
        /// Determines whether the specified <see cref="object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(UniqueName, ((SceneLabel)obj).UniqueName);
        }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("{0} ({1})", UniqueName);
        }
    }
}