﻿namespace Icm.Sex.Domain.BodyTemplates
{
    using System.Collections.Generic;
    using System.Collections.Immutable;
    using System.Linq;
    using Icm.Collections;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// A body template
    /// </summary>
    public class BodyTemplate : IUniqueName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BodyTemplate"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parts">The parts.</param>
        public BodyTemplate(string name, IEnumerable<Part> parts)
        {
            Name = name;
            UniqueName = name.ToLowerInvariant();
            Parts = parts.Aggregate(ImmutableHashSet<Part>.Empty, (set, part) => set.Add(part));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BodyTemplate"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parts">The parts.</param>
        public BodyTemplate(string name, params Part[] parts)
        {
            Name = name;
            UniqueName = name.ToLowerInvariant();
            Parts = parts.Aggregate(ImmutableHashSet<Part>.Empty, (set, part) => set.Add(part));
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; }

        /// <summary>
        /// Gets or sets the unique name.
        /// </summary>
        /// <value>
        /// The unique name.
        /// </value>
        public string UniqueName { get; }

        /// <summary>
        /// Gets or sets the parts.
        /// </summary>
        /// <value>
        /// The parts.
        /// </value>
        public IReadOnlyCollection<Part> Parts { get; }

        /// <summary>
        /// Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return $"{UniqueName} {{{Parts.JoinStr(", ", part => part.Name)}}}";
        }

        /// <summary>
        /// Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return UniqueName.Equals(((BodyTemplate)obj).UniqueName);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return UniqueName.GetHashCode();
        }
    }
}
