﻿namespace Icm.Sex.Domain.BodyTemplates.Repositories
{
    using Icm.Data;

    /// <summary>
    /// In-memory body template repository.
    /// </summary>
    public class MemoryBodyTemplateRepository : MemoryRepository<BodyTemplate, string>, IBodyTemplateRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MemoryBodyTemplateRepository"/> class.
        /// </summary>
        public MemoryBodyTemplateRepository()
            : base(reg => reg.UniqueName)
        {
        }
    }
}