﻿namespace Icm.Sex.Domain.BodyTemplates.Repositories
{
    using Icm.Data;

    /// <summary>
    /// Interface for body template repositories.
    /// </summary>
    public interface IBodyTemplateRepository : IRepository<BodyTemplate, string>
    {
    }
}