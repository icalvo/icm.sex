﻿namespace Icm.Sex.Domain.BodyTemplates.Services
{
    using System.Collections.Generic;
    using Activities.Services;
    using Parts;

    /// <summary>
    /// Service for setting initial body template data.
    /// </summary>
    public class BodyTemplateInitializationService : IInitializationService<BodyTemplate>
    {
        /// <summary>
        /// Ensures that the standard body templates are present in the repository.
        /// </summary>
        public IEnumerable<BodyTemplate> DefaultItems()
        {
            yield return new BodyTemplate(
                "man",
                new Face(),
                new Mouth(),
                new Tongue(),
                new Finger(),
                new Rectum(),
                new Anus(),
                new Ass(),
                new Nipples(),
                new Abdomen(),
                new Hand(),
                new Penis(),
                new Testicles());

            yield return new BodyTemplate(
                "woman",
                new Face(),
                new Mouth(),
                new Tongue(),
                new Finger(),
                new Rectum(),
                new Anus(),
                new Ass(),
                new Nipples(),
                new Abdomen(),
                new Hand(),
                new Tits(),
                new Vulva(),
                new Vagina());
        }
    }
}