namespace Icm.Sex.Domain.BodyTemplates
{
    public interface IUniqueName
    {
        string UniqueName { get; }
    }
}