﻿namespace Icm.Sex.Domain.Connections
{
    public class Willingness : Enumeration
    {
        public readonly static Willingness Willing = new Willingness(0, "Willing");

        public readonly static Willingness Unwilling = new Willingness(1, "Unwilling");

        private Willingness(int value, string displayName)
            : base(value, displayName)
        {
        }
    }
}