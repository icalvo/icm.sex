namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate where a male touches other penis.
    /// </summary>
    public class MaleTouchesOtherPenisPredicate : BaseMalePredicate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaleTouchesOtherPenisPredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public MaleTouchesOtherPenisPredicate(IPredicate<Body> maleBodyPredicate)
            : base(maleBodyPredicate)
        {
        }

        /// <summary>
        /// Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            var givingBody = connection.GivingBody;
            var receivingBody = connection.ReceivingBody;
            if (MaleBodyPredicate.Evaluate(givingBody))
            {
                if (givingBody.Equals(receivingBody))
                {
                    EditableFalseReasons.Add("(" + connection + ") The connection is solo");
                    return false;
                }

                if (connection.ReceivingPart is Penis)
                {
                    EditableTrueReasons.Add("(" + connection + ") A male touches other's penis");
                    return true;
                }

                EditableFalseReasons.Add("(" + connection + ") A male does not touch other's penis");
                return false;
            }

            EditableFalseReasons.Add("(" + connection + ") The giver is not male");
            return false;
        }
    }
}