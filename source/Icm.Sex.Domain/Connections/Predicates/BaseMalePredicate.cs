namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// A connection predicate that relies on a supplied predicate for testing male bodies.
    /// </summary>
    public abstract class BaseMalePredicate : BasePredicate<Connection>
    {
        private readonly IPredicate<Body> maleBodyPredicate;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseMalePredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        protected BaseMalePredicate(IPredicate<Body> maleBodyPredicate)
        {
            this.maleBodyPredicate = maleBodyPredicate;
        }

        /// <summary>
        /// Gets the male body predicate.
        /// </summary>
        /// <value>
        /// The male body predicate.
        /// </value>
        protected IPredicate<Body> MaleBodyPredicate => maleBodyPredicate;
    }
}