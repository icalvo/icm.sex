namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// Predicate for connections
    /// </summary>
    public class LooseMaleHeteroPredicate : BaseConstructorPredicate<Connection>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LooseMaleHeteroPredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public LooseMaleHeteroPredicate(IPredicate<Body> maleBodyPredicate)
            : base()
        {
            PredicateToTest = new OrPredicate<Connection>(
                new MaleDoNonMasturbationWithHimselfPredicate(maleBodyPredicate),
                new MaleDoSomethingWithOtherMalePredicate(maleBodyPredicate))
            .Not();
        }
    }
}