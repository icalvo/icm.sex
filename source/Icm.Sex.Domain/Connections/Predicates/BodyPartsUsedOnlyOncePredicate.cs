﻿namespace Icm.Sex.Domain.Connections.Predicates
{
    using System.Collections.Generic;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;

    public class BodyPartsUsedOnlyOncePredicate : BasePredicate<Connection>
    {
        private readonly Dictionary<BodyPart, Connection> usedDictionary;
        private readonly PartDependencyDictionary dependencyDictionary;

        public BodyPartsUsedOnlyOncePredicate()
        {
            usedDictionary = new Dictionary<BodyPart, Connection>();
            dependencyDictionary = GetStandardDependencyDictionary();
        }

        private PartDependencyDictionary GetStandardDependencyDictionary()
        {
            var result = new PartDependencyDictionary();

            result.Get<Vulva>().Add(new Vagina());
            result.Get<Vagina>().Add(new Vulva());

            return result;
        }

        public override bool TrueFor(Connection connection)
        {
            if (usedDictionary.ContainsKey(connection.GivingBodyPart))
            {
                EditableFalseReasons.Add(connection.GivingBodyPart + " is already used for: " + usedDictionary[connection.GivingBodyPart]);
                return false;
            }

            usedDictionary.Add(connection.GivingBodyPart, connection);
            foreach (var dependentPart in dependencyDictionary.Get(connection.GivingPart))
            {
                usedDictionary.Add(new BodyPart(connection.GivingBody, dependentPart), connection);
            }

            if (usedDictionary.ContainsKey(connection.ReceivingBodyPart))
            {
                EditableFalseReasons.Add(connection.ReceivingBodyPart + " is already used for: " + usedDictionary[connection.GivingBodyPart]);
                return false;
            }

            usedDictionary.Add(connection.ReceivingBodyPart, connection);
            foreach (var dependentPart in dependencyDictionary.Get(connection.ReceivingPart))
            {
                usedDictionary.Add(new BodyPart(connection.ReceivingBody, dependentPart), connection);
            }

            return true;
        }

        public override void InitializeState()
        {
            usedDictionary.Clear();
        }
    }
}