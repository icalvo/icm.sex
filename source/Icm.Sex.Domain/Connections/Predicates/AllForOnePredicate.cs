namespace Icm.Sex.Domain.Connections.Predicates
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Predicates;

    public class AllForOnePredicate : BasePredicate<IEnumerable<Connection>>
    {
        private readonly int bodyCount;

        public AllForOnePredicate(int bodyCount)
        {
            this.bodyCount = bodyCount;
        }

        private class BodyConnection
        {
            private BodyConnection(Connection cnx, bool takeGiving)
            {
                if (takeGiving)
                {
                    this.Body = cnx.GivingBody;
                    this.Part = cnx.GivingPart;
                    this.OtherBody = cnx.ReceivingBody;
                    this.OtherPart = cnx.ReceivingPart;
                }
                else
                {
                    this.Body = cnx.ReceivingBody;
                    this.Part = cnx.ReceivingPart;
                    this.OtherBody = cnx.GivingBody;
                    this.OtherPart = cnx.GivingPart;
                }
                this.Activity = cnx.Activity;
                this.Connection = cnx;
            }

            public static BodyConnection FromGiving(Connection cnx)
            {
                return new BodyConnection(cnx, true);
            }

            public static BodyConnection FromReceiving(Connection cnx)
            {
                return new BodyConnection(cnx, false);
            }

            public Body Body { get; private set; }
            public Part Part { get; private set; }

            public Activity Activity { get; private set; }
            public Body OtherBody { get; private set; }
            public Part OtherPart { get; private set; }

            public Connection Connection { get; private set; }
        }

        public override IEnumerable<string> FalseReasons(IEnumerable<Connection> obj)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<string> TrueReasons(IEnumerable<Connection> obj)
        {
            throw new System.NotImplementedException();
        }

        public override bool TrueFor(IEnumerable<Connection> cnxGroup)
        {
            IsErogenousPredicate isErogenous = new IsErogenousPredicate();

            IEnumerable<BodyConnection> temp = cnxGroup.Select(BodyConnection.FromGiving);
            IEnumerable<BodyConnection> temp2 = cnxGroup.Select(BodyConnection.FromReceiving);

            IEnumerable<BodyConnection> bodyConnections = temp.Union(temp2).Where(bcnx => !Equals(bcnx.Body, bcnx.OtherBody));

            var bodyGroups = bodyConnections.GroupBy(tuple => tuple.Body);

            var egoReceivers = bodyGroups
                .Where(cnxs => cnxs.All(cnx => isErogenous.TrueFor(cnx.Part)))
                .Where(cnxs => cnxs.Select(cnx => cnx.OtherBody).Distinct().Count() == this.bodyCount - 1)
                .Select(grp => grp.Key);

            if (egoReceivers.Any())
            {
                var egoReceiver = egoReceivers.Single();
                IEnumerable<BodyConnection> nonEgoConnections = bodyConnections.Where(bc => !Equals(bc.Body, egoReceiver));
                bool trueFor = nonEgoConnections
                    .All(bc => !isErogenous.TrueFor(bc.Part) || Equals(bc.OtherBody, egoReceiver));
                return trueFor;
            }

            return false;
        }
    }
}