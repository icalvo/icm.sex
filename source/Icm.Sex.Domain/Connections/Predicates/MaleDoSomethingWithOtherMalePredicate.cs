namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// Predicate for connections where a male do something with other male.
    /// </summary>
    public class MaleDoSomethingWithOtherMalePredicate : BaseMalePredicate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaleDoSomethingWithOtherMalePredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public MaleDoSomethingWithOtherMalePredicate(IPredicate<Body> maleBodyPredicate)
            : base(maleBodyPredicate)
        {
        }

        /// <summary>
        /// Is the predicate true for the supplied connection?
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            var body1 = connection.GivingBody;
            var body2 = connection.ReceivingBody;
            if (MaleBodyPredicate.Evaluate(body1) && MaleBodyPredicate.Evaluate(body2))
            {
                if (body1.Equals(body2))
                {
                    EditableFalseReasons.Add("(" + connection + ") The connection is male but solo");
                    return false;
                }

                EditableTrueReasons.Add("(" + connection + ") The connection is male-male and not solo");
                return true;
            }

            EditableFalseReasons.Add("(" + connection + ") The connection is not male-male");
            return false;
        }
    }
}