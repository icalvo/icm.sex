﻿namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate for detecting oral sex connections.
    /// </summary>
    public class OralSexConnectionPredicate : BasePredicate<Connection>
    {
        /// <summary>
        /// Is the predicate true for the supplied connection?
        /// </summary>
        /// <param name="connection">The connection to be tested.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            bool result = true;
            if (!(connection.Activity is Suck || connection.Activity is Lick))
            {
                EditableFalseReasons.Add("Activity is neither lick nor suck");
                result = false;
            }

            if (!(connection.ReceivingPart is Penis || connection.ReceivingPart is Testicles || connection.ReceivingPart is Vulva))
            {
                EditableFalseReasons.Add("Receiving part is neither penis nor testicles nor vulva");
                result = false;
            }

            return result;
        }
    }
}
