namespace Icm.Sex.Domain.Connections.Predicates
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;

    public class ExcludeEquivalentPredicate : BasePredicate<IEnumerable<Connection>>
    {
        private readonly IEnumerable<Body> bodies;
        private readonly List<Connection> allConnections;

        public ExcludeEquivalentPredicate(IEnumerable<Body> bodies, IEnumerable<Connection> allConnections)
        {
            this.bodies = bodies;
            this.allConnections = allConnections.ToList();
        }

        public override IEnumerable<string> FalseReasons(IEnumerable<Connection> obj)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<string> TrueReasons(IEnumerable<Connection> obj)
        {
            throw new System.NotImplementedException();
        }

        public override bool TrueFor(IEnumerable<Connection> collection)
        {
            if (this.bodies.Any(body => this.Inverse(body) != null))
            {
                Dictionary<Body, Body> inverseBodies = this.bodies.ToDictionary(body => body, body => this.Inverse(body) ?? body);

                IEnumerable<Connection> inverseSituation = collection.Select(
                    cnx => new Connection(
                        inverseBodies[cnx.GivingBody],
                        cnx.GivingPart,
                        cnx.Activity,
                        inverseBodies[cnx.ReceivingBody],
                        cnx.ReceivingPart));

                if (this.AnalyzedBefore(collection, inverseSituation))
                {
                    return false;
                }

                return true;
            }

            return true;
        }

        private bool AnalyzedBefore(IEnumerable<Connection> collection, IEnumerable<Connection> inverseSituation)
        {
            return this.LessThan(this.Code(inverseSituation), this.Code(collection));
        }

        private bool LessThan(BitArray code, BitArray bitArray)
        {
            for (int i = 0; i < code.Length; i++)
            {
                if (!code[i] && bitArray[i])
                {
                    return false;
                }

                if (code[i] && !bitArray[i])
                {
                    return true;
                }
            }

            return false;
        }

        private BitArray Code(IEnumerable<Connection> situation)
        {
            BitArray result = new BitArray(this.allConnections.Count);

            foreach (Connection connection in situation)
            {
                int index = this.allConnections.IndexOf(connection);

                result.Set(index, true);
            }

            return result;
        }

        private Body Inverse(Body body)
        {
            var equivalents = this.bodies.Where(other =>
                !Equals(body, other) && Equals(body.Template, other.Template));
            if (equivalents.Count() == 1)
            {
                return equivalents.First();
            }

            return null;
        }
    }
}