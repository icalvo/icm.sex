namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate for anal sex connections
    /// </summary>
    public class AnalSexConnectionPredicate : BasePredicate<Connection>
    {
        /// <summary>
        /// Is the predicate true for the supplied connection?
        /// </summary>
        /// <param name="connection">The connection to be tested.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            return
                (connection.Activity is Penetrate) &&
                (connection.ReceivingPart is Rectum);
        }
    }
}
