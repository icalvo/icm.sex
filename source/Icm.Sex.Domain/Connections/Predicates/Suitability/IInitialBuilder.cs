﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    public interface IInitialBuilder : IGivingSuitable, IGivingUnsuitable
    {
    }
}