﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;

    public class ActivitySuitableParts : IInitialBuilder, IGivingSuitableNoReceiving, IGivingUnuitableNoReceiving
    {
        private readonly IList<Part> givingParts;
        private readonly IList<Part> receivingParts;

        public Activity Activity { get; private set; }

        public IEnumerable<Part> GivingParts => givingParts;

        public bool GivingAreSuitable { get; private set; }

        public IEnumerable<Part> ReceivingParts => receivingParts;

        public bool ReceivingAreSuitable { get; private set; }

        public IGivingSuitableNoReceiving SetGivingSuitable<TPart>()
            where TPart : Part, new()
        {
            if (givingParts.Any() && !GivingAreSuitable)
            {
                throw new InvalidOperationException("Cannot change suitability if a list already has elements");
            }

            givingParts.Add(new TPart());
            GivingAreSuitable = true;

            return this;
        }

        public ActivitySuitableParts SetReceivingSuitable<TPart>()
            where TPart : Part, new()
        {
            if (receivingParts.Any() && !ReceivingAreSuitable)
            {
                throw new InvalidOperationException("Cannot change suitability if a list already has elements");
            }

            receivingParts.Add(new TPart());
            ReceivingAreSuitable = true;

            return this;
        }

        public ActivitySuitableParts SetGivingUnsuitable<TPart>()
            where TPart : Part, new()
        {
            if (givingParts.Any() && GivingAreSuitable)
            {
                throw new InvalidOperationException("Cannot change suitability if a list already has elements");
            }

            givingParts.Add(new TPart());
            GivingAreSuitable = false;

            return this;
        }

        public ActivitySuitableParts SetReceivingUnsuitable<TPart>()
            where TPart : Part, new()
        {
            if (receivingParts.Any() && ReceivingAreSuitable)
            {
                throw new InvalidOperationException("Cannot change suitability if a list already has elements");
            }

            receivingParts.Add(new TPart());
            ReceivingAreSuitable = false;

            return this;
        }

        public ActivitySuitableParts(Activity activity)
        {
            Activity = activity;
            givingParts = new List<Part>();
            receivingParts = new List<Part>();
            GivingAreSuitable = false;
            ReceivingAreSuitable = false;
        }

        public bool IsSuitable(Connection connection)
        {
            return (GivingAreSuitable == GivingParts.Contains(connection.GivingPart)) &&
                   (ReceivingAreSuitable == ReceivingParts.Contains(connection.ReceivingPart));
        }

        IGivingSuitableNoReceiving IGivingSuitable.SetGivingSuitable<TPart>()
        {
            return SetGivingSuitable<TPart>();
        }

        IGivingUnuitableNoReceiving IGivingUnsuitable.SetGivingUnsuitable<TPart>()
        {
            return SetGivingUnsuitable<TPart>();
        }

        IReceivingSuitable IReceivingSuitable.SetReceivingSuitable<TPart>()
        {
            return SetReceivingSuitable<TPart>();
        }

        IReceivingUnsuitable IReceivingUnsuitable.SetReceivingUnsuitable<TPart>()
        {
            return SetReceivingUnsuitable<TPart>();
        }
    }
}