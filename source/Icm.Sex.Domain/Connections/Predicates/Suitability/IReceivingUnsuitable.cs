namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using Icm.Sex.Domain.Parts;

    public interface IReceivingUnsuitable
    {
        IReceivingUnsuitable SetReceivingUnsuitable<TPart>()
            where TPart : Part, new();
    }
}