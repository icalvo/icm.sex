﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Parts;

    public class DifficultConnectionsPredicate : BasePredicate<Connection>
    {
        public override IEnumerable<string> FalseReasons(Connection obj)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<string> TrueReasons(Connection obj)
        {
            throw new System.NotImplementedException();
        }

        public override bool TrueFor(Connection obj)
        {
            // Join mouth with several parts
            if (obj.GivingBody == obj.ReceivingBody &&
                (obj.Activity is Lick ||
                 obj.Activity is Kiss ||
                 obj.Activity is Suck) &&
                (obj.ReceivingPart is Abdomen ||
                 obj.ReceivingPart is Anus ||
                 obj.ReceivingPart is Face ||
                 obj.ReceivingPart is Ass ||
                 (!obj.GivingBody.Autofellatio && obj.ReceivingPart is Penis) ||
                 obj.ReceivingPart is Testicles ||
                 obj.ReceivingPart is Vagina ||
                 obj.ReceivingPart is Vulva)) return false;

            // Join mouth with nipples or tits if there are no tits or they are small.
            if (obj.GivingBody == obj.ReceivingBody &&
                (obj.Activity is Lick ||
                 obj.Activity is Kiss ||
                 obj.Activity is Suck) &&
                (obj.ReceivingPart is Nipples ||
                 obj.ReceivingPart is Tits) &&
                (!obj.GivingBody.BodyParts.Any(bodypart => bodypart.Part is Tits) || !obj.GivingBody.BigTits)) return false;
            if (obj.GivingBody == obj.ReceivingBody &&
                obj.Activity is Penetrate) return false;

            return true;
        }
    }
}