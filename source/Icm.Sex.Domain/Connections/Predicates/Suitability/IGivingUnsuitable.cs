namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using Icm.Sex.Domain.Parts;

    public interface IGivingUnsuitable
    {
        IGivingUnuitableNoReceiving SetGivingUnsuitable<TPart>()
            where TPart : Part, new();
    }
}