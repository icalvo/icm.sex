namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using System.Collections.Generic;
    using Icm.Sex.Domain.Activities;

    public class SuitabilityDictionary
    {
        private readonly Dictionary<Activity, ActivitySuitableParts> store;

        public SuitabilityDictionary()
        {
            store = new Dictionary<Activity, ActivitySuitableParts>();
        }

        public ActivitySuitableParts Get<TActivity>()
            where TActivity : Activity, new()
        {
            var activity = new TActivity();
            return Get(activity);
        }

        public ActivitySuitableParts Get(Activity activity)
        {
            ActivitySuitableParts suitableParts;
            if (!store.TryGetValue(activity, out suitableParts))
            {
                suitableParts = new ActivitySuitableParts(activity);
                store.Add(activity, suitableParts);
            }

            return suitableParts;
        }
    }
}