﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;

    public class ActivityPartSuitabilityPredicate : BasePredicate<Connection>
    {
        private readonly SuitabilityDictionary suitabilityDictionary;

        public ActivityPartSuitabilityPredicate()
        {
            suitabilityDictionary = GetStandardSuitabilityDictionary();
        }

        public ActivityPartSuitabilityPredicate(SuitabilityDictionary suitabilityDictionary)
        {
            this.suitabilityDictionary = suitabilityDictionary;
        }

        public static SuitabilityDictionary GetStandardSuitabilityDictionary()
        {
            var result = new SuitabilityDictionary();

            result.Get<Kiss>()
                .SetGivingSuitable<Mouth>()
                .SetReceivingUnsuitable<Vagina>()
                .SetReceivingUnsuitable<Rectum>();

            result.Get<Penetrate>()
                .SetGivingSuitable<Penis>()
                .SetGivingSuitable<Finger>()
                .SetReceivingSuitable<Vagina>()
                .SetReceivingSuitable<Rectum>();

            result.Get<Lick>()
                .SetGivingSuitable<Tongue>()
                .SetReceivingUnsuitable<Vagina>()
                .SetReceivingUnsuitable<Rectum>();

            result.Get<Slap>()
                .SetGivingSuitable<Hand>()
                .SetGivingSuitable<Penis>()
                .SetReceivingUnsuitable<Vagina>()
                .SetReceivingUnsuitable<Rectum>();

            result.Get<Stroke>()
                .SetGivingSuitable<Hand>()
                .SetGivingSuitable<Finger>()
                .SetReceivingUnsuitable<Mouth>()
                .SetReceivingUnsuitable<Vagina>()
                .SetReceivingUnsuitable<Rectum>();

            result.Get<Suck>()
                .SetGivingSuitable<Mouth>()
                .SetReceivingSuitable<Penis>();
            return result;
        }

        public override bool TrueFor(Connection connection)
        {
            return suitabilityDictionary.Get(connection.Activity).IsSuitable(connection);
        }
    }
}