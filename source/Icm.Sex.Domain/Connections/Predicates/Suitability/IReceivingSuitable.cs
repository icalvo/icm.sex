namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using Icm.Sex.Domain.Parts;

    public interface IReceivingSuitable
    {
        IReceivingSuitable SetReceivingSuitable<TPart>()
            where TPart : Part, new();
    }
}