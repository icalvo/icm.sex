﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using System.Collections.Generic;
    using Icm.Sex.Core.Predicates;

    public class SelfPredicate : BasePredicate<Connection>
    {
        public override IEnumerable<string> FalseReasons(Connection obj)
        {
            throw new System.NotImplementedException();
        }

        public override IEnumerable<string> TrueReasons(Connection obj)
        {
            throw new System.NotImplementedException();
        }

        public override bool TrueFor(Connection obj)
        {
            return obj.GivingBody.Equals(obj.ReceivingBody);
        }
    }
}