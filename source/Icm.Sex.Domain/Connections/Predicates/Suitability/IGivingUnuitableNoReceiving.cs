namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    public interface IGivingUnuitableNoReceiving : IGivingUnsuitable, IReceivingSuitable, IReceivingUnsuitable
    {
    }
}