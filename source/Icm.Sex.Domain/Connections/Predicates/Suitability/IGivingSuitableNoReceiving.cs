namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    public interface IGivingSuitableNoReceiving : IGivingSuitable, IReceivingSuitable, IReceivingUnsuitable
    {
    }
}