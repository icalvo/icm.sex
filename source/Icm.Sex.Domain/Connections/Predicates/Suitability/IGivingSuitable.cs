﻿namespace Icm.Sex.Domain.Connections.Predicates.Suitability
{
    using Icm.Sex.Domain.Parts;

    public interface IGivingSuitable
    {
        IGivingSuitableNoReceiving SetGivingSuitable<TPart>()
            where TPart : Part, new();
    }
}