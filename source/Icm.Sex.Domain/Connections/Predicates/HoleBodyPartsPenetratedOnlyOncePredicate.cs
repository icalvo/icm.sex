namespace Icm.Sex.Domain.Connections.Predicates
{
    using System.Collections.Generic;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Predicates;

    public class HoleBodyPartsPenetratedOnlyOncePredicate : BasePredicate<Connection>
    {
        private readonly Dictionary<BodyPart, int> penetratedParts;

        private readonly IPredicate<Part> isHolePredicate;

        public HoleBodyPartsPenetratedOnlyOncePredicate()
        {
            penetratedParts = new Dictionary<BodyPart, int>();
            isHolePredicate = new IsHolePredicate();
        }

        public override bool TrueFor(Connection connection)
        {
            if (!(connection.Activity is Penetrate))
            {
                return true;
            }

            if (isHolePredicate.Evaluate(connection.ReceivingPart))
            {
                if (!penetratedParts.ContainsKey(connection.ReceivingBodyPart))
                {
                    penetratedParts.Add(connection.ReceivingBodyPart, 0);
                }

                penetratedParts[connection.ReceivingBodyPart] += 1;
                if (penetratedParts[connection.ReceivingBodyPart] > 1)
                {
                    return false;
                }
            }

            return true;
        }

        public override void InitializeState()
        {
            penetratedParts.Clear();
        }
    }
}