namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// Predicate for connections where a male is penetrated.
    /// </summary>
    public class MaleIsPenetratedPredicate : BaseMalePredicate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaleIsPenetratedPredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public MaleIsPenetratedPredicate(IPredicate<Body> maleBodyPredicate)
            : base(maleBodyPredicate)
        {
        }

        /// <summary>
        /// Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            var body2 = connection.ReceivingBody;
            if (MaleBodyPredicate.Evaluate(body2))
            {
                if (connection.Activity is Penetrate)
                {
                    EditableTrueReasons.Add("A male is penetrated");
                    return true;
                }

                EditableFalseReasons.Add("The receiving body is male but the activity is not penetration");
                return false;
            }

            EditableFalseReasons.Add("The receiving body is not male");
            return false;
        }
    }
}