﻿namespace Icm.Sex.Domain.Connections.Predicates
{
    using System.Collections.Generic;
    using Icm.Sex.Domain.Parts;

    public class PartDependencyDictionary
    {
        private readonly Dictionary<Part, IList<Part>> store;

        public PartDependencyDictionary()
        {
            store = new Dictionary<Part, IList<Part>>();
        }

        public IList<Part> Get<TPart>()
            where TPart : Part, new()
        {
            var part = new TPart();
            return Get(part);
        }

        public IList<Part> Get(Part part)
        {
            IList<Part> dependentParts;
            if (store.TryGetValue(part, out dependentParts))
            {
                return dependentParts;
            }

            dependentParts = new List<Part>();
            store.Add(part, dependentParts);
            return new List<Part>();
        }
    }
}