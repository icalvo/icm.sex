namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Collections;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Predicate for connections where a male do non-masturbation with himself.
    /// </summary>
    public class MaleDoNonMasturbationWithHimselfPredicate : BaseMalePredicate
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MaleDoNonMasturbationWithHimselfPredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public MaleDoNonMasturbationWithHimselfPredicate(IPredicate<Body> maleBodyPredicate)
            : base(maleBodyPredicate)
        {
        }

        /// <summary>
        /// Is the predicate true for the supplied connection?
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public override bool TrueFor(Connection connection)
        {
            var body1 = connection.GivingBody;
            var body2 = connection.ReceivingBody;

            var malePredicate = MaleBodyPredicate.Enum();

            malePredicate.DoTest(new[] { body1, body2 });

            if (malePredicate.LastResult.Value)
            {
                EditableTrueReasons.AddRange(malePredicate.TrueReasons);
                if (body1.Equals(body2))
                {
                    if (connection.Activity is Stroke && (connection.ReceivingPart is Penis || connection.ReceivingPart is Testicles))
                    {
                        EditableFalseReasons.Add("(" + connection + ") The male connection is solo and only masturbations");
                        return false;
                    }

                    EditableTrueReasons.Add("(" + connection + ") The male connection is solo is not masturbations");
                    return true;
                }
            }

            EditableFalseReasons.AddRange(malePredicate.FalseReasons);
            EditableFalseReasons.Add("(" + connection + ") Not a male connection");
            return false;
        }
    }
}