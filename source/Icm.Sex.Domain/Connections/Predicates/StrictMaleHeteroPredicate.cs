namespace Icm.Sex.Domain.Connections.Predicates
{
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;

    /// <summary>
    /// Strict rule for detecting an heterosexual connection.
    /// </summary>
    public class StrictMaleHeteroPredicate : BaseConstructorPredicate<Connection>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StrictMaleHeteroPredicate"/> class.
        /// </summary>
        /// <param name="maleBodyPredicate">The male body predicate.</param>
        public StrictMaleHeteroPredicate(IPredicate<Body> maleBodyPredicate)
            : base()
        {
            PredicateToTest = new OrPredicate<Connection>(
                new MaleDoNonMasturbationWithHimselfPredicate(maleBodyPredicate),
                new MaleDoSomethingWithOtherMalePredicate(maleBodyPredicate),
                new MaleTouchesOtherPenisPredicate(maleBodyPredicate),
                new MaleTouchesOtherTesticlesPredicate(maleBodyPredicate),
                new MaleIsPenetratedPredicate(maleBodyPredicate))
            .Not();
        }
    }
}