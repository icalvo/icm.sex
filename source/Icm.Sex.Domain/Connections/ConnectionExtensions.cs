﻿namespace Icm.Sex.Domain.Connections
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    /// Extensions for <see cref="Connection"/>
    /// </summary>
    public static class ConnectionExtensions
    {
        /// <summary>
        /// Bodies of the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static IEnumerable<Body> Bodies(this Connection connection)
        {
            return new[] { connection.GivingBody, connection.ReceivingBody }.Distinct();
        }

        /// <summary>
        /// Bodies of the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static IEnumerable<Part> Parts(this Connection connection)
        {
            return new[] { connection.GivingPart, connection.ReceivingPart }.Distinct();
        }

        /// <summary>
        /// Bodies of the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static IEnumerable<BodyPart> BodyParts(this Connection connection)
        {
            return new[] { connection.GivingBodyPart, connection.ReceivingBodyPart }.Distinct();
        }

        /// <summary>
        ///     Identifiers the specified connection.
        /// </summary>
        /// <param name="connection">The connection.</param>
        /// <returns></returns>
        public static string Identifier(this Connection connection)
        {
            if (connection.GivingBodyPart == null || connection.ReceivingBodyPart == null)
            {
                return string.Empty;
            }

            return
                $"{connection.GivingBody.UniqueName}|{connection.GivingPart}|{connection.Activity}|{connection.ReceivingBody.UniqueName}|{connection.ReceivingPart}";
        }
    }
}