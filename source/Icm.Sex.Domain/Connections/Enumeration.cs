﻿namespace Icm.Sex.Domain.Connections
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public abstract class Enumeration : IComparable
    {
        private readonly int value;
        private readonly string displayName;
        private static readonly Dictionary<Type, IEnumerable> Cache;

        protected Enumeration(int value, string displayName)
        {
            var type = GetType();
            this.value = value;
            this.displayName = displayName;
        }

        public int Value => value;

        public string DisplayName => displayName;

        public static IEnumerable<T> GetAll<T>()
            where T : Enumeration
        {
            var type = typeof(T);
            IEnumerable result;
            if (Cache.TryGetValue(type, out result))
            {
                return result.Cast<T>();
            }

            var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

            IEnumerable<T> enumerable = fields.Select(f => f.GetValue(null)).Cast<T>();

            Cache.Add(type, enumerable);

            return enumerable;
        }

        public static int AbsoluteDifference(Enumeration firstValue, Enumeration secondValue)
        {
            var absoluteDifference = Math.Abs(firstValue.Value - secondValue.Value);
            return absoluteDifference;
        }

        public static T FromValue<T>(int value)
            where T : Enumeration
        {
            var matchingItem = Parse<T, int>(value, "value", item => item.Value == value);
            return matchingItem;
        }

        public static T FromDisplayName<T>(string displayName)
            where T : Enumeration
        {
            var matchingItem = Parse<T, string>(displayName, "display name", item => item.DisplayName == displayName);
            return matchingItem;
        }

        private static T Parse<T, TK>(TK value, string description, Func<T, bool> predicate)
            where T : Enumeration
        {
            var matchingItem = GetAll<T>().FirstOrDefault(predicate);

            if (matchingItem != null)
            {
                return matchingItem;
            }

            var message = $"'{value}' is not a valid {description} in {typeof(T)}";
            throw new ApplicationException(message);
        }

        public override string ToString()
        {
            return DisplayName;
        }

        public override bool Equals(object obj)
        {
            var otherValue = obj as Enumeration;

            if (otherValue == null)
            {
                return false;
            }

            var typeMatches = GetType() == obj.GetType();
            var valueMatches = value.Equals(otherValue.Value);

            return typeMatches && valueMatches;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public int CompareTo(object other)
        {
            return Value.CompareTo(((Enumeration)other).Value);
        }
    }
}