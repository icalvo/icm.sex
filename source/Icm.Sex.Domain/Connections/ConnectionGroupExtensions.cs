namespace Icm.Sex.Domain.Connections
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Sex.Domain.Bodies;

    public static class ConnectionGroupExtensions
    {
        public static IEnumerable<Body> Bodies(this ConnectionGroup connectionGroup)
        {
            return connectionGroup.Connections
                .SelectMany(ConnectionExtensions.Bodies)
                .Distinct();
        }
    }
}