namespace Icm.Sex.Domain.Connections
{
    using System.Collections.Generic;

    /// <summary>
    ///
    /// </summary>
    public class ConnectionGroup
    {
        public ConnectionGroup()
        {
            Connections = new HashSet<Connection>();
        }

        /// <summary>
        ///     Gets the internal identifier.
        /// </summary>
        /// <value>
        ///     The internal identifier.
        /// </value>
        public int InternalId { get; private set; }

        public virtual ICollection<Connection> Connections { get; protected set; }
    }
}