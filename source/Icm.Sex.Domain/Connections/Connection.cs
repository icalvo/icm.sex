﻿namespace Icm.Sex.Domain.Connections
{
    using System;
    using Icm.Sex.Core.Graph;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Parts;

    /// <summary>
    ///     A directed connection between two body parts through a given activity
    /// </summary>
    public class Connection : IEdge<BodyPart>
    {
        private Activity activity;
        private BodyPart givingBodyPart;
        private BodyPart receivingBodyPart;

        /// <summary>
        ///     Initializes a new instance of the <see cref="Connection" /> class.
        /// </summary>
        /// <param name="givingBody">The giving body.</param>
        /// <param name="givingPart">The giving part.</param>
        /// <param name="givingWillingness">The willingness of the giving body.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="receivingWillingness"></param>
        /// <param name="receivingBody">The receiving body.</param>
        /// <param name="receivingPart">The receiving part.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     givingBody
        ///     or
        ///     givingPart
        ///     or
        ///     activity
        ///     or
        ///     receivingBody
        ///     or
        ///     receivingPart
        /// </exception>
        public Connection(Body givingBody, Part givingPart, Willingness givingWillingness, Activity activity, Willingness receivingWillingness, Body receivingBody, Part receivingPart)
        {
            if (givingBody == null)
            {
                throw new ArgumentNullException(nameof(givingBody));
            }

            if (givingPart == null)
            {
                throw new ArgumentNullException(nameof(givingPart));
            }

            if (activity == null)
            {
                throw new ArgumentNullException(nameof(activity));
            }

            if (receivingBody == null)
            {
                throw new ArgumentNullException(nameof(receivingBody));
            }

            if (receivingPart == null)
            {
                throw new ArgumentNullException(nameof(receivingPart));
            }

            BodyPart givingBodyPart = givingBody.GetBodyPart(givingPart);
            if (givingBodyPart == null)
            {
                throw new ArgumentException(
                    $"{givingBody.Name}'s body doesn't have a {givingPart.Name}");
            }

            BodyPart receivingBodyPart = receivingBody.GetBodyPart(receivingPart);
            if (receivingBodyPart == null)
            {
                throw new ArgumentException(
                    $"{receivingBody.Name}'s body doesn't have a {receivingPart.Name}");
            }

            this.givingBodyPart = givingBodyPart;
            GivingWillingness = givingWillingness;
            this.activity = activity;
            ReceivingWillingness = receivingWillingness;
            this.receivingBodyPart = receivingBodyPart;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Connection" /> class.
        /// </summary>
        /// <param name="givingBody">The giving body.</param>
        /// <param name="givingPart">The giving part.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="receivingBody">The receiving body.</param>
        /// <param name="receivingPart">The receiving part.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     givingBody
        ///     or
        ///     givingPart
        ///     or
        ///     activity
        ///     or
        ///     receivingBody
        ///     or
        ///     receivingPart
        /// </exception>
        public Connection(Body givingBody, Part givingPart, Activity activity, Body receivingBody, Part receivingPart)
            : this(givingBody, givingPart, Willingness.Willing, activity, Willingness.Willing, receivingBody, receivingPart)
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Connection" /> class.
        /// </summary>
        /// <param name="givingBodyPart">The giving body part.</param>
        /// <param name="activity">The activity.</param>
        /// <param name="receivingBodyPart">The receiving body part.</param>
        /// <exception cref="System.ArgumentNullException">
        ///     givingBodyPart
        ///     or
        ///     activity
        ///     or
        ///     receivingBodyPart
        /// </exception>
        public Connection(BodyPart givingBodyPart, Activity activity, BodyPart receivingBodyPart)
        {
            if (givingBodyPart == null)
            {
                throw new ArgumentNullException(nameof(givingBodyPart));
            }

            if (activity == null)
            {
                throw new ArgumentNullException(nameof(activity));
            }

            if (receivingBodyPart == null)
            {
                throw new ArgumentNullException(nameof(receivingBodyPart));
            }

            this.givingBodyPart = givingBodyPart;
            this.activity = activity;
            this.receivingBodyPart = receivingBodyPart;
        }

        /// <summary>
        ///     Gets or sets the activity.
        /// </summary>
        /// <value>
        ///     The activity.
        /// </value>
        public virtual Activity Activity
        {
            get { return activity; }
            set { activity = value; }
        }

        /// <summary>
        ///     Gets or sets the giving body part.
        /// </summary>
        /// <value>
        ///     The giving body part.
        /// </value>
        public virtual BodyPart GivingBodyPart
        {
            get { return givingBodyPart; }
            set { givingBodyPart = value; }
        }

        /// <summary>
        ///     Gets the giving body.
        /// </summary>
        /// <value>
        ///     The giving body.
        /// </value>
        public Body GivingBody => GivingBodyPart.Body;

        /// <summary>
        ///     Gets the giving part.
        /// </summary>
        /// <value>
        ///     The giving part.
        /// </value>
        public Part GivingPart => GivingBodyPart.Part;

        /// <summary>
        ///     Gets or sets the receiving body part.
        /// </summary>
        /// <value>
        ///     The receiving body part.
        /// </value>
        public virtual BodyPart ReceivingBodyPart
        {
            get { return receivingBodyPart; }
            set { receivingBodyPart = value; }
        }

        /// <summary>
        ///     Gets the receiving body.
        /// </summary>
        /// <value>
        ///     The receiving body.
        /// </value>
        public Body ReceivingBody => ReceivingBodyPart.Body;

        /// <summary>
        ///     Gets the receiving part.
        /// </summary>
        /// <value>
        ///     The receiving part.
        /// </value>
        public Part ReceivingPart => ReceivingBodyPart.Part;

        /// <summary>
        ///     Gets the source.
        /// </summary>
        /// <value>
        ///     The source.
        /// </value>
        BodyPart IEdge<BodyPart>.Source => GivingBodyPart;

        /// <summary>
        ///     Gets the destination.
        /// </summary>
        /// <value>
        ///     The destination.
        /// </value>
        BodyPart IEdge<BodyPart>.Destination => ReceivingBodyPart;

        public Willingness GivingWillingness { get; }

        public Willingness ReceivingWillingness { get; }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="string" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return
                $"{GivingBody.UniqueName}.{GivingPart} {Activity.UniqueName} {ReceivingBody.UniqueName}.{ReceivingPart}";
        }

        /// <summary>
        ///     Returns a <see cref="string" /> that represents this instance.
        /// </summary>
        /// <returns>
        ///     A <see cref="string" /> that represents this instance.
        /// </returns>
        public string FileIdentifier()
        {
            return
                $"{GivingBody.UniqueName}.{GivingPart}.{Activity.UniqueName}.{ReceivingBody.UniqueName}.{ReceivingPart}";
        }

        /// <summary>
        ///     Determines whether the specified <see cref="object"></see>, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="object" /> to compare with this instance.</param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return this.Identifier().Equals(((Connection)obj).Identifier());
        }

        /// <summary>
        ///     Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        public override int GetHashCode()
        {
            return this.Identifier().GetHashCode();
        }

        /// <summary>
        ///     Is the giving part the default one for the activity?
        /// </summary>
        /// <returns></returns>
        public bool IsGivingWithDefaultPart()
        {
            return Equals(Activity.DefaultGivingPart(GivingBody), GivingPart);
        }

        /// <summary>
        ///     Is the receiving part the default one for the activity?
        /// </summary>
        /// <returns></returns>
        public bool IsReceivingWithDefaultPart()
        {
            return Equals(Activity.DefaultReceivingPart(ReceivingBody), ReceivingPart);
        }
    }
}