namespace Icm.Sex.Domain.BodyTemplates
{
    using System.Collections.Generic;
    using System.Linq;

    public static class UniqueNameEnumerableExtensions
    {
        public static T ByName<T>(this IEnumerable<T> list, string uniqueName)
            where T : IUniqueName
        {
            return list.Single(x => x.UniqueName == uniqueName);
        }
    }
}