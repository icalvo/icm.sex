﻿namespace Icm.Sex.Core
{
    public static class ShortcutDictionaryExtensions
    {
        public static string ShortcutAt(this string name, ShortcutDictionary dic)
        {
            return dic.ShortcutFor(name);
        }
    }
}