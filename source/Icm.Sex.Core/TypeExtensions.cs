﻿namespace Icm.Sex.Core
{
    using System;
    using System.Linq;

    public static class TypeExtensions
    {
        public static string NameWithGenerics(this Type type)
        {
            if (type.IsGenericType)
            {
                return type.Name.SkipBoth(0, 2) + "<" +
                       string.Join(",", type.GenericTypeArguments.Select(NameWithGenerics)) + ">";
            }

            return type.Name;
        }
    }
}