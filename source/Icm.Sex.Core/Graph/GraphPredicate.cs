﻿namespace Icm.Sex.Core.Graph
{
    using System.Collections.Generic;
    using Icm.Collections;
    using Icm.Sex.Core.Predicates;

    /// <summary>
    /// Predicate over a graph that evaluates for each edge added.
    /// </summary>
    /// <typeparam name="TNode">The type of the node.</typeparam>
    /// <typeparam name="TEdge">The type of the edge.</typeparam>
    public class GraphPredicate<TNode, TEdge> : BaseIterativeEnumerablePredicate<TEdge>
        where TEdge : IEdge<TNode>
    {
        private readonly AdjacencyList<TNode, TEdge> graph;
        private readonly IPredicate<TEdge, AdjacencyList<TNode, TEdge>> edgeTest;

        /// <summary>
        /// Initializes a new instance of the <see cref="GraphPredicate{TNode, TEdge}"/> class.
        /// </summary>
        /// <param name="test">The test.</param>
        protected GraphPredicate(IPredicate<TEdge, AdjacencyList<TNode, TEdge>> test)
        {
            graph = new AdjacencyList<TNode, TEdge>();
            edgeTest = test;
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public void Initialize()
        {
            ElementPredicate = new EdgePredicate(graph, edgeTest);
        }

        /// <summary>
        /// Is the predicate true for this collection of edges?
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <returns>A boolean indicating whether the predicate true for this collection of edges.</returns>
        public override bool TrueFor(IEnumerable<TEdge> collection)
        {
            Reset();
            return base.TrueFor(collection);
        }

        /// <summary>
        /// Resets this instance.
        /// </summary>
        private void Reset()
        {
            graph.Clear();
        }

        /// <summary>
        /// Predicate on graph edges.
        /// </summary>
        private class EdgePredicate : BasePredicate<TEdge>
        {
            private readonly AdjacencyList<TNode, TEdge> graph;
            private readonly IPredicate<TEdge, AdjacencyList<TNode, TEdge>> edgeTest;

            /// <summary>
            /// Initializes a new instance of the <see cref="EdgePredicate"/> class.
            /// </summary>
            /// <param name="graph">The graph.</param>
            /// <param name="edgeTest">The edge test.</param>
            public EdgePredicate(AdjacencyList<TNode, TEdge> graph, IPredicate<TEdge, AdjacencyList<TNode, TEdge>> edgeTest)
            {
                this.graph = graph;
                this.edgeTest = edgeTest;
            }

            /// <summary>
            /// Trues for.
            /// </summary>
            /// <param name="edge">The edge.</param>
            /// <returns></returns>
            public override bool TrueFor(TEdge edge)
            {
                graph.ForceEdge(edge);
                var result = edgeTest.Evaluate(edge, graph);
                EditableFalseReasons.AddRange(edgeTest.FalseReasons);
                EditableTrueReasons.AddRange(edgeTest.FalseReasons);
                return result;
            }
        }
    }
}
