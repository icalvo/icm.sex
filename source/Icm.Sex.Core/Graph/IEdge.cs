﻿namespace Icm.Sex.Core.Graph
{
    /// <summary>
    ///     Directed edge of a graph
    /// </summary>
    /// <typeparam name="T">Type of the vertices</typeparam>
    public interface IEdge<out T>
    {
        T Source { get; }

        T Destination { get; }
    }
}