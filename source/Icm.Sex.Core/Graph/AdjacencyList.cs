﻿namespace Icm.Sex.Core.Graph
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     An adjacency list is a way of representing a graph.
    /// </summary>
    /// <typeparam name="TNode">The type of the node.</typeparam>
    /// <typeparam name="TEdge">The type of the edge.</typeparam>
    public class AdjacencyList<TNode, TEdge>
        where TEdge : IEdge<TNode>
    {
        /// <summary>
        ///     Dictionary of edges.
        /// </summary>
        private readonly Dictionary<TNode, ICollection<TEdge>> edges;

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjacencyList{TNode, TEdge}" /> class.
        /// </summary>
        public AdjacencyList()
        {
            edges = new Dictionary<TNode, ICollection<TEdge>>();
        }

        public AdjacencyList(params TNode[] nodes)
            : this(nodes.AsEnumerable())
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="AdjacencyList{TNode, TEdge}" /> class.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        public AdjacencyList(IEnumerable<TNode> nodes)
        {
            if (nodes == null)
            {
                throw new ArgumentNullException(nameof(nodes));
            }

            edges = new Dictionary<TNode, ICollection<TEdge>>();
            foreach (TNode node in nodes)
            {
                AddNode(node);
            }
        }

        /// <summary>
        ///     Clears the adjacency list.
        /// </summary>
        public void Clear()
        {
            edges.Clear();
        }

        /// <summary>
        ///     Adds a node.
        /// </summary>
        /// <param name="node">The node.</param>
        public void AddNode(TNode node)
        {
            edges.Add(node, new List<TEdge>());
        }

        /// <summary>
        ///     Force-adds a node.
        /// </summary>
        /// <param name="node">The node.</param>
        public void ForceNode(TNode node)
        {
            edges[node] = new List<TEdge>();
        }

        /// <summary>
        ///     Adds an edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void AddEdge(TEdge edge)
        {
            edges[edge.Source].Add(edge);
            edges[edge.Destination].Add(edge);
        }

        /// <summary>
        ///     Force-adds an edge.
        /// </summary>
        /// <param name="edge">The edge.</param>
        public void ForceEdge(TEdge edge)
        {
            ForceNode(edge.Source);
            ForceNode(edge.Destination);
            AddEdge(edge);
        }

        /// <summary>
        ///     Does the graph contain a node?
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>A boolean indicating whether the graph contains a node.</returns>
        public bool ContainsNode(TNode node)
        {
            return edges.ContainsKey(node);
        }

        /// <summary>
        ///     Edges adjacent to a node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>The adjacent edges.</returns>
        public IEnumerable<TEdge> Edges(TNode node)
        {
            return edges[node].ToList();
        }

        /// <summary>
        ///     All the distinct edges of the graph.
        /// </summary>
        /// <returns>The edges.</returns>
        public IEnumerable<TEdge> Edges()
        {
            return edges.Keys.SelectMany(node => edges[node]).Distinct();
        }

        /// <summary>
        ///     Nodes of the graph.
        /// </summary>
        /// <returns>The nodes.</returns>
        public IEnumerable<TNode> Nodes()
        {
            return edges.Keys.ToList();
        }

        /// <summary>
        ///     Removes a node from the graph.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <returns>A boolean indicating the success of the operation.</returns>
        public bool RemoveNode(TNode node)
        {
            return edges.Remove(node);
        }

        /// <summary>
        ///     Tries to get the edges adjacent to a node.
        /// </summary>
        /// <param name="node">The node.</param>
        /// <param name="adjacentEdges">The edges.</param>
        /// <returns>A boolean indicating the success of the operation.</returns>
        public bool TryGetEdges(TNode node, out ICollection<TEdge> adjacentEdges)
        {
            return edges.TryGetValue(node, out adjacentEdges);
        }
    }
}