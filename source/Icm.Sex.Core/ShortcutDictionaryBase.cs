﻿namespace Icm.Sex.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Collections.Immutable;

    public abstract class ShortcutDictionaryBase : IImmutableDictionary<string, string>
    {
        private readonly IImmutableDictionary<string, string> store;

        protected ShortcutDictionaryBase(IImmutableDictionary<string, string> dic)
        {
            store = dic;
        }

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return store.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)store).GetEnumerator();
        }

        public int Count => store.Count;

        public bool ContainsKey(string key)
        {
            return store.ContainsKey(key);
        }

        public bool TryGetValue(string key, out string value)
        {
            return store.TryGetValue(key, out value);
        }

        public string this[string key] => store[key];

        public IEnumerable<string> Keys => store.Keys;

        public IEnumerable<string> Values => store.Values;

        public IImmutableDictionary<string, string> Clear()
        {
            return store.Clear();
        }

        public IImmutableDictionary<string, string> Add(string key, string value)
        {
            return store.Add(key, value);
        }

        public IImmutableDictionary<string, string> AddRange(IEnumerable<KeyValuePair<string, string>> pairs)
        {
            return store.AddRange(pairs);
        }

        public IImmutableDictionary<string, string> SetItem(string key, string value)
        {
            return store.SetItem(key, value);
        }

        public IImmutableDictionary<string, string> SetItems(IEnumerable<KeyValuePair<string, string>> items)
        {
            return store.SetItems(items);
        }

        public IImmutableDictionary<string, string> RemoveRange(IEnumerable<string> keys)
        {
            return store.RemoveRange(keys);
        }

        public IImmutableDictionary<string, string> Remove(string key)
        {
            return store.Remove(key);
        }

        public bool Contains(KeyValuePair<string, string> pair)
        {
            return store.Contains(pair);
        }

        public bool TryGetKey(string equalKey, out string actualKey)
        {
            return store.TryGetKey(equalKey, out actualKey);
        }
    }
}