﻿namespace Icm.Sex.Core.UnitOfWork
{
    /// <summary>
    /// Basic unit of work interface that allows saving.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Saves the changes on this unit of work since the creation or the last save.
        /// </summary>
        void Save();
    }
}