﻿namespace Icm.Sex.Core
{
    using Icm.Data;

    /// <summary>
    ///     Repository with methods for eager loading and returning no proxied objects.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TKey">The type of the key.</typeparam>
    public interface IExtendedRepository<TEntity, TKey> : IRepository<TEntity, TKey>
    {
    }
}