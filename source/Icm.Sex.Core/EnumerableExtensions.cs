﻿namespace Icm.Sex.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     Extension methods for <see cref="IEnumerable" />
    /// </summary>
    public static class EnumerableExtensions
    {
        public static bool HasOfType<TTarget>(this IEnumerable collection)
        {
            return collection.OfType<TTarget>().Any();
        }

        public static IList<T> WhereByBitmap<T>(this IEnumerable<T> elements, BitArray bitmap)
        {
            var elementsList = elements.ToList();
            var res = new List<T>();
            for (var i = 0; i < bitmap.Count; i++)
            {
                if (bitmap[i])
                {
                    res.Add(elementsList[i]);
                }
            }

            return res;
        }

        public static IList<T> WhereByBitmap<T>(this IEnumerable<T> elements, string bitmap)
        {
            var elementsList = elements.ToList();
            var bitmapArray = bitmap.ToCharArray();
            var res = new List<T>();
            for (var i = 0; i < bitmap.Length; i++)
            {
                if (bitmapArray[i] == '1')
                {
                    res.Add(elementsList[i]);
                }
            }

            return res;
        }
    }
}