﻿namespace Icm.Sex.Core
{
    using System.Collections.Immutable;
    using System.Diagnostics;
    using System.Linq;

    public class ShortcutDictionary : ShortcutDictionaryBase
    {
        private ShortcutDictionary(IImmutableDictionary<string, string> dic)
            : base(dic)
        {
            CheckInvariant();
        }

        public static ShortcutDictionary Empty =>
            new ShortcutDictionary(ImmutableDictionary<string, string>.Empty);

        public string ShortcutFor(string name)
        {
            return this.Single(x => x.Value == name).Key;
        }

        public ShortcutDictionary AddRange(params string[] items)
        {
            return items.Aggregate(this, Add);
        }

        private static ShortcutDictionary Add(ShortcutDictionary dic, string newItem)
        {
            return dic.Add(newItem);
        }

        public ShortcutDictionary Add(string newItem)
        {
            var existingShortcut =
                Keys
                .FirstOrDefault(key => newItem.ToLower().StartsWith(key));
            var existingItem = existingShortcut == null ? null : this[existingShortcut];
            var shortcutLength = NewShortcutLength(newItem, existingItem);
            var newDic = this
                .ReplaceShortcut(existingShortcut, existingItem, shortcutLength)
                .AddShortcut(newItem, shortcutLength);
            return new ShortcutDictionary(newDic);
        }

        private void CheckInvariant()
        {
            foreach (var key in Keys)
            {
                Debug.Assert(key == key.ToLower(), "key must be in lower case");
                Debug.Assert(this[key].ToLower().StartsWith(key), "value in lower case must start with key");
            }
        }

        public static int NewShortcutLength(string newItem, string itemForExistingShortcut)
        {
            if (itemForExistingShortcut == null)
            {
                return 1;
            }

            return newItem.ToLower().Zip(
                itemForExistingShortcut.ToLower(),
                (c1, c2) => c1 == c2)
                .TakeWhile(equal => equal)
                .Count() + 1;
        }
    }
}