﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;

    /// <summary>
    /// Test for elements of type T, along with reasons for the failure
    /// when the test returns false.
    /// </summary>
    /// <typeparam name="T">Type of the element</typeparam>
    public interface IPredicate<in T>
    {
        /// <summary>
        /// Gets the last result.
        /// </summary>
        /// <value>
        /// The last result.
        /// </value>
        bool? LastResult { get; }

        /// <summary>
        /// Gets the failure reasons for the last evaluation of the predicate to be false.
        /// </summary>
        /// <value>
        /// The reasons.
        /// </value>
        IEnumerable<string> FalseReasons { get; }

        /// <summary>
        /// Gets the failure reasons for the last evaluation of the predicate to be true.
        /// </summary>
        /// <value>
        /// The reasons.
        /// </value>
        IEnumerable<string> TrueReasons { get; }

        /// <summary>
        /// Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="obj">The object to be tested.</param>
        void DoTest(T obj);

        string Name(string target = "this");

        void InitializeState();
    }
}
