namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;
    using System.Linq;

    public class ContainsPredicate<T> : BasePredicate<T>
    {
        private readonly IEnumerable<T> activities;

        public ContainsPredicate(IEnumerable<T> activities)
        {
            this.activities = activities;
        }
        public ContainsPredicate(params T[] activities)
        {
            this.activities = activities;
        }

        public override IEnumerable<string> FalseReasons(T obj)
        {
            return new[] { string.Format("{0} not in ({1})", obj, string.Join(", ", this.activities.Select(act => act.ToString()))) };
        }

        public override IEnumerable<string> TrueReasons(T obj)
        {
            return new[] { string.Format("{0} in ({1})", obj, string.Join(", ", this.activities.Select(act => act.ToString()))) };
        }

        public override bool TrueFor(T obj)
        {
            return this.activities.Contains(obj);
        }
    }
}