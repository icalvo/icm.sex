﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;

    /// <summary>
    ///     Base class for predicates.
    /// </summary>
    /// <typeparam name="T1">The type of the 1.</typeparam>
    /// <typeparam name="T2">The type of the 2.</typeparam>
    public abstract class BasePredicate<T1, T2> : IPredicate<T1, T2>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BasePredicate{T1, T2}" /> class.
        /// </summary>
        protected BasePredicate()
        {
            EditableFalseReasons = new List<string>();
            EditableTrueReasons = new List<string>();
        }

        /// <summary>
        ///     Gets the failure reasons if the last evaluation of the predicate was false.
        /// </summary>
        /// <value>
        ///     The failure reasons.
        /// </value>
        public IEnumerable<string> FalseReasons => EditableFalseReasons.AsReadOnly();

        /// <summary>
        ///     Gets the success reasons if the last evaluation of the predicate was true.
        /// </summary>
        /// <value>
        ///     The failure reasons.
        /// </value>
        public IEnumerable<string> TrueReasons => EditableTrueReasons.AsReadOnly();

        public bool? LastResult { get; private set; }

        protected List<string> EditableFalseReasons { get; }

        protected List<string> EditableTrueReasons { get; }

        void IPredicate<T1, T2>.DoTest(T1 obj1, T2 obj2)
        {
            EditableFalseReasons.Clear();
            EditableTrueReasons.Clear();
            bool result = TrueFor(obj1, obj2);

            LastResult = result;
        }

        /// <summary>
        ///     Is the predicate true for the supplied objects?
        /// </summary>
        /// <param name="obj1">The first object.</param>
        /// <param name="obj2">The second object.</param>
        /// <returns></returns>
        public abstract bool TrueFor(T1 obj1, T2 obj2);
    }
}