﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;
    using Icm.Collections;

    /// <summary>
    /// Predicate composed of other predicates, that must all be true for the composite to be true.
    /// </summary>
    /// <typeparam name="T">Type of the element to be tested</typeparam>
    public class AndPredicate<T> : AggregatePredicate<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AndPredicate{T}"/> class.
        /// </summary>
        public AndPredicate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AndPredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        public AndPredicate(IEnumerable<IPredicate<T>> tests)
            : base(tests)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AndPredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        public AndPredicate(params IPredicate<T>[] tests)
            : base(tests)
        {
        }

        public override bool TrueFor(T obj)
        {
            var result = true;
            foreach (var test in Tests)
            {
                if (test.Evaluate(obj))
                {
                    EditableTrueReasons.AddRange(test.TrueReasons);
                }
                else
                {
                    EditableFalseReasons.AddRange(test.FalseReasons);
                    result = false;
                }
            }

            return result;
        }

        protected override string AggregateFunctionName()
        {
            return "and";
        }
    }
}
