﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Collections;

    public abstract class AggregatePredicate<T> : BasePredicate<T>
    {
        private readonly IList<IPredicate<T>> tests;

        /// <summary>
        /// Initializes a new instance of the <see cref="AggregatePredicate{T}"/> class.
        /// </summary>
        protected AggregatePredicate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AggregatePredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        protected AggregatePredicate(IEnumerable<IPredicate<T>> tests)
        {
            this.tests = new List<IPredicate<T>>(tests);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AggregatePredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        protected AggregatePredicate(params IPredicate<T>[] tests)
        {
            this.tests = new List<IPredicate<T>>(tests);
        }

        protected IList<IPredicate<T>> Tests => tests;

        public override bool TrueFor(T obj)
        {
            var result = false;
            foreach (var test in Tests)
            {
                if (test.Evaluate(obj))
                {
                    EditableTrueReasons.AddRange(test.TrueReasons);
                    result = true;
                }
                else
                {
                    EditableFalseReasons.AddRange(test.FalseReasons);
                }
            }

            return result;
        }

        public override string Name(string target = "this")
        {
            return string.Join(
                $" {AggregateFunctionName()} ",
                tests.Select(
                test =>
                {
                    if (test is AggregatePredicate<T>)
                    {
                        return "(" + test.Name(target) + ")";
                    }

                    return test.Name(target);
                }));
        }

        protected abstract string AggregateFunctionName();
    }
}