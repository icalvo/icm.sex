﻿namespace Icm.Sex.Core.Predicates
{
    using Icm.Collections;

    /// <summary>
    /// Base class for predicates that implements themselves basing on other predicates built on the constructor.
    /// </summary>
    /// <typeparam name="T">Type of the objects to be evaluated.</typeparam>
    public abstract class BaseConstructorPredicate<T> : BasePredicate<T>
    {
        protected IPredicate<T> PredicateToTest { get; set; }

        public override bool TrueFor(T cnx)
        {
            var result = PredicateToTest.Evaluate(cnx);

            EditableTrueReasons.AddRange(PredicateToTest.TrueReasons);
            EditableFalseReasons.AddRange(PredicateToTest.FalseReasons);

            return result;
        }
    }
}