﻿namespace Icm.Sex.Core.Predicates
{
    using System;

    public class FunctionPredicate<T> : BasePredicate<T>
    {
        private readonly Func<T, bool> functionPredicate;

        public FunctionPredicate(Func<T, bool> functionPredicate)
        {
            this.functionPredicate = functionPredicate;
        }

        public override bool TrueFor(T obj)
        {
            return functionPredicate(obj);
        }
    }
}