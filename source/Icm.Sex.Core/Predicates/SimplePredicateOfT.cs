﻿namespace Icm.Sex.Core.Predicate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Predicate based on a <see cref="Func{T, bool}"/>.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SimplePredicate<T> : IPredicate<T>
    {
        #region Private attributes

        private readonly Func<T, bool> predicateFunction;

        private readonly Func<T, string> failureFunction;
        private string[] failureReasons;
        
        #endregion

        #region Ctor

        /// <summary>
        /// Initializes a new instance of the SimplePredicate class.
        /// </summary>
        /// <param name=""></param>
        /// <param name="predicateFunction"></param>
        public SimplePredicate(Func<T, bool> predicateFunction)
        {
            if (predicateFunction == null)
            {
                throw new ArgumentNullException("predicateFunction");
            }

            this.failureReasons = new string[] { };
            this.predicateFunction = predicateFunction;
            this.failureFunction = obj => obj.ToString();
        }

        /// <summary>
        /// Initializes a new instance of the SimplePredicate class.
        /// </summary>
        /// <param name=""></param>
        /// <param name="predicateFunction"></param>
        /// <param name="failureFunction"></param>
        public SimplePredicate(Func<T, bool> predicateFunction, Func<T, string> failureFunction)
        {
            if (predicateFunction == null)
            {
                throw new ArgumentNullException("predicateFunction");
            }

            if (failureFunction == null)
            {
                throw new ArgumentNullException("failureFunction");
            }

            this.failureReasons = new string[] { };
            this.predicateFunction = predicateFunction;
            this.failureFunction = failureFunction;
        }
        
        #endregion

        #region IPredicate implementation
        public IEnumerable<string> FalseReasons
        {
            get 
            {
                return this.failureReasons;
            }
        }

        public bool Test(T obj)
        {
            if (this.predicateFunction(obj))
            {
                this.failureReasons = new string[] { };
                return true;
            }
            else
            {
                this.failureReasons = new string[] { this.failureFunction(obj) };
                return false;
            }
        }
 
        #endregion    
    }
}
