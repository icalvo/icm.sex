﻿namespace Icm.Sex.Core.Predicates
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq.Expressions;

    /// <summary>
    ///     Extension methods for <see cref="IEnumerable" />
    /// </summary>
    public static class PredicateExtensions
    {
        public static bool Check<T>(T obj, IPredicate<T> predicate)
        {
            return predicate.Evaluate(obj);
        }

        public static IPredicate<T> Test<T>(this IPredicate<T> predicate, T obj)
        {
            predicate.DoTest(obj);
            return predicate;
        }

        public static IPredicate<T1, T2> Test<T1, T2>(this IPredicate<T1, T2> predicate, T1 obj1, T2 obj2)
        {
            predicate.DoTest(obj1, obj2);
            return predicate;
        }

        public static bool Evaluate<T>(this IPredicate<T> predicate, T obj)
        {
            predicate.DoTest(obj);
            Debug.Assert(predicate.LastResult != null, "predicate.LastResult != null");
            return predicate.LastResult.Value;
        }

        public static bool Evaluate<T1, T2>(this IPredicate<T1, T2> predicate, T1 obj1, T2 obj2)
        {
            predicate.DoTest(obj1, obj2);
            Debug.Assert(predicate.LastResult != null, "predicate.LastResult != null");
            return predicate.LastResult.Value;
        }

        public static IEnumerable<string> Reasons<T>(this IPredicate<T> predicate)
        {
            if (predicate.LastResult.HasValue)
            {
                if (predicate.LastResult.Value)
                {
                    return predicate.TrueReasons;
                }

                return predicate.FalseReasons;
            }

            return null;
        }

        public static IPredicate<T> Not<T>(this IPredicate<T> predicate)
        {
            return new NotPredicate<T>(predicate);
        }

        public static IPredicate<T> And<T>(this IPredicate<T> predicate, params IPredicate<T>[] predicates)
        {
            var andPredicates = new List<IPredicate<T>>();
            andPredicates.Add(predicate);
            andPredicates.AddRange(predicates);
            return new AndPredicate<T>(andPredicates);
        }

        public static IPredicate<T> Or<T>(this IPredicate<T> predicate, params IPredicate<T>[] predicates)
        {
            var orPredicates = new List<IPredicate<T>>();
            orPredicates.Add(predicate);
            orPredicates.AddRange(predicates);
            return new OrPredicate<T>(orPredicates);
        }

        public static IPredicate<IEnumerable<T>> Enum<T>(this IPredicate<T> predicate)
        {
            return new BaseIterativeEnumerablePredicate<T>(predicate);
        }

        public static IPredicate<T2> Transform<T1, T2>(this IPredicate<T1> predicate, Expression<Func<T2, T1>> convert)
        {
            return new TransformPredicate<T2, T1>(predicate, convert);
        }
    }
}
