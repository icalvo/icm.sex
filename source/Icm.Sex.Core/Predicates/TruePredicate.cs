﻿namespace Icm.Sex.Core.Predicates
{
    public class TruePredicate<T> : BasePredicate<T>
    {
        public override bool TrueFor(T obj)
        {
            return true;
        }
    }
}