﻿namespace Icm.Sex.Core.Predicates
{
    using System;
    using Icm.Collections;

    /// <summary>
    ///     Predicate composed of other predicates, that must all be true for the composite to be true.
    /// </summary>
    /// <typeparam name="T">Type of the element to be tested</typeparam>
    public class NotPredicate<T> : BasePredicate<T>
    {
        private readonly IPredicate<T> test;

        /// <summary>
        ///     Initializes a new instance of the <see cref="NotPredicate{T}" /> class.
        /// </summary>
        public NotPredicate()
        {
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="NotPredicate{T}" /> class.
        /// </summary>
        /// <param name="test">The test.</param>
        public NotPredicate(IPredicate<T> test)
        {
            if (test == null)
            {
                throw new ArgumentNullException(nameof(test));
            }

            this.test = test;
        }

        private IPredicate<T> TestToNegate => test;

        public override bool TrueFor(T obj)
        {
            if (test.Evaluate(obj))
            {
                EditableFalseReasons.AddRange(test.TrueReasons);
                return false;
            }

            EditableTrueReasons.AddRange(test.FalseReasons);
            return true;
        }

        public override string Name(string target = "this")
        {
            return "not " + test.Name(target);
        }
    }
}