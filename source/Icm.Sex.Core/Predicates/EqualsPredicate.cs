﻿namespace Icm.Sex.Core.Predicates
{
    public class EqualsPredicate<T> : BasePredicate<T>
    {
        private readonly T objectToCompare;

        public EqualsPredicate(T objectToCompare)
        {
            this.objectToCompare = objectToCompare;
        }

        public override bool TrueFor(T obj)
        {
            return Equals(obj, objectToCompare);
        }
    }
}