﻿namespace Icm.Sex.Core.Predicates
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    ///     Base predicate that provides storing for reasons.
    /// </summary>
    /// <typeparam name="T">Type of the objects to be evaluated.</typeparam>
    public abstract class BasePredicate<T> : IPredicate<T>
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="BasePredicate{T}" /> class.
        /// </summary>
        protected BasePredicate()
        {
            EditableFalseReasons = new List<string>();
            EditableTrueReasons = new List<string>();
        }

        /// <summary>
        ///     Gets the failure reasons for the last evaluation of the predicate to be false.
        /// </summary>
        /// <value>
        ///     The failure reasons.
        /// </value>
        public IEnumerable<string> FalseReasons
        {
            get
            {
                return EditableFalseReasons.Select(msg => $"{Name()}: {msg}");
            }
        }

        /// <summary>
        ///     Gets the failure reasons for the last evaluation of the predicate to be true.
        /// </summary>
        /// <value>
        ///     The failure reasons.
        /// </value>
        public IEnumerable<string> TrueReasons
        {
            get
            {
                return EditableTrueReasons.Select(msg => $"{Name()}: {msg}");
            }
        }

        public bool? LastResult { get; private set; }

        /// <summary>
        ///     Is the predicate true for the supplied object?
        /// </summary>
        /// <param name="obj">The object to be tested.</param>
        /// <returns></returns>
        public abstract bool TrueFor(T obj);

        public virtual string Name(string target = "this")
        {
            Type type = GetType();
            string typeName;
            if (type.IsGenericType)
            {
                typeName = type.Name.SkipBoth(0, 2);
            }
            else
            {
                typeName = type.Name;
            }

            if (typeName.EndsWith("Predicate"))
            {
                typeName = typeName.SkipBoth(0, "Predicate".Length);
            }

            return typeName + "(" + target + ")";
        }

        public virtual void InitializeState()
        {
        }

        protected IList<string> EditableFalseReasons { get; }

        protected IList<string> EditableTrueReasons { get; }

        void IPredicate<T>.DoTest(T obj)
        {
            EditableFalseReasons.Clear();
            EditableTrueReasons.Clear();
            bool result = TrueFor(obj);
            LastResult = result;
        }
    }
}