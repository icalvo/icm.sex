﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;

    /// <summary>
    ///     Predicate on enumerables of T.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IEnumerablePredicate<T> : IPredicate<IEnumerable<T>>
    {
        /// <summary>
        ///     Elements that have produced a false result in the last test.
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> FalseElements();
    }
}