﻿namespace Icm.Sex.Core.Predicates
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;

    public class TransformPredicate<T1, T2> : BasePredicate<T1>
    {
        private readonly Func<T1, T2> converter;
        private readonly IPredicate<T2> predicate;
        private readonly Expression<Func<T1, T2>> converterExpression;

        public TransformPredicate(IPredicate<T2> predicate, Expression<Func<T1, T2>> converterExpression)
        {
            this.predicate = predicate;
            this.converterExpression = converterExpression;
            converter = converterExpression.Compile();
        }

        private class ParameterReplacerVisitor<TOutput> : ExpressionVisitor
        {
            private readonly ParameterExpression source;
            private readonly Expression target;

            public ParameterReplacerVisitor(
                    ParameterExpression source, Expression target)
            {
                this.source = source;
                this.target = target;
            }

            internal Expression<TOutput> VisitAndConvert<T>(Expression<T> root)
            {
                return (Expression<TOutput>)VisitLambda(root);
            }

            protected override Expression VisitLambda<T>(Expression<T> node)
            {
                // Leave all parameters alone except the one we want to replace.
                var parameters = node.Parameters
                                     .Where(p => p != source);

                return Expression.Lambda<TOutput>(Visit(node.Body), parameters);
            }

            protected override Expression VisitParameter(ParameterExpression node)
            {
                // Replace the source with the target, visit other params as usual.
                return node == source ? target : base.VisitParameter(node);
            }
        }

        private string ConverterDescription(string target)
        {
            return converterExpression.Body.ToString();
        }

        public override bool TrueFor(T1 obj)
        {
            return predicate.Evaluate(converter(obj));
        }

        public override string Name(string target = "this")
        {
            return predicate.Name(ConverterDescription(target));
        }
    }
}