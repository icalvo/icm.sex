﻿namespace Icm.Sex.Core.Predicate
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Predicate based on a <see cref="Func{T1, T2, bool}"/>.
    /// </summary>
    /// <typeparam name="T1">The type of the first element.</typeparam>
    /// <typeparam name="T2">The type of the second element.</typeparam>
    public class SimplePredicate<T1, T2> : IPredicate<T1, T2>
    {
        #region Private attributes

        private readonly Func<T1, T2, bool> predicateFunction;
        private readonly Func<T1, T2, string> failureFunction;
        private string[] failureReasons;

        #endregion

        #region Ctor
        /// <summary>
        /// Initializes a new instance of the SimplePredicate class.
        /// </summary>
        /// <param name=""></param>
        /// <param name="predicateFunction"></param>
        public SimplePredicate(Func<T1, T2, bool> predicateFunction)
        {
            if (predicateFunction == null)
            {
                throw new ArgumentNullException("predicateFunction");
            }

            this.failureReasons = new string[] { };
            this.predicateFunction = predicateFunction;
            this.failureFunction = (obj1, obj2) => obj1 + ", " + obj2;
        }

        /// <summary>
        /// Initializes a new instance of the SimplePredicate class.
        /// </summary>
        /// <param name=""></param>
        /// <param name="predicateFunction"></param>
        /// <param name="failureFunction"></param>
        public SimplePredicate(Func<T1, T2, bool> predicateFunction, Func<T1, T2, string> failureFunction)
        {
            if (predicateFunction == null)
            {
                throw new ArgumentNullException("predicateFunction");
            }

            if (failureFunction == null)
            {
                throw new ArgumentNullException("failureFunction");
            }

            this.failureReasons = new string[] { };
            this.predicateFunction = predicateFunction;
            this.failureFunction = failureFunction;
        }

        #endregion

        #region IPredicate implementation
        public IEnumerable<string> FailureReasons
        {
            get
            {
                return this.failureReasons;
            }
        }

        public bool TrueFor(T1 obj1, T2 obj2)
        {
            if (this.predicateFunction(obj1, obj2))
            {
                this.failureReasons = new string[] { };
                return true;
            }
            else
            {
                this.failureReasons = new string[] { this.failureFunction(obj1, obj2) };
                return false;
            }
        }

        #endregion
    }
}
