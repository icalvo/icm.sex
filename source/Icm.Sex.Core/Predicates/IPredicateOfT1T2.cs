﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;

    /// <summary>
    /// Test for tuples of type T1, T2, along with reasons for the failure
    /// when the test returns false.
    /// </summary>
    /// <typeparam name="T1">The type of the first item of the tuple.</typeparam>
    /// <typeparam name="T2">The type of the second item of the tuple.</typeparam>
    public interface IPredicate<in T1, in T2>
    {
        /// <summary>
        /// Gets the last result.
        /// </summary>
        /// <value>
        /// The last result.
        /// </value>
        bool? LastResult { get; }

        /// <summary>
        /// Gets the failure reasons for the last evaluation of the predicate to be false.
        /// </summary>
        /// <value>
        /// The reasons.
        /// </value>
        IEnumerable<string> FalseReasons { get; }

        /// <summary>
        /// Gets the failure reasons for the last evaluation of the predicate to be true.
        /// </summary>
        /// <value>
        /// The reasons.
        /// </value>
        IEnumerable<string> TrueReasons { get; }

        /// <summary>
        /// Is the predicate true for the two supplied objects?
        /// </summary>
        /// <param name="object1">The first object.</param>
        /// <param name="object2">The second object.</param>
        void DoTest(T1 object1, T2 object2);
    }
}
