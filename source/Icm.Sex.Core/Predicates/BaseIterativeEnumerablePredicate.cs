﻿namespace Icm.Sex.Core.Predicates
{
    using System;
    using System.Collections.Generic;
    using Icm.Collections;

    /// <summary>
    ///     Predicate that evaluates a collection of elements by evaluating them one by one.
    /// </summary>
    /// <typeparam name="T">Type of the elements.</typeparam>
    public class BaseIterativeEnumerablePredicate<T> : BasePredicate<IEnumerable<T>>, IEnumerablePredicate<T>
    {
        private readonly List<T> falseElements;

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseIterativeEnumerablePredicate{T}" /> class.
        /// </summary>
        /// <param name="elementTest">The element test.</param>
        /// <exception cref="System.ArgumentNullException">elementTest</exception>
        public BaseIterativeEnumerablePredicate(IPredicate<T> elementTest)
        {
            falseElements = new List<T>();
            if (elementTest == null)
            {
                throw new ArgumentNullException(nameof(elementTest));
            }

            ElementPredicate = elementTest;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseIterativeEnumerablePredicate{T}" /> class.
        /// </summary>
        protected internal BaseIterativeEnumerablePredicate()
        {
            falseElements = new List<T>();
        }

        /// <summary>
        ///     Gets or sets the predicate for elements.
        /// </summary>
        /// <value>
        ///     The predicate for elements.
        /// </value>
        protected IPredicate<T> ElementPredicate { get; set; }

        public IEnumerable<T> FalseElements()
        {
            return falseElements.AsReadOnly();
        }

        public override bool TrueFor(IEnumerable<T> collection)
        {
            falseElements.Clear();
            bool result = true;
            ElementPredicate.InitializeState();
            foreach (T element in collection)
            {
                if (ElementPredicate.Evaluate(element))
                {
                    EditableTrueReasons.AddRange(ElementPredicate.TrueReasons);
                }
                else
                {
                    EditableFalseReasons.AddRange(ElementPredicate.FalseReasons);
                    result = false;
                    falseElements.Add(element);
                }
            }

            return result;
        }

        public override string Name(string target = "this")
        {
            return "for all " + ElementPredicate.Name(target);
        }
    }
}