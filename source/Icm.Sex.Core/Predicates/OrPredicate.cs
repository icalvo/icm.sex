﻿namespace Icm.Sex.Core.Predicates
{
    using System.Collections.Generic;
    using Icm.Collections;

    /// <summary>
    /// Predicate composed of other predicates, that must all be true for the composite to be true.
    /// </summary>
    /// <typeparam name="T">Type of the element to be tested</typeparam>
    public class OrPredicate<T> : AggregatePredicate<T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrPredicate{T}"/> class.
        /// </summary>
        public OrPredicate()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrPredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        public OrPredicate(IEnumerable<IPredicate<T>> tests)
            : base(tests)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrPredicate{T}"/> class.
        /// </summary>
        /// <param name="tests">The tests.</param>
        public OrPredicate(params IPredicate<T>[] tests)
            : base(tests)
        {
        }

        public override bool TrueFor(T obj)
        {
            var result = false;
            foreach (var test in Tests)
            {
                if (test.Evaluate(obj))
                {
                    EditableTrueReasons.AddRange(test.TrueReasons);
                    result = true;
                }
                else
                {
                    EditableFalseReasons.AddRange(test.FalseReasons);
                }
            }

            return result;
        }

        protected override string AggregateFunctionName()
        {
            return "or";
        }
    }
}
