﻿namespace Icm.Sex.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Iterates over all possible combinatorial variations of a list of elements.
    /// </summary>
    /// <typeparam name="TElement">Type of the elements.</typeparam>
    /// <remarks>
    /// <example>
    /// <para>If the given list is {A, B, C}, it will return:</para>
    /// <list>
    /// <item>["100", {A}]</item>
    /// <item>["010", {B}]</item>
    /// <item>["110", {A, B}]</item>
    /// <item>["001", {C}]</item>
    /// <item>["101", {A, C}]</item>
    /// <item>["011", {B, C}]</item>
    /// <item>["111", {A, B, C}]</item>
    /// </list>
    /// </example>
    /// </remarks>
    public class VariationIterator<TElement> :
        IEnumerable<Tuple<BitArray, IEnumerable<TElement>>>,
        IEnumerator<Tuple<BitArray, IEnumerable<TElement>>>
    {
        private readonly IList<TElement> list;
        private BitArray current;

        /// <summary>
        /// Initializes a new instance of the <see cref="VariationIterator{TElement}"/> class.
        /// </summary>
        /// <param name="list">The list.</param>
        public VariationIterator(IEnumerable<TElement> list)
        {
            this.list = list.ToList();
            MyReset();
        }

        Tuple<BitArray, IEnumerable<TElement>> IEnumerator<Tuple<BitArray, IEnumerable<TElement>>>.Current => MyCurrent;

        object IEnumerator.Current => MyCurrent;

        private Tuple<BitArray, IEnumerable<TElement>> MyCurrent
        {
            get
            {
                var res = list.WhereByBitmap(current);

                return new Tuple<BitArray, IEnumerable<TElement>>((BitArray)current.Clone(), res);
            }
        }

        void IEnumerator.Reset()
        {
            MyReset();
        }

        bool IEnumerator.MoveNext()
        {
            bool carry = true;

            for (var i = 0; i < current.Count; i++)
            {
                if (carry)
                {
                    carry = current[i];
                    current[i] = !current[i];
                }
            }

            return !carry;
        }

        IEnumerator<Tuple<BitArray, IEnumerable<TElement>>> IEnumerable<Tuple<BitArray, IEnumerable<TElement>>>.GetEnumerator()
        {
            return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this;
        }

        void IDisposable.Dispose()
        {
        }

        private void MyReset()
        {
            current = new BitArray(list.Count());
        }
    }
}
