﻿namespace Icm.Sex.Core
{
    using System.Collections.Generic;

    /// <summary>
    ///     <see cref="IDictionary{TKey,TValue}" /> extensions.
    /// </summary>
    public static class DictionaryExtensions
    {
        public static TValue TryGet<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key)
        {
            TValue result;

            // Ignore return value
            dictionary.TryGetValue(key, out result);
            return result;
        }
    }
}