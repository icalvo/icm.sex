﻿namespace Icm.Sex.Core
{
    using System.Collections.Immutable;

    public static class ImmutableDictionaryExtensions
    {
        public static IImmutableDictionary<string, string> AddShortcut(
            this IImmutableDictionary<string, string> dic,
            string newItem,
            int length)
        {
            return dic.Add(Shortcut(newItem, length), newItem);
        }

        public static IImmutableDictionary<string, string> ReplaceShortcut(
            this IImmutableDictionary<string, string> dic,
            string existingShortcut,
            string item,
            int shortcutLength)
        {
            if (existingShortcut == null)
            {
                return dic;
            }

            return dic
                .Remove(existingShortcut)
                .AddShortcut(item, shortcutLength);
        }

        public static string Shortcut(this string newItem, int length)
        {
            return newItem.Substring(0, length).ToLower();
        }
    }
}