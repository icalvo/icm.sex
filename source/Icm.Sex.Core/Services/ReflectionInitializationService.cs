﻿namespace Icm.Sex.Core.Services
{
    using System.Collections.Generic;
    using System.Reflection;
    using Icm.Collections;
    using Icm.Data;
    using Icm.Reflection;

    /// <summary>
    ///     Service for setting initial activity data.
    /// </summary>
    public abstract class ReflectionInitializationService<T> : IInitializationService<T>
    {
        private readonly IList<Assembly> assemblies;
        private readonly IRepository<T, string> repository;

        protected ReflectionInitializationService(params Assembly[] assemblies)
        {
            this.assemblies = new List<Assembly>();
            this.assemblies.Add(typeof(T).Assembly);
            if (assemblies != null)
            {
                this.assemblies.AddRange(assemblies);
            }
        }

        protected ReflectionInitializationService(IRepository<T, string> repository, params Assembly[] assemblies)
            : this(assemblies)
        {
            this.repository = repository;
        }

        /// <summary>
        ///     Ensures that all the activities (inheritors of <see cref="Activity" /> are present in the repository.
        /// </summary>
        public void EnsureDefaultsInRepository()
        {
            foreach (T activity in this.GetDefaultItems())
            {
                if (!this.repository.ContainsId(activity.GetType().Name.ToLower()))
                {
                    this.repository.Add(activity);
                }
            }
        }

        /// <summary>
        ///     Gets an instance of all the declared activities.
        /// </summary>
        /// <returns>An instance of all the declared activities</returns>
        private IEnumerable<T> GetDefaultItems()
        {
            return ActivatorTools.GetInstanceListOfAllImplementors<T>(this.assemblies);
        }
    }
}