﻿namespace Icm.Sex.Core.Services
{
    /// <summary>
    /// Service for setting initial activity data.
    /// </summary>
    public interface IInitializationService<T>
    {
        /// <summary>
        /// Ensures that all the activities (inheritors of <see cref="Activity"/> are present in the repository.
        /// </summary>
        void EnsureDefaultsInRepository();
    }
}