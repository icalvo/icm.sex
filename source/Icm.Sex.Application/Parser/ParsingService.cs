﻿namespace Icm.Sex.Application.Parser
{
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.UnitsOfWork;

    public class ParsingService : ParsingServiceBase
    {
        private readonly ISexUnitOfWork unitOfWork;

        public ParsingService(
            ISexUnitOfWork unitOfWork,
            ITokenParser tokenParser,
            ParsingOptions options)
            : base(
                  tokenParser,
                  unitOfWork.PartRepository,
                  unitOfWork.ActivityRepository,
                  unitOfWork.SceneRepository,
                  unitOfWork.BodyRepository,
                  unitOfWork.BodyTemplateRepository,
                  options)
        {
            this.unitOfWork = unitOfWork;
        }

        protected override void OnBodyTemplateAdded(BodyTemplate bodyTemplate)
        {
            Templates.Add(bodyTemplate);
        }

        protected override void OnBodyAdded(Body newBody)
        {
            Bodies.Add(newBody);
        }

        protected override void OnSceneAdded(Scene item)
        {
            Scenes.Add(item);
        }

        protected override void OnParsingFinished()
        {
            unitOfWork.Save();
            Messages.Add(new ParseAction("SAVED!"));
        }
    }
}