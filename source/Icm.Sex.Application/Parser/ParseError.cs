namespace Icm.Sex.Application.Parser
{
    using System;

    public class ParseError : ParseMessage
    {
        /// <summary>
        ///     Initializes a new instance of the ParseError class.
        /// </summary>
        public ParseError(int line, Exception exception)
        {
            Line = line;
            Exception = exception;
            Message = exception.Message;
        }

        public ParseError(int line, string message)
        {
            Line = line;
            Message = message;
        }

        public int Line { get; set; }

        public Exception Exception { get; set; }

        public override string ToString()
        {
            return $"{Exception.GetType().Name} thrown at line {Line}: {Exception.Message}";
        }
    }
}