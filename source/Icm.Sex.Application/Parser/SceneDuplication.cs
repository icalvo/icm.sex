namespace Icm.Sex.Application.Parser
{
    public enum SceneDuplication
    {
        None = 0,
        ByDate = 1,
        ByDescription = 2
    }
}
