namespace Icm.Sex.Application.Parser
{
    public class ParsingOptions
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="ParsingOptions" /> class.
        /// </summary>
        /// <param name="requireSituationDuration">if set to <c>true</c> [require situation duration].</param>
        /// <param name="sceneDuplicationOptions">The scene duplication options.</param>
        /// <param name="sceneLabel">The scene label.</param>
        private ParsingOptions(
            bool requireSituationDuration,
            SceneDuplication sceneDuplicationOptions,
            string sceneLabel)
        {
            RequireSituationDuration = requireSituationDuration;
            SceneDuplicationOptions = sceneDuplicationOptions;
            SceneLabel = sceneLabel;
        }

        public bool RequireSituationDuration { get; private set; }

        public SceneDuplication SceneDuplicationOptions { get; private set; }

        public string SceneLabel { get; set; }

        public static ParsingOptions Diary()
        {
            return new ParsingOptions(false, SceneDuplication.ByDate, "diario");
        }

        public static ParsingOptions Film()
        {
            return new ParsingOptions(true, SceneDuplication.ByDescription, "film");
        }
    }
}