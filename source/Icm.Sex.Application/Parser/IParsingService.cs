﻿namespace Icm.Sex.Application.Parser
{
    using System.Collections.Generic;

    public interface IParsingService
    {
        IList<ParseMessage> Messages { get; }

        /// <summary>
        ///     Parses a file.
        /// </summary>
        /// <param name="filename">The file name.</param>
        void ParseFileName(string filename);
    }
}