namespace Icm.Sex.Application.Parser
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using Icm.Collections;
    using Icm.Sex.Domain.Activities;
    using Icm.Sex.Domain.Activities.Repositories;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.BodyTemplates;
    using Icm.Sex.Domain.BodyTemplates.Repositories;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Exceptions;
    using Icm.Sex.Domain.Orgasms;
    using Icm.Sex.Domain.Parts;
    using Icm.Sex.Domain.Parts.Repositories;
    using Icm.Sex.Domain.SceneLabels;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.Situations;

    public interface ISceneBuilder
    {
        /// <summary>
        ///     Adds the situation.
        /// </summary>
        /// <exception cref="System.Exception">Invalid situation: it must have duration</exception>
        void AddSituation();

        /// <summary>
        ///     Adds a scene.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="description">The description.</param>
        /// <exception cref="System.Exception">
        ///     Invalid scene: it must have a date.
        ///     or
        ///     Invalid scene: it must have a description.
        /// </exception>
        void AddScene(DateTime? date = null, string description = null);

        void AddBody(Body body);

        void AddConnection(string connectionLine);
    }

    public abstract class ParsingServiceBase : IParsingService, ISceneBuilder
    {
        private readonly IPartRepository partRepository;
        private readonly IActivityRepository activityRepository;
        private readonly ITokenParser tokenParser;

        private ConnectionGroup currentConnectionGroup;

        private Scene currentScene;
        private Situation currentSituation;

        protected ParsingServiceBase(
            ITokenParser tokenParser,
            IPartRepository partRepository,
            IActivityRepository activityRepository,
            ISceneRepository sceneRepository,
            IBodyRepository bodyRepository,
            IBodyTemplateRepository templateRepository,
            ParsingOptions options)
        {
            this.tokenParser = tokenParser;
            this.partRepository = partRepository;
            this.activityRepository = activityRepository;
            Templates = templateRepository;
            Bodies = bodyRepository;
            Scenes = sceneRepository;
            Messages = new List<ParseMessage>();
            Options = options;
        }

        protected IBodyTemplateRepository Templates { get; }

        protected IBodyRepository Bodies { get; }

        protected ISceneRepository Scenes { get; }

        public ParsingOptions Options { get; set; }

        private int Line { get; set; }

        public IList<ParseMessage> Messages { get; private set; }

        /// <summary>
        ///     Parses a file.
        /// </summary>
        /// <param name="filename">The file name.</param>
        public void ParseFileName(string filename)
        {
            ParseLines(IterateFileLines(filename));
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        public void ParseStringUnsafe(string fileContents)
        {
            ParseLinesNoCatch(fileContents.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries));
        }

        /// <summary>
        ///     Parses the string.
        /// </summary>
        /// <param name="fileContents">The file contents.</param>
        public void ParseString(string fileContents)
        {
            ParseLines(fileContents.Split(new[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries));
        }

        /// <summary>
        ///     Parses the lines.
        /// </summary>
        /// <param name="lines">The lines.</param>
        public void ParseLines(IEnumerable<string> lines)
        {
            try
            {
                ParseLinesNoCatch(lines);
            }
            catch (Exception ex)
            {
                Messages.Add(new ParseError(Line, ex));
            }
        }

        /// <summary>
        ///     Parses the lines.
        /// </summary>
        /// <param name="lines">The lines.</param>
        /// <exception cref="ConnectionParseException"></exception>
        private void ParseLinesNoCatch(IEnumerable<string> lines)
        {
            Messages.Clear();
            string templateName = null;
            var parts = new List<Part>();
            Line = 0;
            foreach (string line in lines)
            {
                Line++;
                tokenParser.Initialize();
                tokenParser.Parse(line);

                if (tokenParser.Errors.Any())
                {
                    Messages.AddRange(tokenParser.Errors.Select(error => new ParseError(Line, "Error at position " + error.Index)));
                    continue;
                }

                IList<string> tokenList = tokenParser.Tokens.ToList();

                if (!tokenList.Any())
                {
                }
                else if (tokenList[0].StartsWith("#"))
                {
                }
                else if (StartsWith(tokenList, "define", "body"))
                {
                    parts.Clear();
                    templateName = tokenList[2];
                }
                else if (StartsWith(tokenList, "import"))
                {
                    string templateParentName = tokenList[1];
                    parts.AddRange(Templates.Single(x => x.UniqueName == templateParentName.ToLowerInvariant()).Parts);
                }
                else if (StartsWith(tokenList, "add"))
                {
                    parts.Add(ParsePart(tokenList[1]));
                }
                else if (StartsWith(tokenList, "remove"))
                {
                    parts.Remove(ParsePart(tokenList[1]));
                }
                else if (StartsWith(tokenList, "end", "define"))
                {
                    AddBodyTemplate(templateName, parts);
                }
                else if (StartsWith(tokenList, "body"))
                {
                    AddBody(tokenList[1], tokenList[2]);
                }
                else if (StartsWith(tokenList, "scene"))
                {
                    DateTime date;
                    DateTime? dateArg = null;
                    string description = null;

                    if (DateTime.TryParse(tokenList[1], out date))
                    {
                        dateArg = date;

                        if (tokenList.Count >= 3)
                        {
                            description = tokenList.ElementAtOrDefault(2);
                        }
                    }
                    else
                    {
                        description = tokenList.ElementAtOrDefault(1);
                    }

                    AddScene(dateArg, description);
                }
                else if (currentScene != null)
                {
                    if (StartsWith(tokenList, "situation"))
                    {
                        if (tokenList.Count >= 3)
                        {
                            AddSituation(tokenList[1], tokenList[2]);
                        }
                        else if (tokenList.Count == 2)
                        {
                            AddSituation(tokenList[1]);
                        }
                        else
                        {
                            AddSituation();
                        }
                    }
                    else if (StartsWith(tokenList, "orgasm"))
                    {
                        AddOrgasm(tokenList);
                    }
                    else
                    {
                        if (tokenList.Count != 3)
                        {
                            throw new ConnectionParseException("Connection with more than 3 elements.");
                        }

                        AddConnection(tokenList[0], tokenList[1], tokenList[2]);
                    }
                }
            }

            OnParsingFinished();
        }

        protected abstract void OnParsingFinished();

        /// <summary>
        ///     Adds a body template.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="parts">The parts.</param>
        private void AddBodyTemplate(string name, IEnumerable<Part> parts)
        {
            if (Templates.Any(x => x.UniqueName == name.ToLowerInvariant()))
            {
                return;
            }

            var bodyTemplate = new BodyTemplate(name, parts);
            AddBodyTemplate(bodyTemplate);
        }

        protected void AddBodyTemplate(BodyTemplate bodyTemplate)
        {
            Templates.Add(bodyTemplate);
            OnBodyTemplateAdded(bodyTemplate);
        }

        protected abstract void OnBodyTemplateAdded(BodyTemplate bodyTemplate);

        /// <summary>
        ///     Adds a body.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="templateName">Name of the template.</param>
        /// <exception cref="TemplateNotFoundException">Template  + templateName +  not found</exception>
        private void AddBody(string name, string templateName)
        {
            BodyTemplate template = Templates.Single(x => x.UniqueName == templateName.ToLowerInvariant());
            if (template == null)
            {
                throw new TemplateNotFoundException("Template " + templateName + " not found");
            }

            if (Bodies.Any(x => x.UniqueName == name.ToLowerInvariant()))
            {
                Messages.Add(new ParseInfo("Ignoring body " + name + " because it already exists in the storage."));
            }
            else
            {
                var newBody = new Body(name, template);
                AddBody(newBody);
                Messages.Add(new ParseAction("Created body " + name + " from template " + templateName));
            }
        }

        private void AddBody(Body newBody)
        {
            Bodies.Add(newBody);
            OnBodyAdded(newBody);
        }

        public void AddConnection(string connectionLine)
        {
            throw new NotImplementedException();
        }

        protected abstract void OnBodyAdded(Body newBody);

        /// <summary>
        ///     Adds a scene.
        /// </summary>
        /// <param name="date">The date.</param>
        /// <param name="description">The description.</param>
        /// <exception cref="System.Exception">
        ///     Invalid scene: it must have a date.
        ///     or
        ///     Invalid scene: it must have a description.
        /// </exception>
        public void AddScene(DateTime? date = null, string description = null)
        {
            if (Options.SceneDuplicationOptions == SceneDuplication.ByDate &&
                !date.HasValue)
            {
                throw new Exception("Invalid scene: it must have a date.");
            }

            if (Options.SceneDuplicationOptions == SceneDuplication.ByDescription &&
                description == null)
            {
                throw new Exception("Invalid scene: it must have a description.");
            }

            if (Options.SceneDuplicationOptions == SceneDuplication.ByDate &&
                Scenes.Any(scene => scene.Date == date))
            {
                Messages.Add(
                    new ParseInfo(
                        string.Format("Ignoring scene with date {0} because it already exists in the storage.", date)));
                currentScene = null;
                return;
            }

            if (Options.SceneDuplicationOptions == SceneDuplication.ByDescription &&
                Scenes.Any(scene => scene.Description == description))
            {
                Messages.Add(
                    new ParseInfo(
                        string.Format("Ignoring scene '{0}' because it already exists in the storage.", description)));
                currentScene = null;
                return;
            }

            if (date == null)
            {
                date = DateTime.Now;
            }

            currentScene = new Scene(date.Value, description);
            AddScene(currentScene);
            if (!string.IsNullOrEmpty(Options.SceneLabel))
            {
                currentScene.Labels.Add(new SceneLabel(Options.SceneLabel));
            }

            Messages.Add(
                new ParseAction(
                    string.Format("Created new scene with date {0} and description '{1}'.", date, description)));
        }

        void ISceneBuilder.AddBody(Body body)
        {
            AddBody(body);
        }

        protected void AddScene(Scene item)
        {
            Scenes.Add(item);
            OnSceneAdded(item);
        }

        protected abstract void OnSceneAdded(Scene item);

        /// <summary>
        ///     Adds the situation with a specified start or end.
        /// </summary>
        /// <param name="start">The start.</param>
        /// <param name="end">The end.</param>
        private void AddSituation(string start, string end)
        {
            DateTime startDate = DateTime.ParseExact(start, "m:ss", CultureInfo.InvariantCulture);
            DateTime endDate = DateTime.ParseExact(end, "m:ss", CultureInfo.InvariantCulture);
            TimeSpan duration = endDate.Subtract(startDate);
            currentSituation = currentScene.AddSituation(duration);
            currentConnectionGroup = new ConnectionGroup();
            currentSituation.ConnectionGroups.Add(currentConnectionGroup);
        }

        /// <summary>
        ///     Adds the situation.
        /// </summary>
        /// <param name="durationString">The duration.</param>
        private void AddSituation(string durationString)
        {
            TimeSpan duration = TimeSpan.Parse(durationString);
            currentSituation = currentScene.AddSituation(duration);
            currentConnectionGroup = new ConnectionGroup();
            currentSituation.ConnectionGroups.Add(currentConnectionGroup);
        }

        /// <summary>
        ///     Adds the situation.
        /// </summary>
        /// <exception cref="System.Exception">Invalid situation: it must have duration</exception>
        public void AddSituation()
        {
            if (Options.RequireSituationDuration)
            {
                throw new Exception("Invalid situation: it must have duration");
            }

            currentSituation = currentScene.AddSituation();
            currentConnectionGroup = new ConnectionGroup();
            currentSituation.ConnectionGroups.Add(currentConnectionGroup);
        }

        /// <summary>
        ///     Adds the orgasm.
        /// </summary>
        /// <param name="sa">The sa.</param>
        private void AddOrgasm(IList<string> sa)
        {
            Body orgasmingBody = Bodies.Single(x => x.UniqueName == sa[1].ToLowerInvariant());
            var partsEjaculated = new List<BodyPart>();
            partsEjaculated
                .AddRange(
                    sa
                        .Skip(2)
                        .Select(part => part.Split('.'))
                        .Select(partSa => GetBodyPartEjaculated(orgasmingBody, partSa[0], partSa[1])));
            currentSituation.AddOrgasm(new Orgasm(orgasmingBody, partsEjaculated));
        }

        /// <summary>
        ///     Adds the connection.
        /// </summary>
        /// <param name="givingBodyPart">The giving body part.</param>
        /// <param name="activityName">Name of the activity.</param>
        /// <param name="receivingBodyPart">The receiving body part.</param>
        /// <exception cref="UnrecognizedActivityException"></exception>
        /// <exception cref="BodyNotFoundException">
        /// </exception>
        /// <exception cref="UnspecifiedGivingPartException">
        ///     No part specified and there is no default giving part for ' + activity
        ///     + ' and body ' + body1 + '
        /// </exception>
        /// <exception cref="UnspecifiedReceivingPartException"></exception>
        private void AddConnection(string givingBodyPart, string activityName, string receivingBodyPart)
        {
            var activity = ParseActivity(activityName);

            if (activity == null)
            {
                throw new UnrecognizedActivityException(string.Format("Unrecognized activity '{0}'", activityName));
            }

            string[] givingSa = givingBodyPart.Split('.');
            if (!HasBody(givingSa[0]))
            {
                throw new BodyNotFoundException(string.Format("Unrecognized body '{0}'", givingSa[0]));
            }

            Body body1 = GetBody(givingSa[0]);
            Part part1;
            if (givingSa.Length > 1)
            {
                part1 = ParsePart(givingSa[1]);
            }
            else
            {
                part1 = activity.DefaultGivingPart(body1);
                if (part1 == null)
                {
                    throw new UnspecifiedGivingPartException(
                        "No part specified and there is no default giving part for '" + activity + "' and body '" +
                        body1 + "'");
                }
            }

            string[] receivingSa = receivingBodyPart.Split('.');
            Body body2;
            if (receivingSa[0] == string.Empty)
            {
                IList<Body> bodiesWithPart = GetBodiesWithPart(receivingSa[1]);

                if (bodiesWithPart.Count == 1)
                {
                    body2 = bodiesWithPart[0];
                }
                else
                {
                    if (bodiesWithPart.Contains(body1))
                    {
                        body2 = body1;
                    }
                    else
                    {
                        throw new AmbiguousPartException(receivingSa[1], bodiesWithPart.Select(x => x.Name).ToArray());
                    }
                }
            }
            else
            {
                if (!HasBody(receivingSa[0]))
                {
                    throw new BodyNotFoundException(string.Format("Unrecognized body '{0}'", receivingSa[0]));
                }

                body2 = GetBody(receivingSa[0]);
            }

            Part part2;
            if (receivingSa.Length > 1)
            {
                part2 = ParsePart(receivingSa[1]);
            }
            else
            {
                part2 = activity.DefaultReceivingPart(body2);
                if (part2 == null)
                {
                    throw new UnspecifiedReceivingPartException(
                        string.Format(
                            "No part specified and there is no default receiving part for '{0}' and body '{1}'",
                            activity,
                            body2));
                }
            }

            currentConnectionGroup.Connections.Add(new Connection(body1, part1, activity, body2, part2));
        }

        private IList<Body> GetBodiesWithPart(string partName)
        {
            return Bodies.Where(x => x.Parts.Any(p => p.Name == partName)).ToList();
        }

        private Body GetBody(string bodyName)
        {
            return Bodies.Single(x => x.UniqueName == bodyName.ToLowerInvariant());
        }

        private bool HasBody(string bodyName)
        {
            return Bodies.Any(x => x.UniqueName == bodyName.ToLowerInvariant());
        }

        /// <summary>
        ///     Startses the with.
        /// </summary>
        /// <param name="sa">The sa.</param>
        /// <param name="tokens">The tokens.</param>
        /// <returns></returns>
        private static bool StartsWith(IEnumerable<string> sa, params string[] tokens)
        {
            int index = 0;
            foreach (string token in tokens)
            {
                if ((string.Empty + sa.ElementAtOrDefault(index)).ToLower() != token)
                {
                    return false;
                }

                index++;
            }

            return true;
        }

        /// <summary>
        ///     Iterates over the lines of a file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>
        ///     Enumeration of lines of the file.
        /// </returns>
        private static IEnumerable<string> IterateFileLines(string filename)
        {
            StreamReader reader = File.OpenText(filename);

            while (true)
            {
                string line = reader.ReadLine();
                if (line == null)
                {
                    break;
                }

                yield return line;
            }

            reader.Close();
        }

        /// <summary>
        ///     Parses a verb into an activity.
        /// </summary>
        /// <param name="verb">The verb.</param>
        /// <returns>
        ///     A corresponding activity or null if no match is found.
        /// </returns>
        private Activity ParseActivity(string verb)
        {
            string uniqueVerb = verb.ToLowerInvariant();
            return activityRepository.SingleOrDefault(activity => uniqueVerb.StartsWith(activity.UniqueName));
        }

        /// <summary>
        ///     Parses the part.
        /// </summary>
        /// <param name="noun">The noun.</param>
        /// <returns></returns>
        private Part ParsePart(string noun)
        {
            string uniqueNoun = noun.ToLowerInvariant();
            return partRepository.SingleOrDefault(part => uniqueNoun.StartsWith(part.Name));
        }

        /// <summary>
        ///     Gets the body part ejaculated.
        /// </summary>
        /// <param name="orgasmingBody">The orgasming body.</param>
        /// <param name="bodyName">Name of the body.</param>
        /// <param name="partName">Name of the part.</param>
        /// <returns></returns>
        private BodyPart GetBodyPartEjaculated(Body orgasmingBody, string bodyName, string partName)
        {
            Body ejaculatedBody = bodyName == string.Empty
                ? orgasmingBody
                : GetBody(bodyName.ToLowerInvariant());
            Part ejaculatedPart = ParsePart(partName);
            return ejaculatedBody.GetBodyPart(ejaculatedPart);
        }

        public bool HasErrors()
        {
            return Messages.Any(message => message.GetType() == typeof(ParseError));
        }
    }
}