﻿namespace Icm.Sex.Application.Parser
{
    public abstract class ParseMessage
    {
        public string Message { get; protected set; }
    }
}