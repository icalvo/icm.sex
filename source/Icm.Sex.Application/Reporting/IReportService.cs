﻿namespace Icm.Sex.Application.Reporting
{
    using Icm.Sex.Infrastructure.Reporting;

    public interface IReportService
    {
        Report Generate();

        Report Generate(string title);
    }
}