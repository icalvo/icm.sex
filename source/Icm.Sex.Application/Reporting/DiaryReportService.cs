namespace Icm.Sex.Application.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Collections;
    using Icm.Localization;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Parts.Predicates;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.Scenes.Extensions;
    using Icm.Sex.Domain.Scenes.Predicates;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.Situations;
    using Icm.Sex.Infrastructure.Reporting;
    using Icm.Sex.Language;

    /// <summary>
    /// Service for generating activity reports.
    /// </summary>
    public class DiaryReportService : TranslationService, IReportService
    {
        private readonly ISceneRepository scenesRepo;
        private readonly IBodyRepository bodiesRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DiaryReportService" /> class.
        /// </summary>
        /// <param name="sceneRepo">The scene repo.</param>
        /// <param name="bodyRepo">The body repo.</param>
        /// <param name="localizationRepo">The localization repository.</param>
        /// <param name="lcid">LCID of the language of the generated report.</param>
        public DiaryReportService(ISceneRepository sceneRepo, IBodyRepository bodyRepo, ILocalizationRepository localizationRepo, int lcid)
            : base(localizationRepo, lcid)
        {
            scenesRepo = sceneRepo;
            bodiesRepo = bodyRepo;
        }

        public Report Generate()
        {
            return Generate(this.Trans("Report"));
        }

        public Report Generate(string title)
        {
            return new CompositeReport(title, GenerateMainBlocks(title));
        }

        public IEnumerable<Report> GenerateMainBlocks(string title)
        {
            var diaryScenes = scenesRepo
                .Where(scene => scene.HasLabel("diario"))
                .ToList();

            yield return new CompositeReport("All the time", GenerateForScenes(diaryScenes));
            IEnumerable<CompositeReport> yearlyReports =
                from scene in diaryScenes
                group scene by scene.Date.Year
                into yearGroup
                select new CompositeReport(yearGroup.Key.ToString(), GenerateForScenes(yearGroup.ToList()));

            foreach (CompositeReport yearlyReport in yearlyReports)
            {
                yield return yearlyReport;
            }
        }

        private Chart IntGroupChart<T>(string title, IEnumerable<IGrouping<string, T>> groups, Func<IEnumerable<T>, int, int> countFunc, IEnumerable<int> keys)
        {
            return new Chart(
                this.Trans(title),
                groups.Select(group => new Series(group.Key, keys.Select(key => SeriesPoint.At(key, countFunc(group, key))))));
        }

        private Chart GroupChart<T>(string title, IEnumerable<IGrouping<string, T>> groups, Func<IEnumerable<T>, double> countFunc)
        {
            return new Chart(
                this.Trans(title),
                groups.Select(group => Series.SinglePoint(group.Key, countFunc(group))));
        }

        private IEnumerable<Report> GenerateForScenes(IList<Scene> scenes)
        {
            var participantsGrouping = scenes
                .GroupBy(BodyNamesList)
                .OrderByDescending(group => group.Key.Length)
                .ToList();

            yield return ParticipantsMonthlyChart(participantsGrouping);
            yield return ParticipantsWeeklyChart(participantsGrouping);
            yield return ParticipantsHourlyChart(participantsGrouping);
            yield return ParticipantsChart(participantsGrouping);
            yield return ConnectionsChart(scenes);
            yield return StimulationChart(scenes);
            yield return NonSoloOrgasmReport(scenes);
            yield return SimultaneityChart(scenes);
            yield return NonSoloEjaxReport(scenes);
            yield return CategoryChart(scenes);
        }

        private static string BodyNamesList(Scene scene)
        {
            return scene
                .Bodies()
                .Select(body => body.Name)
                .OrderBy(name => name)
                .JoinStr(", ");
        }

        private Chart CategoryChart(IEnumerable<Scene> scenes)
        {
            return new Chart(
                this.Trans("Report_Non_Solo_Scenes_Breakdown"),
                scenes
                    .Where(scene => !new SoloScenePredicate().TrueFor(scene))
                    .GroupBy(SceneCategory)
                    .Select(group => Series.SinglePoint(this.Trans("Report_Category_" + group.Key), @group.Count())));
        }

        private CompositeReport NonSoloEjaxReport(IEnumerable<Scene> scenes)
        {
            return MultipleChartForEachBody(
                "Report_Non_Solo_Ejaculations_Breakdown",
                scenes,
                (nonSoloScenes, body) => nonSoloScenes
                    .Orgasms(body)
                    .Where(orgasm => orgasm.PartsEjaculated.Count > 0)
                    .GroupBy(orgasm => EjaculationDescription(orgasm.PartsEjaculated)));
        }

        private CompositeReport NonSoloOrgasmReport(IEnumerable<Scene> scenes)
        {
            return MultipleChartForEachBody(
                "Report_Non_Solo_Orgasms_Breakdown",
                scenes,
                (scenes2, body) => scenes2.SituationsWithOrgasm(body)
                    .GroupBy(situation => OrgasmDescription(situation, body)));
        }

        private CompositeReport MultipleChartForEachBody<T>(string title, IEnumerable<Scene> scenes, Func<IEnumerable<Scene>, Body, IEnumerable<IGrouping<string, T>>> sceneGrouping)
        {
            return new CompositeReport(
                this.Trans(title),
                bodiesRepo
                    .Select(body =>
                        Chart.FromGroupingList(
                            body.Name,
                            sceneGrouping(scenes.Where(NonSolo), body)
                                .Where(group => group.Any()))));
        }

        private Chart SimultaneityChart(IEnumerable<Scene> scenes)
        {
            return Chart.FromGroupingList(
                this.Trans("Report_Non_Solo_Orgasm_Simultaneity_Breakdown"),
                scenes
                    .Where(NonSolo)
                    .SituationsWithOrgasm()
                    .GroupBy(OrgasmingBodyList));
        }

        private static string OrgasmingBodyList(Situation situation)
        {
            return situation
                .Orgasms
                .Select(orgasm => orgasm.OrgasmingBody.Name)
                .OrderBy(name => name)
                .JoinStr(", ");
        }

        private static bool NonSolo(Scene scene)
        {
            return scene.Bodies().Count() > 1;
        }

        private Chart ConnectionsChart(IEnumerable<Scene> scenes)
        {
            return Chart.FromGroupingList(
                this.Trans("Report_Non_Solo_Connections_Frequency"),
                scenes
                    .Where(NonSolo)
                    .SelectMany(scene => scene.Situations)
                    .SelectMany(situation => situation.ConnectionGroups)
                    .SelectMany(connectionGroup => connectionGroup.Connections)
                    .GroupBy(OrgasmDescription)
                    .OrderByDescending(group => group.Count()));
        }

        private Chart StimulationChart(IEnumerable<Scene> scenes)
        {
            var pred = new IsErogenousPredicate();
            return Chart.FromGroupingList(
                this.Trans("Report_Non_Solo_Connections_Frequency"),
                scenes
                    .Where(NonSolo)
                    .SelectMany(scene => scene.Situations)
                    .SelectMany(situation => situation.ConnectionGroups)
                    .SelectMany(connectionGroup => connectionGroup.Connections)
                    .GroupBy(cnx => cnx.BodyParts().Where(bp => pred.Evaluate(bp.Part)).Select(bp => bp.Body.Name).OrderBy(name => name).JoinStr(","))
                    .OrderByDescending(group => group.Count()));
        }

        private Chart ParticipantsChart(List<IGrouping<string, Scene>> participantsGrouping)
        {
            var participantsChart = GroupChart(
                "Report_Scenes_By_Participants_Breakdown",
                participantsGrouping,
                scenesInGroup => scenesInGroup.Count());
            return participantsChart;
        }

        private Chart ParticipantsHourlyChart(List<IGrouping<string, Scene>> participantsGrouping)
        {
            var participantsHourlyChart = IntGroupChart(
                "Report_Scenes_By_Participants_Hourly_Breakdown",
                participantsGrouping,
                (scenesInGroup, key) => scenesInGroup.Count(scene => scene.Date.Hour == key),
                Enumerable.Range(0, 24));
            return participantsHourlyChart;
        }

        private Chart ParticipantsWeeklyChart(List<IGrouping<string, Scene>> participantsGrouping)
        {
            var participantsWeeklyChart = IntGroupChart(
                "Report_Scenes_By_Participants_Weekly_Breakdown",
                participantsGrouping,
                (scenesInGroup, key) => scenesInGroup.Count(scene => (int)scene.Date.AddHours(-5).DayOfWeek == key),
                Enumerable.Range(0, 7));
            participantsWeeklyChart.Properties["formatter"] = "week";
            return participantsWeeklyChart;
        }

        private Chart ParticipantsMonthlyChart(List<IGrouping<string, Scene>> participantsGrouping)
        {
            var participantsMonthlyChart = IntGroupChart(
                "Report_Scenes_By_Participants_Monthly_Breakdown",
                participantsGrouping,
                (scenesInGroup, key) => scenesInGroup.Count(scene => scene.Date.AddHours(-5).Month == key),
                Enumerable.Range(1, 12));
            participantsMonthlyChart.Properties["formatter"] = "month";
            return participantsMonthlyChart;
        }

        private static string SceneCategory(Scene scene)
        {
            var found =
                CategoryPredicates()
                .Select(x => new { x.Key, x.Value })
                .FirstOrDefault(category => category.Value.Evaluate(scene));

            return found == null ? "Straight" : found.Key;
        }

        private static Dictionary<string, IPredicate<Scene>> CategoryPredicates()
        {
            return new Dictionary<string, IPredicate<Scene>>
            {
                { "Anal", new AnalSexScenePredicate() },
                { "Oral", new OralSexScenePredicate() }
            };
        }
    }
}