namespace Icm.Sex.Application.Reporting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Icm.Collections;
    using Icm.Localization;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain.Bodies.Repositories;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.Scenes.Extensions;
    using Icm.Sex.Domain.Scenes.Predicates;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Infrastructure.Reporting;
    using Icm.Sex.Language;

    /// <summary>
    /// Service for generating activity reports.
    /// </summary>
    public class FilmReportService : TranslationService, IReportService
    {
        private readonly ISceneRepository scenes;
        private readonly IBodyRepository bodies;

        /// <summary>
        /// Initializes a new instance of the <see cref="FilmReportService" /> class.
        /// </summary>
        /// <param name="sceneRepo">The scene repo.</param>
        /// <param name="bodyRepo">The body repo.</param>
        /// <param name="localizationRepo"></param>
        /// <param name="lcid"></param>
        public FilmReportService(ISceneRepository sceneRepo, IBodyRepository bodyRepo, ILocalizationRepository localizationRepo, int lcid)
            : base(localizationRepo, lcid)
        {
            scenes = sceneRepo;
            bodies = bodyRepo;
        }

        public Report Generate()
        {
            return Generate(this.Trans("Report"));
        }

        public Report Generate(string title)
        {
            var result = GenerateForScenes(title, scenes.Where(scene => scene.Labels.Any(label => label.UniqueName == "film")));

            return result;
        }

        private Chart GroupChart<T>(string title, IEnumerable<IGrouping<string, T>> groups, Func<IEnumerable<T>, double, double> countFunc, IEnumerable<double> keys)
        {
            var chart = new Chart(this.Trans(title));

            foreach (var group in groups)
            {
                var series = new Series(group.Key);

                foreach (var key in keys)
                {
                    series.Points.Add(SeriesPoint.At(key, countFunc(group, key)));
                }

                chart.Series.Add(series);
            }

            return chart;
        }

        private Chart GroupChart<T>(string title, IEnumerable<IGrouping<string, T>> groups, Func<IEnumerable<T>, double> countFunc)
        {
            var chart = new Chart(this.Trans(title));

            foreach (var group in groups)
            {
                var series = Series.SinglePoint(group.Key, countFunc(group));
                chart.Series.Add(series);
            }

            return chart;
        }

        private CompositeReport GenerateForScenes(string title, IEnumerable<Scene> scenes)
        {
            var soloPredicate = new SoloScenePredicate();
            CompositeReport yearlyData = new CompositeReport(title);

            var participantsGrouping = scenes
                .GroupBy(scene => scene
                        .Bodies()
                        .Select(body => body.Name)
                        .OrderBy(name => name)
                        .JoinStr(", "))
                    .OrderByDescending(group => group.Key.Length)
                    .ToList();

            var participantsMonthlyChart = GroupChart(
                "Report_Scenes_By_Participants_Monthly_Breakdown",
                participantsGrouping,
                (scenes_, key) => scenes_.Count(scene => scene.Date.AddHours(-5).Month == key),
                Enumerable.Range(1, 12).Select(i => (double)i));
            participantsMonthlyChart.Properties["formatter"] = "month";
            yearlyData.AddChild(participantsMonthlyChart);

            var participantsWeeklyChart = GroupChart(
                "Report_Scenes_By_Participants_Weekly_Breakdown",
                participantsGrouping,
                (scenes_, key) => scenes_.Count(scene => (int)scene.Date.AddHours(-5).DayOfWeek == key),
                Enumerable.Range(0, 7).Select(i => (double)i));
            participantsWeeklyChart.Properties["formatter"] = "week";
            yearlyData.AddChild(participantsWeeklyChart);

            var participantsHourlyChart = GroupChart(
                "Report_Scenes_By_Participants_Hourly_Breakdown",
                participantsGrouping,
                (scenes_, key) => scenes_.Count(scene => scene.Date.Hour == key),
                Enumerable.Range(0, 24).Select(i => (double)i));
            yearlyData.AddChild(participantsHourlyChart);

            var participantsChart = GroupChart(
                "Report_Scenes_By_Participants_Breakdown",
                participantsGrouping,
                (scenes_) => scenes_.Count());
            yearlyData.AddChild(participantsChart);

            var connectionsChart = new Chart("Report_Non_Solo_Connections_Frequency");

            var connections = scenes
                .Where(scene => !soloPredicate.TrueFor(scene))
                .SelectMany(scene => scene.Situations)
                .SelectMany(situation => situation.ConnectionGroups)
                .SelectMany(connectionGroup => connectionGroup.Connections)
                .GroupBy(OrgasmDescription)
                .OrderByDescending(group => group.Count());

            foreach (var group in connections)
            {
                connectionsChart.Series.Add(Series.SinglePoint(group.Key, group.Count()));
            }

            yearlyData.AddChild(connectionsChart);

            var nonSoloOrgasmReport = new CompositeReport(this.Trans("Report_Non_Solo_Orgasms_Breakdown"));
            yearlyData.AddChild(nonSoloOrgasmReport);

            foreach (var body in bodies)
            {
                var orgasmSituations = scenes.Where(scene => !soloPredicate.TrueFor(scene)).SituationsWithOrgasm(body);

                var bodyChart = new Chart(body.Name);
                nonSoloOrgasmReport.AddChild(bodyChart);

                foreach (var group in orgasmSituations.GroupBy(situation => OrgasmDescription(situation, body)))
                {
                    var orgasms = group.Count();
                    if (orgasms > 0)
                    {
                        bodyChart.Series.Add(Series.SinglePoint(group.Key, group.Count()));
                    }
                }
            }

            var nonSoloSituations = scenes.Where(scene => !soloPredicate.TrueFor(scene)).SituationsWithOrgasm();
            var simultaneityChart = new Chart(this.Trans("Report_Non_Solo_Orgasm_Simultaneity_Breakdown"));
            yearlyData.AddChild(simultaneityChart);

            var simultaneityGroups = nonSoloSituations.GroupBy(situation => situation.Orgasms.Select(orgasm => orgasm.OrgasmingBody.Name).OrderBy(name => name).JoinStr(", "));

            foreach (var group in simultaneityGroups)
            {
                simultaneityChart.Series.Add(Series.SinglePoint(group.Key, group.Count()));
            }

            var nonSoloEjaxReport = new CompositeReport(this.Trans("Report_Non_Solo_Ejaculations_Breakdown"));
            yearlyData.AddChild(nonSoloEjaxReport);

            foreach (var body in bodies)
            {
                var orgasms = scenes.Where(scene => !soloPredicate.TrueFor(scene)).Orgasms(body).Where(orgasm => orgasm.PartsEjaculated.Count > 0);

                var bodyChart = new Chart(body.Name);
                nonSoloEjaxReport.AddChild(bodyChart);
                foreach (var group in orgasms.GroupBy(orgasm => EjaculationDescription(orgasm.PartsEjaculated)))
                {
                    var count = group.Count();
                    if (count > 0)
                    {
                        bodyChart.Series.Add(Series.SinglePoint(group.Key, group.Count()));
                    }
                }
            }

            var categoryChart = new Chart(this.Trans("Report_Non_Solo_Scenes_Breakdown"));
            yearlyData.AddChild(categoryChart);

            var groups = scenes.Where(scene => !soloPredicate.TrueFor(scene)).GroupBy(scene => Category(scene));

            foreach (var group in groups)
            {
                categoryChart.Series.Add(Series.SinglePoint(this.Trans("Report_Category_" + group.Key), group.Count()));
            }

            return yearlyData;
        }

        private string Category(Scene scene)
        {
            var categories = CategoryPredicates();

            foreach (var category in categories)
            {
                if (category.Value.Evaluate(scene))
                {
                    return category.Key;
                }
            }

            return "Straight";
        }

        private static Dictionary<string, IPredicate<Scene>> CategoryPredicates()
        {
            return new Dictionary<string, IPredicate<Scene>>
            {
                { "Anal", new AnalSexScenePredicate() },
                { "Oral", new OralSexScenePredicate() }
            };
        }
    }
}