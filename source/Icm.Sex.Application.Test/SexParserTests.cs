﻿namespace Icm.Sex.Application.Test
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Linq;
    using Icm.Collections;
    using Icm.Sex.Application.Parser;
    using Icm.Sex.Core.Predicates;
    using Icm.Sex.Domain;
    using Icm.Sex.Domain.Activities.Services;
    using Icm.Sex.Domain.Bodies;
    using Icm.Sex.Domain.Bodies.Predicates;
    using Icm.Sex.Domain.BodyTemplates.Services;
    using Icm.Sex.Domain.Connections;
    using Icm.Sex.Domain.Connections.Predicates;
    using Icm.Sex.Domain.Orgasms;
    using Icm.Sex.Domain.Parts.Services;
    using Icm.Sex.Domain.Scenes;
    using Icm.Sex.Domain.Scenes.Predicates;
    using Icm.Sex.Domain.Scenes.Repositories;
    using Icm.Sex.Domain.Situations;
    using Icm.Sex.Domain.UnitsOfWork;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    [ExcludeFromCodeCoverage]
    public class SexParserTests
    {
        private ParsingService parser;
        private MemorySexUnitOfWork unitOfWork;

        [TestInitialize]
        public void Setup()
        {
            unitOfWork = new MemorySexUnitOfWork();
            var initService = new InitializationService(
                unitOfWork,
                new PartInitializationService(),
                new ActivityInitializationService(),
                new BodyTemplateInitializationService());
            initService.Initialize();
            parser = new ParsingService(
                unitOfWork,
                new StandardTokenParser(),
                ParsingOptions.Diary());
        }

        [TestMethod]
        public void TokenParseError()
        {
            string source = @"define body ""futa";

            parser.ParseStringUnsafe(source);

            Assert.IsTrue(parser.HasErrors());
        }

        [TestMethod]
        public void CorrectCase()
        {
            string example = @"
define body futanari
    import woman
    add penis
    add testicles
end define

body joe man
body jill woman
body jane futanari

scene 2010-01-01
situation
    jane sucks joe.penis
situation
    jane sucks joe.penis
    jill licks joe.testicles
situation
    joe penetrates jane.vagina
situation
    joe penetrates jane.vagina
    jill licks joe.testicles
    orgasm jill
situation
    joe strokes joe.penis
    orgasm joe jane.mouth jill.mouth
";
            parser.ParseStringUnsafe(example);
            Body jane = unitOfWork.BodyRepository.Single(body => body.UniqueName == "jane");
            ISceneRepository scenes = unitOfWork.SceneRepository;
            Assert.AreEqual("futanari", jane.Template.UniqueName);
            Assert.AreEqual(1, scenes.Count());
            Assert.AreEqual(new DateTime(2010, 1, 1), scenes.First().Date);
            Assert.AreEqual(5, scenes.First().Situations.Count());

            AssertConnection(scenes, "jane.mouth", Willingness.Willing, "suck", Willingness.Willing, "joe.penis", 0, 0, 0, 0);
            AssertNoOrgasm(scenes, 0, 0);
            AssertConnection(scenes, "jane.mouth", Willingness.Willing, "suck", Willingness.Willing, "joe.penis", 0, 1, 0, 0);
            AssertConnection(scenes, "jill.tongue", Willingness.Willing, "lick", Willingness.Willing, "joe.testicles", 0, 1, 0, 1);
            AssertNoOrgasm(scenes, 0, 1);
            AssertConnection(scenes, "joe.penis", Willingness.Willing, "penetrate", Willingness.Willing, "jane.vagina", 0, 2, 0, 0);
            AssertNoOrgasm(scenes, 0, 2);
            AssertConnection(scenes, "joe.penis", Willingness.Willing, "penetrate", Willingness.Willing, "jane.vagina", 0, 3, 0, 0);
            AssertConnection(scenes, "jill.tongue", Willingness.Willing, "lick", Willingness.Willing, "joe.testicles", 0, 3, 0, 1);
            AssertOrgasm(scenes, 0, 3, "jill");
            AssertConnection(scenes, "joe.hand", Willingness.Willing, "stroke", Willingness.Willing, "joe.penis", 0, 4, 0, 0);
            AssertOrgasm(scenes, 0, 4, "joe", "jane.mouth", "jill.mouth");
        }

        [TestMethod]
        public void TestPredicateInfractions()
        {
            string example = @"
define body Shemale
    import Man
    add tits
end define

define body Futanari
    import Woman
    add penis
    add testicles
end define

body joe-man Man
body jack-man Man
body fiona-shemale Shemale
body jane-futa Futanari
body jill-woman Woman

scene 2010-01-01
situation
    jack-man sucks joe-man.penis
    jack-man sucks fiona-shemale.penis
    jack-man sucks jane-futa.penis
    fiona-shemale sucks jane-futa.penis
    jane-futa licks jane-futa.tits
    jill-woman.finger penetrates jack-man.anus
    jill-woman.finger penetrates jane-futa.anus
    jill-woman.finger penetrates fiona-shemale.anus
";
            parser.ParseStringUnsafe(example);

            // jack-man sucks joe-man.penis
            AssertPredicateInfractions(new LooseMaleHeteroPredicate(new MaleByTemplatePredicate()), 1);
            AssertPredicateInfractions(new LooseMaleHeteroPredicate(new MaleLoosePredicate()), 2);
            AssertPredicateInfractions(new LooseMaleHeteroPredicate(new MaleUnlimitedPredicate()), 7);
            AssertPredicateInfractions(new StrictMaleHeteroPredicate(new MaleByTemplatePredicate()), 5);
            AssertPredicateInfractions(new StrictMaleHeteroPredicate(new MaleLoosePredicate()), 8);
            AssertPredicateInfractions(new StrictMaleHeteroPredicate(new MaleUnlimitedPredicate()), 14);
        }

        private static void AssertNoOrgasm(
            IEnumerable<Scene> scenes,
            int sceneIdx,
            int situationIdx)
        {
            Scene scene = scenes.ElementAtOrDefault(sceneIdx);
            Assert.IsNotNull(scene);
            Situation situation = scene.Situations.ElementAtOrDefault(situationIdx);
            Assert.IsNotNull(situation);

            Assert.IsFalse(situation.Orgasms.Any());
        }

        private static void AssertOrgasm(
            IEnumerable<Scene> scenes,
            int sceneIdx,
            int situationIdx,
            string orgasmingBody,
            params string[] partsEjaculated)
        {
            Scene scene = scenes.ElementAtOrDefault(sceneIdx);
            Assert.IsNotNull(scene);
            Situation situation = scene.Situations.ElementAtOrDefault(situationIdx);
            Assert.IsNotNull(situation);

            Orgasm orgasm = situation.Orgasms.FirstOrDefault(
                x => x.OrgasmingBody.UniqueName == orgasmingBody);

            Assert.IsNotNull(orgasm);
            Assert.AreEqual(partsEjaculated.Length, orgasm.PartsEjaculated.Count);
            foreach (string part in partsEjaculated)
            {
                Assert.IsTrue(orgasm.PartsEjaculated.Any(z => z.ToString() == part));
            }
        }

        private static void AssertConnection(IEnumerable<Scene> scenes, string givingBodyPart, Willingness givingWillingness, string activity, Willingness receivingWillingness, string receivingBodyPart, int sceneIdx, int situationIdx, int cnxGroupIdx, int cnxIdx)
        {
            Scene scene = scenes.ElementAtOrDefault(sceneIdx);
            Assert.IsNotNull(scene);
            Situation situation = scene.Situations.ElementAtOrDefault(situationIdx);
            Assert.IsNotNull(situation);
            ConnectionGroup cnxGroup = situation.ConnectionGroups.ElementAtOrDefault(cnxGroupIdx);
            Assert.IsNotNull(cnxGroup);
            Connection connection = cnxGroup.Connections.ElementAtOrDefault(cnxIdx);
            Assert.IsNotNull(connection);

            Assert.AreEqual(givingBodyPart, connection.GivingBodyPart.ToString());
            Assert.AreEqual(givingWillingness, connection.GivingWillingness);
            Assert.AreEqual(activity, connection.Activity.ToString());
            Assert.AreEqual(receivingWillingness, connection.ReceivingWillingness);
            Assert.AreEqual(receivingBodyPart, connection.ReceivingBodyPart.ToString());
        }

        private void AssertPredicateInfractions(
            IPredicate<Connection> looseMaleHeteroPredicate,
            int expectedInfractionCount)
        {
            ISceneRepository scenes = unitOfWork.SceneRepository;
            IPredicate<IEnumerable<Scene>> predicate =
                new ScenePredicateOnAllConnections(looseMaleHeteroPredicate).Enum<Scene>().Test(scenes);
            List<string> infractions = predicate.FalseReasons.ToList();
            Assert.AreEqual(expectedInfractionCount, infractions.Count(), infractions.JoinStr("\n"));
        }
    }
}